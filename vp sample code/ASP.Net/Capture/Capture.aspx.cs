using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/*
Copyright Statement
Copyright � 2008 Transaction Network Services Inc ("TNS").  All rights reserved.

Disclaimer
This document is provided by TNS on the basis that you will treat it as confidential. No part of this document may be reproduced or copied in any form by any means without the written permission of TNS. Unless otherwise expressly agreed in writing, the information contained in this document is subject to change without notice and TNS assumes no responsibility for any alteration to, or any error or other deficiency, in this document. 
All intellectual property rights in the Document and the TNS products and service referred to therein (�TNS IPR�) in addition to all extracts and things derived from any part of the Document are owned by TNS and will be assigned to TNS on their creation. You will protect all TNS IPR in a manner that is equal to the protection that you provide to your own intellectual property.  You will notify TNS immediately in writing where you become aware of a breach of TNS IPR.
The names �TNS�, any product names of TNS including �Dialect� and �QSI Payments� and all similar words are trademarks of TNS and you must not use that name or any similar name.
TNS may at its sole discretion terminate the rights granted in this document with immediate effect by notifying you in writing and you will thereupon return (or destroy and certify that destruction to TNS) all copies and extracts of the Document in its possession or control.
TNS does not warrant the accuracy or completeness of the Document or its content or its usefulness to you or your merchant cardholders. To the maximum extent permitted by law, all conditions and warranties implied by law (including without limitation as to fitness for any particular purpose) are excluded.  Where the exclusion is not effective, TNS limits its liability to �100 or the resupply of the Document (at TNS's option). Data used in examples and sample data files are intended to be fictional and any resemblance to real persons or companies is entirely coincidental.
TNS does not indemnify you or any third party in relation to the content of this document or any use of such content. 
Any mention of any product not owned by TNS does not constitute an endorsement of that product.
This document is governed by the laws of England and Wales and is intended to be legally binding.
 */

namespace _TNS
{
    public partial class _Default : System.Web.UI.Page
    {
        public static string Version
        {
            get
            {
                // Return the Example Code Version
                return "SHA256 1.0";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pnlError.Visible = false;
                pnlResponse.Visible = false;
                pnlRequest.Visible = true; 
                // Get the Example Code version information
                Label_Example_Version.Text = _Default.Version;
                Label_VPCRequest_Version.Text = VPCRequest.Version;
                Label_PaymentCodesHelper_Version.Text = PaymentCodesHelper.Version;

                // Display the Example Code version information
                pnlVersionInformation.Visible = true;
            }
        }

        protected void btnPay_Click(object sender, EventArgs e)
        {
            pnlRequest.Visible = false;
            try
            {
                // Create the VPCRequest Object
                VPCRequest conn = new VPCRequest(_TNS.Properties.Settings.Default.PaymentServerURL);

                // Configure the Proxy details (if needed)
                conn.SetProxyHost(_TNS.Properties.Settings.Default.ProxyHost);
                conn.SetProxyUser(_TNS.Properties.Settings.Default.ProxyUser);
                conn.SetProxyPassword(_TNS.Properties.Settings.Default.ProxyPassword);
                conn.SetProxyDomain(_TNS.Properties.Settings.Default.ProxyDomain);
                conn.SetSecureSecret(_TNS.Properties.Settings.Default.vpc_SecureSecret);

                // Add the Digital Order Fields for the functionality you wish to use
                // Core Transaction Fields
                conn.AddDigitalOrderField("vpc_Version", _TNS.Properties.Settings.Default.vpc_Version);
                conn.AddDigitalOrderField("vpc_Command", _TNS.Properties.Settings.Default.vpc_Command);
                conn.AddDigitalOrderField("vpc_AccessCode", _TNS.Properties.Settings.Default.vpc_AccessCode);
                conn.AddDigitalOrderField("vpc_Merchant", _TNS.Properties.Settings.Default.vpc_Merchant);
                conn.AddDigitalOrderField("vpc_User", _TNS.Properties.Settings.Default.vpc_User);
                conn.AddDigitalOrderField("vpc_Password", _TNS.Properties.Settings.Default.vpc_Password);
                conn.AddDigitalOrderField("vpc_MerchTxnRef", vpc_MerchTxnRef.Text);
                conn.AddDigitalOrderField("vpc_TransNo", vpc_TransNo.Text);
                conn.AddDigitalOrderField("vpc_Amount", vpc_Amount.Text);

                // Perform the transaction
                conn.SendRequest();

                // Check if the transaction was successful or if there was an error
                String vpc_TxnResponseCode = conn.GetResultField("vpc_TxnResponseCode", "Unknown");

                // Set the display fields for the receipt with the result fields
                Label_vpc_Message.Text = conn.GetResultField("vpc_Message", "");
                Label_vpc_MerchTxnRef.Text = conn.GetResultField("vpc_MerchTxnRef", "Unknown");
                Label_vpc_Merchant.Text = conn.GetResultField("vpc_Merchant", "Unknown");
                Label_vpc_OrderInfo.Text = conn.GetResultField("vpc_OrderInfo", "Unknown");
                Label_vpc_Amount.Text = conn.GetResultField("vpc_Amount", "Unknown");
                Label_vpc_TxnResponseCode.Text = vpc_TxnResponseCode;
                Label_vpc_AcqResponseCode.Text = conn.GetResultField("vpc_AcqResponseCode", "Unknown");
                Label_vpc_TransactionNo.Text = conn.GetResultField("vpc_TransactionNo", "Unknown");
                Label_vpc_ShopTransactionNo.Text = conn.GetResultField("vpc_ShopTransactionNo", "Unknown");
                Label_vpc_ReceiptNo.Text = conn.GetResultField("vpc_ReceiptNo", "Unknown");
                Label_vpc_AuthorizeId.Text = conn.GetResultField("vpc_AuthorizeId", "Unknown");
                Label_vpc_BatchNo.Text = conn.GetResultField("vpc_BatchNo", "Unknown");
                Label_vpc_TicketNo.Text = conn.GetResultField("vpc_TicketNo", "Unknown");
                Label_vpc_Card.Text = conn.GetResultField("vpc_Card", "Unknown");
                Label_vpc_AuthorisedAmount.Text = conn.GetResultField("vpc_AuthorisedAmount", "Unknown");
                Label_vpc_CapturedAmount.Text = conn.GetResultField("vpc_CapturedAmount", "Unknown");
                Label_vpc_RefundedAmount.Text = conn.GetResultField("vpc_RefundedAmount", "Unknown");

                Label_vpc_TxnResponseCodeDesc.Text = PaymentCodesHelper.GetTxnResponseCodeDescription(vpc_TxnResponseCode);

                // Display the receipt
                pnlResponse.Visible = true;

                // Get the Example Code version Information
                Label_Example_Version.Text = _Default.Version;
                Label_VPCRequest_Version.Text = VPCRequest.Version;
                Label_PaymentCodesHelper_Version.Text = PaymentCodesHelper.Version;

                // Display the Example Code version Information
                pnlVersionInformation.Visible = true;
            }
            catch (Exception ex)
            {
                // Capture and display the error information
                lblErrorMessage.Text = ex.Message + (ex.InnerException != null ? ex.InnerException.Message : "");
                pnlError.Visible = true; 
                try
                {
                    // Get the Example Code version Information
                    Label_Example_Version.Text = _Default.Version;
                    Label_VPCRequest_Version.Text = VPCRequest.Version;
                    Label_PaymentCodesHelper_Version.Text = PaymentCodesHelper.Version;
                    
                    // Display the Example Code version Information
                    pnlVersionInformation.Visible = true;
                }
                catch (Exception ex2)
                {
                    // Do Nothing
                }
            }
        }
    }
}
