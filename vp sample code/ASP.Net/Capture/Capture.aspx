<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Capture.aspx.cs" Inherits="_TNS._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- 
Copyright � 2003-2008 Dialect Payment Technologies Pty Ltd incorporated in Queensland, Australia ("Dialect").  All rights reserved. 
 
This document is provided by Dialect on the basis that you will treat it as confidential. 
No part of this document may be reproduced or copied in any form by any means without the written permission of Dialect.  Unless otherwise expressly agreed in writing, the information contained in this document is subject to change without notice and Dialect assumes no responsibility for any alteration to, or any error or other deficiency, in this document. 
All intellectual property rights in the Document and in all extracts and things derived from any part of the Document are owned by Dialect and will be assigned to Dialect on their creation. You will protect all the intellectual property rights relating to the Document in a manner that is equal to the protection you provide your own intellectual property.  You will notify Dialect immediately, and in writing where you become aware of a breach of Dialect's intellectual property rights in relation to the Document.
The names "Dialect", "QSI Payments" and all similar words are trademarks of Dialect Payment Technologies Pty Ltd and you must not use that name or any similar name.
Dialect may at its sole discretion terminate the rights granted in this document with immediate effect by notifying you in writing and you will thereupon return (or destroy and certify that destruction to Dialect) all copies and extracts of the Document in its possession or control.
Dialect does not warrant the accuracy or completeness of the Document or its content or its usefulness to you or your merchant customers. To the extent permitted by law, all conditions and warranties implied by law (whether as to fitness for any particular purpose or otherwise) are excluded.  Where the exclusion is not effective, Dialect limits its liability to AU$100 or the resupply of the Document (at Dialect's option).
Data used in examples and sample data files are intended to be fictional and any resemblance to real persons or companies is entirely coincidental.
Dialect does not indemnify you or any third party in relation to the content or any use of the content as contemplated in these terms and conditions. 
Mention of any product not owned by Dialect does not constitute an endorsement of that product.
This document is governed by the laws of New South Wales, Australia and is intended to be legally binding. 
-->

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Virtual Payment Client Example - ASP.Net (C#) </title>
    <style type='text/css'>
    h1       { font-family:Arial,sans-serif; font-size:20pt; font-weight:600; margin-bottom:0.1em; color:#08185A;}
    h2       { font-family:Arial,sans-serif; font-size:14pt; font-weight:100; margin-top:0.1em; color:#08185A;}
    h2.co    { font-family:Arial,sans-serif; font-size:24pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
    h3       { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
    h3.co    { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#FFFFFF}
    body     { font-family:Verdana,Arial,sans-serif; font-size:10pt; background-color:#FFFFFF; color:#08185A}
    th       { font-family:Verdana,Arial,sans-serif; font-size:8pt; font-weight:bold; background-color:#CED7EF; padding-top:0.5em; padding-bottom:0.5em;  color:#08185A}
    tr       { height:25px; }
    .shade   { height:25px; background-color:#CED7EF }
    .title   { height:25px; background-color:#0074C4 }
    td       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A }
    td.red   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
    td.green { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#008800 }
    p        { font-family:Verdana,Arial,sans-serif; font-size:10pt; color:#FFFFFF }
    p.blue   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#08185A }
    p.red    { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
    p.green  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#008800 }
    div.bl   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#0074C4 }
    div.red  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
    li       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
    input    { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold }
    select   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold; }
    textarea { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:normal; }
</style>
</head>
<body>
    <!-- start branding table -->
    <div style="text-align: center;">
    <table style="margin-right: auto; margin-left:auto; width: 719px; background-color: #0074c4;">
        <tbody>
            <tr>
                <td style="padding: 5px; background-color: #ced7ef; width: 90%; text-align: left"><h2 class="co">American Express</h2><br />
                    <h3>Virtual Payment Client Example</h3></td>
                <td style="padding: 5px; text-align: left; background-color: #0074c4;"><h3 class="co">Dialect<br />Payment<br />Technologies</h3></td>
            </tr>
        </tbody>
    </table>
    </div>
    <!-- end branding table -->

    <div style="text-align: center;"><h1>Capture Transaction</h1></div>
        
    <form id="TransactionForm" runat="server">
    <div style="text-align: center;">
        <asp:Panel ID="pnlRequest" runat="server" Style="margin: 0 auto; width: 719px;">
            <table style="margin-right: auto; margin-left:auto; border-width: 0; padding: 5px; width: 719px;">
                <tbody>
                    <tr class="title">
                        <td colspan="2" style="text-align: left"><p><strong>&nbsp;How to use this example</strong></p></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left">&nbsp;<p class="blue">To use this example code the application settings in Web.config need to be configured. Listed below are the required settings.</p><p class="blue">Once these settings are configured, enter required data in each of the sections on this page that correspond to the functionality to be used for the transaction.
                            Then click the "Capture Now!" button to continue.</p>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                    <tr class="title">
                        <td colspan="2" style="text-align: left"><p><strong>&nbsp;Configuration Settings Required</strong></p></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>PaymentServerURL:</em></strong></td>
                        <td style="text-align: left">This is the URL that the example will use to connect to the Payment Server, e.g. https://vpos.amxvpos.com/vpcdps</td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>ProxyHost:</em></strong></td>
                        <td style="text-align: left">If a Proxy is required to access the internet specify the Proxy hostname or IP Address.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>ProxyUser:</em></strong></td>
                        <td style="text-align: left">If a Proxy is required to access the internet specify the Proxy hostname or IP Address.</td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>ProxyPassword:</em></strong></td>
                        <td style="text-align: left">If a Proxy is required to access the internet specify the Proxy hostname or IP Address.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>ProxyDomain:</em></strong></td>
                        <td style="text-align: left">If a Proxy is required to access the internet specify the Proxy hostname or IP Address.</td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>vpc_Version:</em></strong></td>
                        <td style="text-align: left">This is the VPC API version being used. The valid value for this example is "1".</td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>vpc_Command:</em></strong></td>
                        <td style="text-align: left">This is the VPC command to be used. The valid value for this example is "capture".</td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>vpc_Merchant:</em></strong></td>
                        <td style="text-align: left">This is the Payment Server Merchant ID that this transaction is to be conducted against.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>vpc_AccessCode:</em></strong></td>
                        <td style="text-align: left">This is the Merchant Access Code that corresponds to the Payment Server Merchant ID to be used. The value for this field is available from the Configuration in the "Admin" section of the Merchant Administration Portal. </td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>vpc_User:</em></strong></td>
                        <td style="text-align: left">This is the username of the user to use for the capture transaction. This user must be created and configured as an "Operator" in the "Admin" section of the Merchant Administration Portal. This a user must be configured with the "Advanced Merchant Administration" priviledge.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>vpc_Password:</em></strong></td>
                        <td style="text-align: left">This is the password for the vpc_User specified above.</td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                    <tr class="title" style="text-align: left">
                        <td colspan="2"><p><strong>&nbsp;Basic Transaction Fields</strong></p></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Merchant Transaction Reference:</em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_MerchTxnRef" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Capture Amount:</em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_Amount" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Shopping Transaction Number:</em></strong><br />(Transaction Number of the Authorization Transaction)</td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_TransNo" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center; height: 21px"><asp:Button ID="btnPay" runat="server" Text="Capture Now!" OnClick="btnPay_Click" /></td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlResponse" runat="server" Style="margin: 0 auto; width: 719px;">
            <table style="margin-right: auto; margin-left:auto; border-width: 0; padding: 5px; width: 719px;">
                <tbody>
                    <tr class="title">
                        <td colspan="2" style="text-align: left"><p><strong>&nbsp;Transaction Receipt Fields</strong></p></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>MerchTxnRef: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_MerchTxnRef" runat="server"/></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Merchant ID: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_Merchant" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>OrderInfo: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_OrderInfo" runat="server"/></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Capture Amount: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_Amount" runat="server"/></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <div class='bl'>Fields above are the primary request values.<hr />Fields below are receipt data fields.</div>
                        </td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Transaction Response Code: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_TxnResponseCode" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Transaction Response Code Description: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_TxnResponseCodeDesc" 
                                runat="server"/></td>
                    </tr>
                    <tr  class="shade">
                        <td style="text-align: right"><strong><em>Payment Server Message: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_Message" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Acquirer Response Code: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_AcqResponseCode" runat="server"/></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Financial Transaction Number: </em></strong><br />(Transaction Number of this Capture Transaction)</td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_TransactionNo" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Shopping Transaction Number: </em></strong><br />(Transaction Number of the Authorization Transaction)</td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_ShopTransactionNo" runat="server"/></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Ticket Number: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_TicketNo" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Receipt Number: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_ReceiptNo" runat="server"/></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Authorization ID: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_AuthorizeId" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Batch Number for this transaction: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_BatchNo" runat="server"/></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Card Type: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_Card" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Authorized Amount: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_AuthorisedAmount" runat="server"/></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Total Captured Amount: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_CapturedAmount" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Total Refunded Amount: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_vpc_RefundedAmount" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlError" runat="server" Style="margin: 0 auto; width: 719px;">
            <table style="margin-right: auto; margin-left:auto; border-width: 0; padding: 5px; width: 719px;">
                <tbody>
                    <tr class="title">
                        <td colspan="2" style="text-align: left"><p><strong>&nbsp;Error Information</strong></p></td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 149px;"><strong><em>Error Message: </em></strong></td>
                        <td style="text-align: left; width: 650px"><asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="height: 21px" />
                        <td style="height: 21px" />
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
        &nbsp;
    </div>
    </form>
    <div style="text-align: center;">
        <asp:Panel ID="pnlVersionInformation" runat="server" Style="margin: 0 auto; width: 719px;">
            <table style="margin-right: auto; margin-left:auto; border-width: 0; padding: 5px; width: 719px;">
                <tbody>
                    <tr class="title" style="text-align: left">
                        <td colspan="2"><p><strong>&nbsp;Example Code Version Information</strong></p></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Example Version: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_Example_Version" runat="server"/></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>VPCRequest Version: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_VPCRequest_Version" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>PaymentCodesHelper Version: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_PaymentCodesHelper_Version" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
        &nbsp;
    </div>
</body>
</html>
