using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/*
Copyright Statement
Copyright � 2008 Transaction Network Services Inc ("TNS").  All rights reserved.

Disclaimer
This document is provided by TNS on the basis that you will treat it as confidential. No part of this document may be reproduced or copied in any form by any means without the written permission of TNS. Unless otherwise expressly agreed in writing, the information contained in this document is subject to change without notice and TNS assumes no responsibility for any alteration to, or any error or other deficiency, in this document. 
All intellectual property rights in the Document and the TNS products and service referred to therein (�TNS IPR�) in addition to all extracts and things derived from any part of the Document are owned by TNS and will be assigned to TNS on their creation. You will protect all TNS IPR in a manner that is equal to the protection that you provide to your own intellectual property.  You will notify TNS immediately in writing where you become aware of a breach of TNS IPR.
The names �TNS�, any product names of TNS including �Dialect� and �QSI Payments� and all similar words are trademarks of TNS and you must not use that name or any similar name.
TNS may at its sole discretion terminate the rights granted in this document with immediate effect by notifying you in writing and you will thereupon return (or destroy and certify that destruction to TNS) all copies and extracts of the Document in its possession or control.
TNS does not warrant the accuracy or completeness of the Document or its content or its usefulness to you or your merchant cardholders. To the maximum extent permitted by law, all conditions and warranties implied by law (including without limitation as to fitness for any particular purpose) are excluded.  Where the exclusion is not effective, TNS limits its liability to �100 or the resupply of the Document (at TNS's option). Data used in examples and sample data files are intended to be fictional and any resemblance to real persons or companies is entirely coincidental.
TNS does not indemnify you or any third party in relation to the content of this document or any use of such content. 
Any mention of any product not owned by TNS does not constitute an endorsement of that product.
This document is governed by the laws of England and Wales and is intended to be legally binding.
 */

namespace _TNS
{
    public partial class _Default : System.Web.UI.Page
    {
        public static string Version
        {
            get
            {
                // Return the Example Code Version
                return "SHA256 1.0";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pnlError.Visible = false;
                pnlResponse.Visible = false;
                pnlRequest.Visible = true;
                Label_Example_Version.Text = _Default.Version;
                Label_VPCRequest_Version.Text = VPCRequest.Version;
                Label_PaymentCodesHelper_Version.Text = PaymentCodesHelper.Version;
                pnlVersionInformation.Visible = true;
            }
        }

        protected void btnPay_Click(object sender, EventArgs e)
        {
            pnlRequest.Visible = false;
            try
            {
                // Create the VPCRequest Object
                VPCRequest conn = new VPCRequest(_TNS.Properties.Settings.Default.PaymentServerURL);

                // Configure the proxy details (if needed)
                conn.SetProxyHost(_TNS.Properties.Settings.Default.ProxyHost);
                conn.SetProxyUser(_TNS.Properties.Settings.Default.ProxyUser);
                conn.SetProxyPassword(_TNS.Properties.Settings.Default.ProxyPassword);
                conn.SetProxyDomain(_TNS.Properties.Settings.Default.ProxyDomain);

                // Set SecureSecret as defined in properties
                conn.SetSecureSecret(_TNS.Properties.Settings.Default.vpc_SecureSecret);

                // Add the Digital Order Fields for the functionality you wish to use
                // Core Transaction Fields
                conn.AddDigitalOrderField("vpc_Version", _TNS.Properties.Settings.Default.vpc_Version);
                conn.AddDigitalOrderField("vpc_AccessCode", _TNS.Properties.Settings.Default.vpc_AccessCode);
                conn.AddDigitalOrderField("vpc_Merchant", _TNS.Properties.Settings.Default.vpc_Merchant);
                conn.AddDigitalOrderField("vpc_MerchTxnRef", vpc_MerchTxnRef.Text);
                conn.AddDigitalOrderField("vpc_OrderInfo", vpc_OrderInfo.Text);
                conn.AddDigitalOrderField("vpc_Amount", vpc_Amount.Text);
                conn.AddDigitalOrderField("vpc_Command", "pay");
                conn.AddDigitalOrderField("vpc_Currency", Currency_List.Text);

                // Card Details
                conn.AddDigitalOrderField("vpc_CardNum", vpc_CardNum.Text);
                conn.AddDigitalOrderField("vpc_CardExp", conn.FormatDate(CardExpiryMonth.Text, CardExpiryYear.Text));

                // Ticket Number
                conn.AddDigitalOrderField("vpc_TicketNo", vpc_TicketNo.Text);

                // Transaction Source and Transaction Source Sub-type
                conn.AddDigitalOrderField("vpc_TxSource", vpc_TxSource.SelectedValue);
                conn.AddDigitalOrderField("vpc_TxSourceSubType", vpc_TxSourceSubType.SelectedValue);

                // Card Security Code
                conn.AddDigitalOrderField("vpc_CardSecurityCode", vpc_CardSecurityCode.Text);

                // Address Verification / Advanced Address Verificaton
                conn.AddDigitalOrderField("vpc_AVS_Street01", vpc_AVS_Street01.Text);
                conn.AddDigitalOrderField("vpc_AVS_PostCode", vpc_AVS_PostCode.Text);
                conn.AddDigitalOrderField("vpc_AVS_City", vpc_AVS_City.Text);
                conn.AddDigitalOrderField("vpc_AVS_StateProv", vpc_AVS_StateProv.Text);
                conn.AddDigitalOrderField("vpc_AVS_Country", vpc_AVS_Country.SelectedValue);
                conn.AddDigitalOrderField("vpc_BillTo_Title", vpc_BillTo_Title.Text);
                conn.AddDigitalOrderField("vpc_BillTo_Firstname", vpc_BillTo_Firstname.Text);
                conn.AddDigitalOrderField("vpc_BillTo_Middlename", vpc_BillTo_Middlename.Text);
                conn.AddDigitalOrderField("vpc_BillTo_Lastname", vpc_BillTo_Lastname.Text);
                conn.AddDigitalOrderField("vpc_BillTo_Phone", vpc_BillTo_Phone.Text);
                conn.AddDigitalOrderField("vpc_ShipTo_Fullname", vpc_ShipTo_Fullname.Text);
                conn.AddDigitalOrderField("vpc_ShipTo_Title", vpc_ShipTo_Title.Text);
                conn.AddDigitalOrderField("vpc_ShipTo_Firstname", vpc_ShipTo_Firstname.Text);
                conn.AddDigitalOrderField("vpc_ShipTo_Middlename", vpc_ShipTo_Middlename.Text);
                conn.AddDigitalOrderField("vpc_ShipTo_Lastname", vpc_ShipTo_Lastname.Text);
                conn.AddDigitalOrderField("vpc_ShipTo_Phone", vpc_ShipTo_Phone.Text);
                conn.AddDigitalOrderField("vpc_ShipTo_Street01", vpc_ShipTo_Street01.Text);
                conn.AddDigitalOrderField("vpc_ShipTo_City", vpc_ShipTo_City.Text);
                conn.AddDigitalOrderField("vpc_ShipTo_StateProv", vpc_ShipTo_StateProv.Text);
                conn.AddDigitalOrderField("vpc_ShipTo_PostCode", vpc_ShipTo_PostCode.Text);
                conn.AddDigitalOrderField("vpc_ShipTo_Country", vpc_ShipTo_Country.SelectedValue);

                // Airline Passenger Data
                conn.AddDigitalOrderField("vpc_APD_DeptDate", vpc_APD_DeptDate.Text);
                conn.AddDigitalOrderField("vpc_APD_PassengerTitle", vpc_APD_PassengerTitle.Text);
                conn.AddDigitalOrderField("vpc_APD_PassengerFirstname", vpc_APD_PassengerFirstname.Text);
                conn.AddDigitalOrderField("vpc_APD_PassengerMiddlename", vpc_APD_PassengerMiddlename.Text);
                conn.AddDigitalOrderField("vpc_APD_PassengerLastname", vpc_APD_PassengerLastname.Text);
                conn.AddDigitalOrderField("vpc_APD_CardmemberTitle", vpc_APD_CardmemberTitle.Text);
                conn.AddDigitalOrderField("vpc_APD_CardmemberFirstname", vpc_APD_CardmemberFirstname.Text);
                conn.AddDigitalOrderField("vpc_APD_CardmemberMiddlename", vpc_APD_CardmemberMiddlename.Text);
                conn.AddDigitalOrderField("vpc_APD_CardmemberLastname", vpc_APD_CardmemberLastname.Text);
                conn.AddDigitalOrderField("vpc_APD_Origin", vpc_APD_Origin.Text);
                conn.AddDigitalOrderField("vpc_APD_Dest", vpc_APD_Dest.Text);
                conn.AddDigitalOrderField("vpc_APD_Route", vpc_APD_Route.Text);
                conn.AddDigitalOrderField("vpc_APD_Carriers", vpc_APD_Carriers.Text);
                conn.AddDigitalOrderField("vpc_APD_FareBasis", vpc_APD_FareBasis.Text);
                conn.AddDigitalOrderField("vpc_APD_NumPassengers", vpc_APD_NumPassengers.Text);
                conn.AddDigitalOrderField("vpc_APD_eTicket", vpc_APD_eTicket.Text);
                conn.AddDigitalOrderField("vpc_APD_ResCode", vpc_APD_ResCode.Text);
                conn.AddDigitalOrderField("vpc_APD_TravelAgentCode", vpc_APD_TravelAgentCode.Text);

                // Internet Transaction Date
                conn.AddDigitalOrderField("vpc_CustomerEmail", vpc_CustomerEmail.Text);
                conn.AddDigitalOrderField("vpc_ITD_CustomerHostname", vpc_ITD_CustomerHostname.Text);
                conn.AddDigitalOrderField("vpc_ITD_CustomerBrowser", vpc_ITD_CustomerBrowser.Text);
                conn.AddDigitalOrderField("vpc_ITD_ShipMethodCode", vpc_ITD_ShipMethodCode.Text);
                conn.AddDigitalOrderField("vpc_ITD_ShipToCountryCode", vpc_ITD_ShipToCountryCode.SelectedValue);
                conn.AddDigitalOrderField("vpc_ITD_MerchantSKU", vpc_ITD_MerchantSKU.Text);
                conn.AddDigitalOrderField("vpc_CustomerIpAddress", vpc_CustomerIpAddress.Text);
                conn.AddDigitalOrderField("vpc_ITD_CustomerANI", vpc_ITD_CustomerANI.Text);
                conn.AddDigitalOrderField("vpc_ITD_CustomerANICallType", vpc_ITD_CustomerANICallType.Text);

                // Card Present Track Data
                conn.AddDigitalOrderField("vpc_CardTrack1", vpc_CardTrack1.Text);
                conn.AddDigitalOrderField("vpc_CardTrack2", vpc_CardTrack2.Text);

                // Perform the transaction
                conn.SendRequest();

                // Check if the transaction was successful or if there was an error
                String vpc_TxnResponseCode = conn.GetResultField("vpc_TxnResponseCode", "Unknown");

                // Set the display fields for the receipt with the result fields
                // Core fields
                Label_vpc_TxnResponseCode.Text = vpc_TxnResponseCode;
                Label_vpc_MerchTxnRef.Text = conn.GetResultField("vpc_MerchTxnRef", "Unknown");
                Label_vpc_OrderInfo.Text = conn.GetResultField("vpc_OrderInfo", "Unknown");
                Label_vpc_Merchant.Text = conn.GetResultField("vpc_Merchant", "Unknown");
                Label_vpc_Amount.Text = conn.GetResultField("vpc_Amount", "Unknown");
                Label_vpc_Message.Text = conn.GetResultField("vpc_Message", "Unknown");
                Label_vpc_ReceiptNo.Text = conn.GetResultField("vpc_ReceiptNo", "Unknown");
                Label_vpc_AcqResponseCode.Text = conn.GetResultField("vpc_AcqResponseCode", "Unknown");
                Label_vpc_AuthorizeId.Text = conn.GetResultField("vpc_AuthorizeId", "Unknown");
                Label_vpc_BatchNo.Text = conn.GetResultField("vpc_BatchNo", "Unknown");
                Label_vpc_TransactionNo.Text = conn.GetResultField("vpc_TransactionNo", "Unknown");
                Label_vpc_Card.Text = conn.GetResultField("vpc_Card", "Unknown");
                Label_vpc_TxnResponseCodeDesc.Text = PaymentCodesHelper.GetTxnResponseCodeDescription(Label_vpc_TxnResponseCode.Text);

                // Card Security Code Result Field
                Label_vpc_cscResultCode.Text = conn.GetResultField("vpc_cscResultCode", "Unknown");
                Label_vpc_cscResultCodeDesc.Text = PaymentCodesHelper.GetCSCDescription(Label_vpc_cscResultCode.Text);

                // Address Verification / Advanced Address Verificaton
                Label_vpc_avsResultCode.Text = conn.GetResultField("vpc_avsResultCode", "Unknown");
                Label_vpc_AcqAVSRespCode.Text = conn.GetResultField("vpc_AcqAVSRespCode", "Unknown");
                Label_vpc_avsResultCodeDesc.Text = PaymentCodesHelper.GetAVSDescription(Label_vpc_avsResultCode.Text);

                // Perform the Capture if the Authorization was successful
                Label_Capture_vpc_Message.Text = "Capture Not Performed";
                Label_Capture_vpc_TxnResponseCode.Text = "Capture Not Performed";
                Label_Capture_vpc_AcqResponseCode.Text = "Capture Not Performed";
                Label_Capture_vpc_TransactionNo.Text = "Capture Not Performed";
                Label_Capture_vpc_MerchTxnRef.Text = "Capture Not Performed";
                Label_Capture_vpc_ReceiptNo.Text = "Capture Not Performed";
                Label_Capture_vpc_BatchNo.Text = "Capture Not Performed";
                Label_Capture_vpc_TxnResponseCodeDesc.Text = "Capture Not Performed";

                if (vpc_TxnResponseCode == "0")
                {
                    // Create a new VPCRequest Object and set the proxy details if required
                    conn = new VPCRequest(_TNS.Properties.Settings.Default.PaymentServerURL);

                    conn.SetProxyHost(_TNS.Properties.Settings.Default.ProxyHost);
                    conn.SetProxyUser(_TNS.Properties.Settings.Default.ProxyUser);
                    conn.SetProxyPassword(_TNS.Properties.Settings.Default.ProxyPassword);
                    conn.SetProxyDomain(_TNS.Properties.Settings.Default.ProxyDomain);
                    conn.SetSecureSecret(_TNS.Properties.Settings.Default.vpc_SecureSecret);

                    // Add the Required Fields
                    conn.AddDigitalOrderField("vpc_Version", _TNS.Properties.Settings.Default.vpc_Version);
                    conn.AddDigitalOrderField("vpc_AccessCode", _TNS.Properties.Settings.Default.vpc_AccessCode);
                    conn.AddDigitalOrderField("vpc_Merchant", _TNS.Properties.Settings.Default.vpc_Merchant);
                    conn.AddDigitalOrderField("vpc_User", _TNS.Properties.Settings.Default.vpc_User);
                    conn.AddDigitalOrderField("vpc_Password", _TNS.Properties.Settings.Default.vpc_Password);
                    conn.AddDigitalOrderField("vpc_Command", "capture");
                    conn.AddDigitalOrderField("vpc_MerchTxnRef", vpc_MerchTxnRef.Text + "-C");
                    conn.AddDigitalOrderField("vpc_TransNo", Label_vpc_TransactionNo.Text);
                    conn.AddDigitalOrderField("vpc_Amount", vpc_Amount.Text);

                    // Perform the transaction
                    conn.SendRequest();

                    // Check if the transaction was successful or if there was an error
                    String ResponseCode = conn.GetResultField("vpc_TxnResponseCode", "Unknown");

                    // Retrieve the Response Fields
                    Label_Capture_vpc_MerchTxnRef.Text = conn.GetResultField("vpc_MerchTxnRef", "Unknown");
                    Label_Capture_vpc_Message.Text = conn.GetResultField("vpc_Message", "");
                    Label_Capture_vpc_TxnResponseCode.Text = ResponseCode;
                    Label_Capture_vpc_AcqResponseCode.Text = conn.GetResultField("vpc_AcqResponseCode", "Unknown");
                    Label_Capture_vpc_TransactionNo.Text = conn.GetResultField("vpc_TransactionNo", "Unknown");
                    Label_Capture_vpc_ReceiptNo.Text = conn.GetResultField("vpc_ReceiptNo", "Unknown");
                    Label_Capture_vpc_BatchNo.Text = conn.GetResultField("vpc_BatchNo", "Unknown");

                    Label_Capture_vpc_TxnResponseCodeDesc.Text = PaymentCodesHelper.GetTxnResponseCodeDescription(ResponseCode);
                }
                // Display the Response Fields
                pnlResponse.Visible = true;

                // Get the example code version information
                Label_Example_Version.Text = _Default.Version;
                Label_VPCRequest_Version.Text = VPCRequest.Version;
                Label_PaymentCodesHelper_Version.Text = PaymentCodesHelper.Version;

                // Display the example code version information
                pnlVersionInformation.Visible = true;
            }
            catch (Exception ex)
            {
                // Capture and Display the Error information
                lblErrorMessage.Text = ex.Message + (ex.InnerException != null ? ex.InnerException.Message : "");
                pnlError.Visible = true;
                try
                {
                    // Get the example code version information
                    Label_Example_Version.Text = _Default.Version;
                    Label_VPCRequest_Version.Text = VPCRequest.Version;
                    Label_PaymentCodesHelper_Version.Text = PaymentCodesHelper.Version;

                    // Display the example code version information
                    pnlVersionInformation.Visible = true;
                }
                catch (Exception ex2)
                {
                    // Do Nothing
                }
            }

        }
    }
}
