<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="3Party_Combined_Auth_Cap_Order.aspx.cs" Inherits="_TNS._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- 
Copyright � 2003-2008 Dialect Payment Technologies Pty Ltd incorporated in Queensland, Australia ("Dialect").  All rights reserved. 
 
This document is provided by Dialect on the basis that you will treat it as confidential. 
No part of this document may be reproduced or copied in any form by any means without the written permission of Dialect.  Unless otherwise expressly agreed in writing, the information contained in this document is subject to change without notice and Dialect assumes no responsibility for any alteration to, or any error or other deficiency, in this document. 
All intellectual property rights in the Document and in all extracts and things derived from any part of the Document are owned by Dialect and will be assigned to Dialect on their creation. You will protect all the intellectual property rights relating to the Document in a manner that is equal to the protection you provide your own intellectual property.  You will notify Dialect immediately, and in writing where you become aware of a breach of Dialect's intellectual property rights in relation to the Document.
The names "Dialect", "QSI Payments" and all similar words are trademarks of Dialect Payment Technologies Pty Ltd and you must not use that name or any similar name.
Dialect may at its sole discretion terminate the rights granted in this document with immediate effect by notifying you in writing and you will thereupon return (or destroy and certify that destruction to Dialect) all copies and extracts of the Document in its possession or control.
Dialect does not warrant the accuracy or completeness of the Document or its content or its usefulness to you or your merchant customers. To the extent permitted by law, all conditions and warranties implied by law (whether as to fitness for any particular purpose or otherwise) are excluded.  Where the exclusion is not effective, Dialect limits its liability to AU$100 or the resupply of the Document (at Dialect's option).
Data used in examples and sample data files are intended to be fictional and any resemblance to real persons or companies is entirely coincidental.
Dialect does not indemnify you or any third party in relation to the content or any use of the content as contemplated in these terms and conditions. 
Mention of any product not owned by Dialect does not constitute an endorsement of that product.
This document is governed by the laws of New South Wales, Australia and is intended to be legally binding. 
-->

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Virtual Payment Client Example - ASP.Net (C#) </title>
    <style type='text/css'>
    h1       { font-family:Arial,sans-serif; font-size:20pt; font-weight:600; margin-bottom:0.1em; color:#08185A;}
    h2       { font-family:Arial,sans-serif; font-size:14pt; font-weight:100; margin-top:0.1em; color:#08185A;}
    h2.co    { font-family:Arial,sans-serif; font-size:24pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
    h3       { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
    h3.co    { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#FFFFFF}
    body     { font-family:Verdana,Arial,sans-serif; font-size:10pt; background-color:#FFFFFF; color:#08185A}
    th       { font-family:Verdana,Arial,sans-serif; font-size:8pt; font-weight:bold; background-color:#CED7EF; padding-top:0.5em; padding-bottom:0.5em;  color:#08185A}
    tr       { height:25px; }
    .shade   { height:25px; background-color:#CED7EF }
    .title   { height:25px; background-color:#0074C4 }
    td       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A }
    td.red   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
    td.green { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#008800 }
    p        { font-family:Verdana,Arial,sans-serif; font-size:10pt; color:#FFFFFF }
    p.blue   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#08185A }
    p.red    { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
    p.green  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#008800 }
    div.bl   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#0074C4 }
    div.red  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
    li       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
    input    { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold }
    select   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold; }
    textarea { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:normal; }
</style>
</head>
<body>
    <!-- start branding table -->
    <div style="text-align: center;">
    <table style="margin-right: auto; margin-left:auto; width: 719px; background-color: #0074c4;">
        <tbody>
            <tr>
                <td style="padding: 5px; background-color: #ced7ef; width: 90%; text-align: left"><h2 class="co">American Express</h2><br />
                    <h3>Virtual Payment Client Example</h3></td>
                <td style="padding: 5px; text-align: left; background-color: #0074c4;"><h3 class="co">Dialect<br />Payment<br />Technologies</h3></td>
            </tr>
        </tbody>
    </table>
    </div>
    <!-- end branding table -->

    <div style="text-align: center;"><h1>Combined Authorization and Capture<br />3-Party Transaction</h1></div>
        
    <form id="TransactionForm" runat="server">
    <div style="text-align: center;">
        <asp:Panel ID="pnlRequest" runat="server" Style="margin: 0 auto; width: 719px;">
            <table style="margin-right: auto; margin-left:auto; border-width: 0; padding: 5px; width: 719px;">
                <tbody>
                    <tr class="title">
                        <td colspan="2" style="text-align: left"><p><strong>&nbsp;How to use this example</strong></p></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left">&nbsp;<p class="blue">To use this example code the application settings in Web.config need to be configured. Listed below are the required settings.</p><p class="blue">Once these settings are configured, enter required data in each of the sections on this page that correspond to the functionality to be used for the transaction.
                            Then click the "Pay Now!" button to continue.</p>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                    <tr class="title">
                        <td colspan="2" style="text-align: left"><p><strong>&nbsp;Configuration Settings Required</strong></p></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>PaymentServerURL_Auth:</em></strong></td>
                        <td style="text-align: left">This is the URL that the example will use to connect to the Payment Server to perform the Authoriation, e.g. https://vpos.amxvpos.com/vpcpay</td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>PaymentServerURL_Capture:</em></strong></td>
                        <td style="text-align: left">This is the URL that the example will use to connect to the Payment Server to perform the Capture, e.g. https://vpos.amxvpos.com/vpcdps</td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>ProxyHost:</em></strong></td>
                        <td style="text-align: left">If a Proxy is required to access the internet specify the Proxy hostname or IP Address.</td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>ProxyUser:</em></strong></td>
                        <td style="text-align: left">If a Proxy is required to access the internet specify the Proxy hostname or IP Address.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>ProxyPassword:</em></strong></td>
                        <td style="text-align: left">If a Proxy is required to access the internet specify the Proxy hostname or IP Address.</td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>ProxyDomain:</em></strong></td>
                        <td style="text-align: left">If a Proxy is required to access the internet specify the Proxy hostname or IP Address.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>vpc_Version:</em></strong></td>
                        <td style="text-align: left">This is the VPC API version being used. The valid value for this example is "1".</td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>vpc_Merchant:</em></strong></td>
                        <td style="text-align: left">This is the Payment Server Merchant ID that this transaction is to be conducted against.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>vpc_AccessCode:</em></strong></td>
                        <td style="text-align: left">This is the Merchant Access Code that corresponds to the Payment Server Merchant ID to be used. The value for this field is available from the Configuration in the "Admin" section of the Merchant Administration Portal. </td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>SecureSecret:</em></strong></td>
                        <td style="text-align: left">This is the "Secure Secret" that corresponds to the Payment Server Merchant ID to be used. The value for this field is available from the Configuration in the "Admin" section of the Merchant Administration Portal. </td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>vpc_ReturnURL:</em></strong></td>
                        <td style="text-align: left">This is the URL that the Payment Server will sent the cardholders browser to upon completion of the transaction. THe cardholders browser will return with the transaction response.</td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>vpc_User:</em></strong></td>
                        <td style="text-align: left">This is the username of the user to use for the capture transaction. This user must be created and configured as an "Operator" in the "Admin" section of the Merchant Administration Portal. This a user must be configured with the "Advanced Merchant Administration" priviledge.</td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>vpc_Password:</em></strong></td>
                        <td style="text-align: left">This is the password for the vpc_User specified above.</td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                    <tr class="title">
                        <td colspan="2" style="text-align: left"><p><strong>&nbsp;Basic Transaction Fields</strong></p></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Merchant Transaction Reference:</em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_MerchTxnRef" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>OrderInfo:</em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_OrderInfo" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Purchase Amount:</em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_Amount" runat="server"></asp:TextBox></td>
                    </tr>
                    <td style="text-align: right"><strong><em>
                        Currency (optional field):</em></strong></td>
                    <td style="text-align: left">
                        <asp:DropDownList ID="Currency_List" runat="server">
                                <asp:ListItem Value="">Please Select</asp:ListItem>
                                <asp:ListItem Value="AED"></asp:ListItem>
								<asp:ListItem Value="ALL"></asp:ListItem>
								<asp:ListItem Value="AUD"></asp:ListItem>
								<asp:ListItem Value="BGN"></asp:ListItem>
								<asp:ListItem Value="BHD"></asp:ListItem>
								<asp:ListItem Value="BND"></asp:ListItem>								
								<asp:ListItem Value="CAD"></asp:ListItem>
								<asp:ListItem Value="CHF"></asp:ListItem>
								<asp:ListItem Value="CNY"></asp:ListItem>
								<asp:ListItem Value="CRC"></asp:ListItem>
								<asp:ListItem Value="CZK"></asp:ListItem>
								<asp:ListItem Value="DKK"></asp:ListItem>
								<asp:ListItem Value="DZD"></asp:ListItem>
								<asp:ListItem Value="EGP"></asp:ListItem>
								<asp:ListItem Value="EUR"></asp:ListItem>
								<asp:ListItem Value="FJD"></asp:ListItem>
								<asp:ListItem Value="GBP"></asp:ListItem>
								<asp:ListItem Value="HKD"></asp:ListItem>
								<asp:ListItem Value="HUF"></asp:ListItem>
								<asp:ListItem Value="IDR"></asp:ListItem>
								<asp:ListItem Value="INR"></asp:ListItem>
								<asp:ListItem Value="ISK"></asp:ListItem>
								<asp:ListItem Value="JOD"></asp:ListItem>
								<asp:ListItem Value="JPY"></asp:ListItem>
								<asp:ListItem Value="KRW"></asp:ListItem>
								<asp:ListItem Value="KWD"></asp:ListItem>
								<asp:ListItem Value="LBP"></asp:ListItem>
								<asp:ListItem Value="LKR"></asp:ListItem>
								<asp:ListItem Value="MAD"></asp:ListItem>
								<asp:ListItem Value="MYR"></asp:ListItem>
								<asp:ListItem Value="NOK"></asp:ListItem>
								<asp:ListItem Value="NZD"></asp:ListItem>
								<asp:ListItem Value="OMR"></asp:ListItem>
								<asp:ListItem Value="PGK"></asp:ListItem>
								<asp:ListItem Value="QAR"></asp:ListItem>
								<asp:ListItem Value="SAR"></asp:ListItem>
								<asp:ListItem Value="SEK"></asp:ListItem>
								<asp:ListItem Value="SGD"></asp:ListItem>
								<asp:ListItem Value="SVC"></asp:ListItem>
								<asp:ListItem Value="SYP"></asp:ListItem>
								<asp:ListItem Value="THB"></asp:ListItem>
								<asp:ListItem Value="TND"></asp:ListItem>
								<asp:ListItem Value="TWD"></asp:ListItem>
								<asp:ListItem Value="USD"></asp:ListItem>
								<asp:ListItem Value="VUV"></asp:ListItem>								
                            </asp:DropDownList></td>
                        
                </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                    <tr>
                        <td colspan="2"  style="text-align: center; height: 21px"><asp:Button ID="btnPay" runat="server" Text="Pay Now!" OnClick="btnPay_Click" /></td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                    <tr class="title">
                        <td colspan="2" style="text-align: left"><p><strong>&nbsp;Optional Ticket Number Field</strong></p></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>TicketNo:</em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_TicketNo" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                    <tr class="title">
                        <td colspan="2" style="text-align: left"><p><strong>&nbsp;Optional Extended Address Verfication Service Fields</strong></p></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Billing Title: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_BillTo_Title" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Billing  Firstname: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_BillTo_Firstname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Billing  Middlename: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_BillTo_Middlename" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Billing  Lastname: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_BillTo_Lastname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Billing  Phone Number: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_BillTo_Phone" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Billing Street/PO Box Address: </em></strong></td>
                        <td style="text-align: left">
                            <asp:TextBox ID="vpc_AVS_Street01" runat="server"></asp:TextBox>
                            <em>(Compulsory field if using AVS)</em>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Billing City/Town: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_AVS_City" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Billing State/Province: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_AVS_StateProv" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Billing Zip/Post Code: </em></strong></td>
                        <td style="text-align: left">
                            <asp:TextBox ID="vpc_AVS_PostCode" runat="server"></asp:TextBox>
                            <em>(Compulsory field if using AVS)</em>
                        </td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Billing Country of Issue: </em></strong></td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="vpc_AVS_Country" runat="server">
                                <asp:ListItem Value="">Please Select</asp:ListItem>
                                <asp:ListItem Value="AFG">Afghanistan</asp:ListItem>
                                <asp:ListItem Value="ALB">Albania</asp:ListItem>
                                <asp:ListItem Value="DZA">Algeria</asp:ListItem>
                                <asp:ListItem Value="ASM">American Samoa</asp:ListItem>
                                <asp:ListItem Value="AND">Andorra</asp:ListItem>
                                <asp:ListItem Value="AGO">Angola</asp:ListItem>
                                <asp:ListItem Value="AIA">Anguilla</asp:ListItem>
                                <asp:ListItem Value="ATA">Antarctica</asp:ListItem>
                                <asp:ListItem Value="ATG">Antigua And Barbuda</asp:ListItem>
                                <asp:ListItem Value="ARG">Argentina</asp:ListItem>
                                <asp:ListItem Value="ARM">Armenia</asp:ListItem>
                                <asp:ListItem Value="ABW">Aruba</asp:ListItem>
                                <asp:ListItem Value="AUS">Australia</asp:ListItem>
                                <asp:ListItem Value="AUT">Austria</asp:ListItem>
                                <asp:ListItem Value="AZE">Azerbaijan</asp:ListItem>
                                <asp:ListItem Value="BHS">Bahamas</asp:ListItem>
                                <asp:ListItem Value="BHR">Bahrain</asp:ListItem>
                                <asp:ListItem Value="BGD">Bangladesh</asp:ListItem>
                                <asp:ListItem Value="BRB">Barbados</asp:ListItem>
                                <asp:ListItem Value="BLR">Belarus</asp:ListItem>
                                <asp:ListItem Value="BEL">Belgium</asp:ListItem>
                                <asp:ListItem Value="BLZ">Belize</asp:ListItem>
                                <asp:ListItem Value="BEN">Benin</asp:ListItem>
                                <asp:ListItem Value="BMU">Bermuda</asp:ListItem>
                                <asp:ListItem Value="BTN">Bhutan</asp:ListItem>
                                <asp:ListItem Value="BOL">Bolivia</asp:ListItem>
                                <asp:ListItem Value="BIH">Bosnia And Herzegowina</asp:ListItem>
                                <asp:ListItem Value="BWA">Botswana</asp:ListItem>
                                <asp:ListItem Value="BVT">Bouvet Island</asp:ListItem>
                                <asp:ListItem Value="BRA">Brazil</asp:ListItem>
                                <asp:ListItem Value="IOT">British Indian Ocean Territory</asp:ListItem>
                                <asp:ListItem Value="BRN">Brunei Darussalam</asp:ListItem>
                                <asp:ListItem Value="BGR">Bulgaria</asp:ListItem>
                                <asp:ListItem Value="BFA">Burkina Faso</asp:ListItem>
                                <asp:ListItem Value="BDI">Burundi</asp:ListItem>
                                <asp:ListItem Value="KHM">Cambodia</asp:ListItem>
                                <asp:ListItem Value="CMR">Cameroon</asp:ListItem>
                                <asp:ListItem Value="CAN">Canada</asp:ListItem>
                                <asp:ListItem Value="CPV">Cape Verde</asp:ListItem>
                                <asp:ListItem Value="CYM">Cayman Islands</asp:ListItem>
                                <asp:ListItem Value="CAF">Central African Republic</asp:ListItem>
                                <asp:ListItem Value="TCD">Chad</asp:ListItem>
                                <asp:ListItem Value="CHL">Chile</asp:ListItem>
                                <asp:ListItem Value="CHN">China</asp:ListItem>
                                <asp:ListItem Value="CXR">Christmas Island</asp:ListItem>
                                <asp:ListItem Value="CCK">Cocos (Keeling) Islands</asp:ListItem>
                                <asp:ListItem Value="COL">Colombia</asp:ListItem>
                                <asp:ListItem Value="COM">Comoros</asp:ListItem>
                                <asp:ListItem Value="COG">Congo</asp:ListItem>
                                <asp:ListItem Value="COK">Cook Islands</asp:ListItem>
                                <asp:ListItem Value="CRI">Costa Rica</asp:ListItem>
                                <asp:ListItem Value="CIV">Cote D'ivoire</asp:ListItem>
                                <asp:ListItem Value="HRV">Croatia</asp:ListItem>
                                <asp:ListItem Value="CUB">Cuba</asp:ListItem>
                                <asp:ListItem Value="CYP">Cyprus</asp:ListItem>
                                <asp:ListItem Value="CZE">Czech Republic</asp:ListItem>
                                <asp:ListItem Value="DNK">Denmark</asp:ListItem>
                                <asp:ListItem Value="DJI">Djibouti</asp:ListItem>
                                <asp:ListItem Value="DMA">Dominica</asp:ListItem>
                                <asp:ListItem Value="DOM">Dominican Republic</asp:ListItem>
                                <asp:ListItem Value="TMP">East Timor</asp:ListItem>
                                <asp:ListItem Value="ECU">Ecuador</asp:ListItem>
                                <asp:ListItem Value="EGY">Egypt</asp:ListItem>
                                <asp:ListItem Value="SLV">El Salvador</asp:ListItem>
                                <asp:ListItem Value="GNQ">Equatorial Guinea</asp:ListItem>
                                <asp:ListItem Value="ERI">Eritrea</asp:ListItem>
                                <asp:ListItem Value="EST">Estonia</asp:ListItem>
                                <asp:ListItem Value="ETH">Ethiopia</asp:ListItem>
                                <asp:ListItem Value="FLK">Falkland Islands (Malvinas)</asp:ListItem>
                                <asp:ListItem Value="FRO">Faroe Islands</asp:ListItem>
                                <asp:ListItem Value="FJI">Fiji</asp:ListItem>
                                <asp:ListItem Value="FIN">Finland</asp:ListItem>
                                <asp:ListItem Value="FRA">France</asp:ListItem>
                                <asp:ListItem Value="FXX">France, Metropolitan</asp:ListItem>
                                <asp:ListItem Value="GUF">French Guiana</asp:ListItem>
                                <asp:ListItem Value="PYF">French Polynesia</asp:ListItem>
                                <asp:ListItem Value="ATF">French Southern Territories</asp:ListItem>
                                <asp:ListItem Value="GAB">Gabon</asp:ListItem>
                                <asp:ListItem Value="GMB">Gambia</asp:ListItem>
                                <asp:ListItem Value="GEO">Georgia</asp:ListItem>
                                <asp:ListItem Value="DEU">Germany</asp:ListItem>
                                <asp:ListItem Value="GHA">Ghana</asp:ListItem>
                                <asp:ListItem Value="GIB">Gibraltar</asp:ListItem>
                                <asp:ListItem Value="GRC">Greece</asp:ListItem>
                                <asp:ListItem Value="GRL">Greenland</asp:ListItem>
                                <asp:ListItem Value="GRD">Grenada</asp:ListItem>
                                <asp:ListItem Value="GLP">Guadeloupe</asp:ListItem>
                                <asp:ListItem Value="GUM">Guam</asp:ListItem>
                                <asp:ListItem Value="GTM">Guatemala</asp:ListItem>
                                <asp:ListItem Value="GIN">Guinea</asp:ListItem>
                                <asp:ListItem Value="GNB">Guinea-Bissau</asp:ListItem>
                                <asp:ListItem Value="GUY">Guyana</asp:ListItem>
                                <asp:ListItem Value="HTI">Haiti</asp:ListItem>
                                <asp:ListItem Value="HMD">Heard And Mc Donald Islands</asp:ListItem>
                                <asp:ListItem Value="HND">Honduras</asp:ListItem>
                                <asp:ListItem Value="HKG">Hong Kong</asp:ListItem>
                                <asp:ListItem Value="HUN">Hungary</asp:ListItem>
                                <asp:ListItem Value="ISL">Iceland</asp:ListItem>
                                <asp:ListItem Value="IND">India</asp:ListItem>
                                <asp:ListItem Value="IDN">Indonesia</asp:ListItem>
                                <asp:ListItem Value="IRN">Iran (Islamic Republic Of)</asp:ListItem>
                                <asp:ListItem Value="IRQ">Iraq</asp:ListItem>
                                <asp:ListItem Value="IRL">Ireland</asp:ListItem>
                                <asp:ListItem Value="ISR">Israel</asp:ListItem>
                                <asp:ListItem Value="ITA">Italy</asp:ListItem>
                                <asp:ListItem Value="JAM">Jamaica</asp:ListItem>
                                <asp:ListItem Value="JPN">Japan</asp:ListItem>
                                <asp:ListItem Value="JOR">Jordan</asp:ListItem>
                                <asp:ListItem Value="KAZ">Kazakhstan</asp:ListItem>
                                <asp:ListItem Value="KEN">Kenya</asp:ListItem>
                                <asp:ListItem Value="KIR">Kiribati</asp:ListItem>
                                <asp:ListItem Value="PRK">Korea, Democratic People's Republic Of</asp:ListItem>
                                <asp:ListItem Value="KOR">Korea, Republic Of</asp:ListItem>
                                <asp:ListItem Value="KWT">Kuwait</asp:ListItem>
                                <asp:ListItem Value="KGZ">Kyrgyzstan</asp:ListItem>
                                <asp:ListItem Value="LAO">Lao People's Democratic Republic</asp:ListItem>
                                <asp:ListItem Value="LVA">Latvia</asp:ListItem>
                                <asp:ListItem Value="LBN">Lebanon</asp:ListItem>
                                <asp:ListItem Value="LSO">Lesotho</asp:ListItem>
                                <asp:ListItem Value="LBR">Liberia</asp:ListItem>
                                <asp:ListItem Value="LBY">Libyan Arab Jamahiriya</asp:ListItem>
                                <asp:ListItem Value="LIE">Liechtenstein</asp:ListItem>
                                <asp:ListItem Value="LTU">Lithuania</asp:ListItem>
                                <asp:ListItem Value="LUX">Luxembourg</asp:ListItem>
                                <asp:ListItem Value="MAC">Macau</asp:ListItem>
                                <asp:ListItem Value="MKD">Macedonia, The Former Yugoslav Republic Of</asp:ListItem>
                                <asp:ListItem Value="MDG">Madagascar</asp:ListItem>
                                <asp:ListItem Value="MWI">Malawi</asp:ListItem>
                                <asp:ListItem Value="MYS">Malaysia</asp:ListItem>
                                <asp:ListItem Value="MDV">Maldives</asp:ListItem>
                                <asp:ListItem Value="MLI">Mali</asp:ListItem>
                                <asp:ListItem Value="MLT">Malta</asp:ListItem>
                                <asp:ListItem Value="MHL">Marshall Islands</asp:ListItem>
                                <asp:ListItem Value="MTQ">Martinique</asp:ListItem>
                                <asp:ListItem Value="MRT">Mauritania</asp:ListItem>
                                <asp:ListItem Value="MUS">Mauritius</asp:ListItem>
                                <asp:ListItem Value="MYT">Mayotte</asp:ListItem>
                                <asp:ListItem Value="MEX">Mexico</asp:ListItem>
                                <asp:ListItem Value="FSM">Micronesia (Federated States Of)</asp:ListItem>
                                <asp:ListItem Value="MDA">Moldova, Republic Of</asp:ListItem>
                                <asp:ListItem Value="MCO">Monaco</asp:ListItem>
                                <asp:ListItem Value="MNG">Mongolia</asp:ListItem>
                                <asp:ListItem Value="MSR">Montserrat</asp:ListItem>
                                <asp:ListItem Value="MAR">Morocco</asp:ListItem>
                                <asp:ListItem Value="MOZ">Mozambique</asp:ListItem>
                                <asp:ListItem Value="MMR">Myanmar</asp:ListItem>
                                <asp:ListItem Value="NAM">Namibia</asp:ListItem>
                                <asp:ListItem Value="NRU">Nauru</asp:ListItem>
                                <asp:ListItem Value="NPL">Nepal</asp:ListItem>
                                <asp:ListItem Value="NLD">Netherlands</asp:ListItem>
                                <asp:ListItem Value="ANT">Netherlands Antilles</asp:ListItem>
                                <asp:ListItem Value="NCL">New Caledonia</asp:ListItem>
                                <asp:ListItem Value="NZL">New Zealand</asp:ListItem>
                                <asp:ListItem Value="NIC">Nicaragua</asp:ListItem>
                                <asp:ListItem Value="NER">Niger</asp:ListItem>
                                <asp:ListItem Value="NGA">Nigeria</asp:ListItem>
                                <asp:ListItem Value="NIU">Niue</asp:ListItem>
                                <asp:ListItem Value="NFK">Norfolk Island</asp:ListItem>
                                <asp:ListItem Value="MNP">Northern Mariana Islands</asp:ListItem>
                                <asp:ListItem Value="NOR">Norway</asp:ListItem>
                                <asp:ListItem Value="OMN">Oman</asp:ListItem>
                                <asp:ListItem Value="PAK">Pakistan</asp:ListItem>
                                <asp:ListItem Value="PLW">Palau</asp:ListItem>
                                <asp:ListItem Value="PAN">Panama</asp:ListItem>
                                <asp:ListItem Value="PNG">Papua New Guinea</asp:ListItem>
                                <asp:ListItem Value="PRY">Paraguay</asp:ListItem>
                                <asp:ListItem Value="PER">Peru</asp:ListItem>
                                <asp:ListItem Value="PHL">Philippines</asp:ListItem>
                                <asp:ListItem Value="PCN">Pitcairn</asp:ListItem>
                                <asp:ListItem Value="POL">Poland</asp:ListItem>
                                <asp:ListItem Value="PRT">Portugal</asp:ListItem>
                                <asp:ListItem Value="PRI">Puerto Rico</asp:ListItem>
                                <asp:ListItem Value="QAT">Qatar</asp:ListItem>
                                <asp:ListItem Value="REU">Reunion</asp:ListItem>
                                <asp:ListItem Value="ROM">Romania</asp:ListItem>
                                <asp:ListItem Value="RUS">Russian Federation</asp:ListItem>
                                <asp:ListItem Value="RWA">Rwanda</asp:ListItem>
                                <asp:ListItem Value="SHN">St. Helena</asp:ListItem>
                                <asp:ListItem Value="KNA">Saint Kitts And Nevis</asp:ListItem>
                                <asp:ListItem Value="LCA">Saint Lucia</asp:ListItem>
                                <asp:ListItem Value="SPM">St. Pierre And Miquelon</asp:ListItem>
                                <asp:ListItem Value="VCT">Saint Vincent And The Grenadines</asp:ListItem>
                                <asp:ListItem Value="WSM">Samoa</asp:ListItem>
                                <asp:ListItem Value="SMR">San Marino</asp:ListItem>
                                <asp:ListItem Value="STP">Sao Tome And Principe</asp:ListItem>
                                <asp:ListItem Value="SAU">Saudi Arabia</asp:ListItem>
                                <asp:ListItem Value="SEN">Senegal</asp:ListItem>
                                <asp:ListItem Value="SYC">Seychelles</asp:ListItem>
                                <asp:ListItem Value="SLE">Sierra Leone</asp:ListItem>
                                <asp:ListItem Value="SGP">Singapore</asp:ListItem>
                                <asp:ListItem Value="SVK">Slovakia</asp:ListItem>
                                <asp:ListItem Value="SVN">Slovenia</asp:ListItem>
                                <asp:ListItem Value="SLB">Solomon Islands</asp:ListItem>
                                <asp:ListItem Value="SOM">Somalia</asp:ListItem>
                                <asp:ListItem Value="ZAF">South Africa</asp:ListItem>
                                <asp:ListItem Value="SGS">South Georgia And The South Sandwich Islands</asp:ListItem>
                                <asp:ListItem Value="ESP">Spain</asp:ListItem>
                                <asp:ListItem Value="LKA">Sri Lanka</asp:ListItem>
                                <asp:ListItem Value="SDN">Sudan</asp:ListItem>
                                <asp:ListItem Value="SUR">Suriname</asp:ListItem>
                                <asp:ListItem Value="SJM">Svalbard And Jan Mayen Islands</asp:ListItem>
                                <asp:ListItem Value="SWZ">Swaziland</asp:ListItem>
                                <asp:ListItem Value="SWE">Sweden</asp:ListItem>
                                <asp:ListItem Value="CHE">Switzerland</asp:ListItem>
                                <asp:ListItem Value="SYR">Syrian Arab Republic</asp:ListItem>
                                <asp:ListItem Value="TWN">Taiwan, Province Of China</asp:ListItem>
                                <asp:ListItem Value="TJK">Tajikistan</asp:ListItem>
                                <asp:ListItem Value="TZA">Tanzania, United Republic Of</asp:ListItem>
                                <asp:ListItem Value="THA">Thailand</asp:ListItem>
                                <asp:ListItem Value="TGO">Togo</asp:ListItem>
                                <asp:ListItem Value="TKL">Tokelau</asp:ListItem>
                                <asp:ListItem Value="TON">Tonga</asp:ListItem>
                                <asp:ListItem Value="TTO">Trinidad And Tobago</asp:ListItem>
                                <asp:ListItem Value="TUN">Tunisia</asp:ListItem>
                                <asp:ListItem Value="TUR">Turkey</asp:ListItem>
                                <asp:ListItem Value="TKM">Turkmenistan</asp:ListItem>
                                <asp:ListItem Value="TCA">Turks And Caicos Islands</asp:ListItem>
                                <asp:ListItem Value="TUV">Tuvalu</asp:ListItem>
                                <asp:ListItem Value="UGA">Uganda</asp:ListItem>
                                <asp:ListItem Value="UKR">Ukraine</asp:ListItem>
                                <asp:ListItem Value="ARE">United Arab Emirates</asp:ListItem>
                                <asp:ListItem Value="GBR">United Kingdom</asp:ListItem>
                                <asp:ListItem Value="USA">United States</asp:ListItem>
                                <asp:ListItem Value="UMI">United States Minor Outlying Islands</asp:ListItem>
                                <asp:ListItem Value="URY">Uruguay</asp:ListItem>
                                <asp:ListItem Value="UZB">Uzbekistan</asp:ListItem>
                                <asp:ListItem Value="VUT">Vanuatu</asp:ListItem>
                                <asp:ListItem Value="VAT">Vatican City State (Holy See)</asp:ListItem>
                                <asp:ListItem Value="VEN">Venezuela</asp:ListItem>
                                <asp:ListItem Value="VNM">Viet Nam</asp:ListItem>
                                <asp:ListItem Value="VGB">Virgin Islands (British)</asp:ListItem>
                                <asp:ListItem Value="VIR">Virgin Islands (U.S.)</asp:ListItem>
                                <asp:ListItem Value="WLF">Wallis And Futuna Islands</asp:ListItem>
                                <asp:ListItem Value="ESH">Western Sahara</asp:ListItem>
                                <asp:ListItem Value="YEM">Yemen</asp:ListItem>
                                <asp:ListItem Value="YUG">Yugoslavia</asp:ListItem>
                                <asp:ListItem Value="ZAR">Zaire</asp:ListItem>
                                <asp:ListItem Value="ZMB">Zambia</asp:ListItem>
                                <asp:ListItem Value="ZWE">Zimbabwe</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Ship-To Fullname: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ShipTo_Fullname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Ship-to  Title: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ShipTo_Title" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Ship-to  Firstname: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ShipTo_Firstname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Ship-to  Middlename: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ShipTo_Middlename" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Ship-to  Lastname: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ShipTo_Lastname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Ship-to Phone Number: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ShipTo_Phone" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Ship-to Street/PO Box Address: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ShipTo_Street01" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Ship-to City/Town: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ShipTo_City" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Ship-to State/Province: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ShipTo_StateProv" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Ship-to Zip/Post Code: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ShipTo_PostCode" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Ship-to Country of Issue: </em></strong></td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="vpc_ShipTo_Country" runat="server">
                                <asp:ListItem Value="">Please Select</asp:ListItem>
                                <asp:ListItem Value="AFG">Afghanistan</asp:ListItem>
                                <asp:ListItem Value="ALB">Albania</asp:ListItem>
                                <asp:ListItem Value="DZA">Algeria</asp:ListItem>
                                <asp:ListItem Value="ASM">American Samoa</asp:ListItem>
                                <asp:ListItem Value="AND">Andorra</asp:ListItem>
                                <asp:ListItem Value="AGO">Angola</asp:ListItem>
                                <asp:ListItem Value="AIA">Anguilla</asp:ListItem>
                                <asp:ListItem Value="ATA">Antarctica</asp:ListItem>
                                <asp:ListItem Value="ATG">Antigua And Barbuda</asp:ListItem>
                                <asp:ListItem Value="ARG">Argentina</asp:ListItem>
                                <asp:ListItem Value="ARM">Armenia</asp:ListItem>
                                <asp:ListItem Value="ABW">Aruba</asp:ListItem>
                                <asp:ListItem Value="AUS">Australia</asp:ListItem>
                                <asp:ListItem Value="AUT">Austria</asp:ListItem>
                                <asp:ListItem Value="AZE">Azerbaijan</asp:ListItem>
                                <asp:ListItem Value="BHS">Bahamas</asp:ListItem>
                                <asp:ListItem Value="BHR">Bahrain</asp:ListItem>
                                <asp:ListItem Value="BGD">Bangladesh</asp:ListItem>
                                <asp:ListItem Value="BRB">Barbados</asp:ListItem>
                                <asp:ListItem Value="BLR">Belarus</asp:ListItem>
                                <asp:ListItem Value="BEL">Belgium</asp:ListItem>
                                <asp:ListItem Value="BLZ">Belize</asp:ListItem>
                                <asp:ListItem Value="BEN">Benin</asp:ListItem>
                                <asp:ListItem Value="BMU">Bermuda</asp:ListItem>
                                <asp:ListItem Value="BTN">Bhutan</asp:ListItem>
                                <asp:ListItem Value="BOL">Bolivia</asp:ListItem>
                                <asp:ListItem Value="BIH">Bosnia And Herzegowina</asp:ListItem>
                                <asp:ListItem Value="BWA">Botswana</asp:ListItem>
                                <asp:ListItem Value="BVT">Bouvet Island</asp:ListItem>
                                <asp:ListItem Value="BRA">Brazil</asp:ListItem>
                                <asp:ListItem Value="IOT">British Indian Ocean Territory</asp:ListItem>
                                <asp:ListItem Value="BRN">Brunei Darussalam</asp:ListItem>
                                <asp:ListItem Value="BGR">Bulgaria</asp:ListItem>
                                <asp:ListItem Value="BFA">Burkina Faso</asp:ListItem>
                                <asp:ListItem Value="BDI">Burundi</asp:ListItem>
                                <asp:ListItem Value="KHM">Cambodia</asp:ListItem>
                                <asp:ListItem Value="CMR">Cameroon</asp:ListItem>
                                <asp:ListItem Value="CAN">Canada</asp:ListItem>
                                <asp:ListItem Value="CPV">Cape Verde</asp:ListItem>
                                <asp:ListItem Value="CYM">Cayman Islands</asp:ListItem>
                                <asp:ListItem Value="CAF">Central African Republic</asp:ListItem>
                                <asp:ListItem Value="TCD">Chad</asp:ListItem>
                                <asp:ListItem Value="CHL">Chile</asp:ListItem>
                                <asp:ListItem Value="CHN">China</asp:ListItem>
                                <asp:ListItem Value="CXR">Christmas Island</asp:ListItem>
                                <asp:ListItem Value="CCK">Cocos (Keeling) Islands</asp:ListItem>
                                <asp:ListItem Value="COL">Colombia</asp:ListItem>
                                <asp:ListItem Value="COM">Comoros</asp:ListItem>
                                <asp:ListItem Value="COG">Congo</asp:ListItem>
                                <asp:ListItem Value="COK">Cook Islands</asp:ListItem>
                                <asp:ListItem Value="CRI">Costa Rica</asp:ListItem>
                                <asp:ListItem Value="CIV">Cote D'ivoire</asp:ListItem>
                                <asp:ListItem Value="HRV">Croatia</asp:ListItem>
                                <asp:ListItem Value="CUB">Cuba</asp:ListItem>
                                <asp:ListItem Value="CYP">Cyprus</asp:ListItem>
                                <asp:ListItem Value="CZE">Czech Republic</asp:ListItem>
                                <asp:ListItem Value="DNK">Denmark</asp:ListItem>
                                <asp:ListItem Value="DJI">Djibouti</asp:ListItem>
                                <asp:ListItem Value="DMA">Dominica</asp:ListItem>
                                <asp:ListItem Value="DOM">Dominican Republic</asp:ListItem>
                                <asp:ListItem Value="TMP">East Timor</asp:ListItem>
                                <asp:ListItem Value="ECU">Ecuador</asp:ListItem>
                                <asp:ListItem Value="EGY">Egypt</asp:ListItem>
                                <asp:ListItem Value="SLV">El Salvador</asp:ListItem>
                                <asp:ListItem Value="GNQ">Equatorial Guinea</asp:ListItem>
                                <asp:ListItem Value="ERI">Eritrea</asp:ListItem>
                                <asp:ListItem Value="EST">Estonia</asp:ListItem>
                                <asp:ListItem Value="ETH">Ethiopia</asp:ListItem>
                                <asp:ListItem Value="FLK">Falkland Islands (Malvinas)</asp:ListItem>
                                <asp:ListItem Value="FRO">Faroe Islands</asp:ListItem>
                                <asp:ListItem Value="FJI">Fiji</asp:ListItem>
                                <asp:ListItem Value="FIN">Finland</asp:ListItem>
                                <asp:ListItem Value="FRA">France</asp:ListItem>
                                <asp:ListItem Value="FXX">France, Metropolitan</asp:ListItem>
                                <asp:ListItem Value="GUF">French Guiana</asp:ListItem>
                                <asp:ListItem Value="PYF">French Polynesia</asp:ListItem>
                                <asp:ListItem Value="ATF">French Southern Territories</asp:ListItem>
                                <asp:ListItem Value="GAB">Gabon</asp:ListItem>
                                <asp:ListItem Value="GMB">Gambia</asp:ListItem>
                                <asp:ListItem Value="GEO">Georgia</asp:ListItem>
                                <asp:ListItem Value="DEU">Germany</asp:ListItem>
                                <asp:ListItem Value="GHA">Ghana</asp:ListItem>
                                <asp:ListItem Value="GIB">Gibraltar</asp:ListItem>
                                <asp:ListItem Value="GRC">Greece</asp:ListItem>
                                <asp:ListItem Value="GRL">Greenland</asp:ListItem>
                                <asp:ListItem Value="GRD">Grenada</asp:ListItem>
                                <asp:ListItem Value="GLP">Guadeloupe</asp:ListItem>
                                <asp:ListItem Value="GUM">Guam</asp:ListItem>
                                <asp:ListItem Value="GTM">Guatemala</asp:ListItem>
                                <asp:ListItem Value="GIN">Guinea</asp:ListItem>
                                <asp:ListItem Value="GNB">Guinea-Bissau</asp:ListItem>
                                <asp:ListItem Value="GUY">Guyana</asp:ListItem>
                                <asp:ListItem Value="HTI">Haiti</asp:ListItem>
                                <asp:ListItem Value="HMD">Heard And Mc Donald Islands</asp:ListItem>
                                <asp:ListItem Value="HND">Honduras</asp:ListItem>
                                <asp:ListItem Value="HKG">Hong Kong</asp:ListItem>
                                <asp:ListItem Value="HUN">Hungary</asp:ListItem>
                                <asp:ListItem Value="ISL">Iceland</asp:ListItem>
                                <asp:ListItem Value="IND">India</asp:ListItem>
                                <asp:ListItem Value="IDN">Indonesia</asp:ListItem>
                                <asp:ListItem Value="IRN">Iran (Islamic Republic Of)</asp:ListItem>
                                <asp:ListItem Value="IRQ">Iraq</asp:ListItem>
                                <asp:ListItem Value="IRL">Ireland</asp:ListItem>
                                <asp:ListItem Value="ISR">Israel</asp:ListItem>
                                <asp:ListItem Value="ITA">Italy</asp:ListItem>
                                <asp:ListItem Value="JAM">Jamaica</asp:ListItem>
                                <asp:ListItem Value="JPN">Japan</asp:ListItem>
                                <asp:ListItem Value="JOR">Jordan</asp:ListItem>
                                <asp:ListItem Value="KAZ">Kazakhstan</asp:ListItem>
                                <asp:ListItem Value="KEN">Kenya</asp:ListItem>
                                <asp:ListItem Value="KIR">Kiribati</asp:ListItem>
                                <asp:ListItem Value="PRK">Korea, Democratic People's Republic Of</asp:ListItem>
                                <asp:ListItem Value="KOR">Korea, Republic Of</asp:ListItem>
                                <asp:ListItem Value="KWT">Kuwait</asp:ListItem>
                                <asp:ListItem Value="KGZ">Kyrgyzstan</asp:ListItem>
                                <asp:ListItem Value="LAO">Lao People's Democratic Republic</asp:ListItem>
                                <asp:ListItem Value="LVA">Latvia</asp:ListItem>
                                <asp:ListItem Value="LBN">Lebanon</asp:ListItem>
                                <asp:ListItem Value="LSO">Lesotho</asp:ListItem>
                                <asp:ListItem Value="LBR">Liberia</asp:ListItem>
                                <asp:ListItem Value="LBY">Libyan Arab Jamahiriya</asp:ListItem>
                                <asp:ListItem Value="LIE">Liechtenstein</asp:ListItem>
                                <asp:ListItem Value="LTU">Lithuania</asp:ListItem>
                                <asp:ListItem Value="LUX">Luxembourg</asp:ListItem>
                                <asp:ListItem Value="MAC">Macau</asp:ListItem>
                                <asp:ListItem Value="MKD">Macedonia, The Former Yugoslav Republic Of</asp:ListItem>
                                <asp:ListItem Value="MDG">Madagascar</asp:ListItem>
                                <asp:ListItem Value="MWI">Malawi</asp:ListItem>
                                <asp:ListItem Value="MYS">Malaysia</asp:ListItem>
                                <asp:ListItem Value="MDV">Maldives</asp:ListItem>
                                <asp:ListItem Value="MLI">Mali</asp:ListItem>
                                <asp:ListItem Value="MLT">Malta</asp:ListItem>
                                <asp:ListItem Value="MHL">Marshall Islands</asp:ListItem>
                                <asp:ListItem Value="MTQ">Martinique</asp:ListItem>
                                <asp:ListItem Value="MRT">Mauritania</asp:ListItem>
                                <asp:ListItem Value="MUS">Mauritius</asp:ListItem>
                                <asp:ListItem Value="MYT">Mayotte</asp:ListItem>
                                <asp:ListItem Value="MEX">Mexico</asp:ListItem>
                                <asp:ListItem Value="FSM">Micronesia (Federated States Of)</asp:ListItem>
                                <asp:ListItem Value="MDA">Moldova, Republic Of</asp:ListItem>
                                <asp:ListItem Value="MCO">Monaco</asp:ListItem>
                                <asp:ListItem Value="MNG">Mongolia</asp:ListItem>
                                <asp:ListItem Value="MSR">Montserrat</asp:ListItem>
                                <asp:ListItem Value="MAR">Morocco</asp:ListItem>
                                <asp:ListItem Value="MOZ">Mozambique</asp:ListItem>
                                <asp:ListItem Value="MMR">Myanmar</asp:ListItem>
                                <asp:ListItem Value="NAM">Namibia</asp:ListItem>
                                <asp:ListItem Value="NRU">Nauru</asp:ListItem>
                                <asp:ListItem Value="NPL">Nepal</asp:ListItem>
                                <asp:ListItem Value="NLD">Netherlands</asp:ListItem>
                                <asp:ListItem Value="ANT">Netherlands Antilles</asp:ListItem>
                                <asp:ListItem Value="NCL">New Caledonia</asp:ListItem>
                                <asp:ListItem Value="NZL">New Zealand</asp:ListItem>
                                <asp:ListItem Value="NIC">Nicaragua</asp:ListItem>
                                <asp:ListItem Value="NER">Niger</asp:ListItem>
                                <asp:ListItem Value="NGA">Nigeria</asp:ListItem>
                                <asp:ListItem Value="NIU">Niue</asp:ListItem>
                                <asp:ListItem Value="NFK">Norfolk Island</asp:ListItem>
                                <asp:ListItem Value="MNP">Northern Mariana Islands</asp:ListItem>
                                <asp:ListItem Value="NOR">Norway</asp:ListItem>
                                <asp:ListItem Value="OMN">Oman</asp:ListItem>
                                <asp:ListItem Value="PAK">Pakistan</asp:ListItem>
                                <asp:ListItem Value="PLW">Palau</asp:ListItem>
                                <asp:ListItem Value="PAN">Panama</asp:ListItem>
                                <asp:ListItem Value="PNG">Papua New Guinea</asp:ListItem>
                                <asp:ListItem Value="PRY">Paraguay</asp:ListItem>
                                <asp:ListItem Value="PER">Peru</asp:ListItem>
                                <asp:ListItem Value="PHL">Philippines</asp:ListItem>
                                <asp:ListItem Value="PCN">Pitcairn</asp:ListItem>
                                <asp:ListItem Value="POL">Poland</asp:ListItem>
                                <asp:ListItem Value="PRT">Portugal</asp:ListItem>
                                <asp:ListItem Value="PRI">Puerto Rico</asp:ListItem>
                                <asp:ListItem Value="QAT">Qatar</asp:ListItem>
                                <asp:ListItem Value="REU">Reunion</asp:ListItem>
                                <asp:ListItem Value="ROM">Romania</asp:ListItem>
                                <asp:ListItem Value="RUS">Russian Federation</asp:ListItem>
                                <asp:ListItem Value="RWA">Rwanda</asp:ListItem>
                                <asp:ListItem Value="SHN">St. Helena</asp:ListItem>
                                <asp:ListItem Value="KNA">Saint Kitts And Nevis</asp:ListItem>
                                <asp:ListItem Value="LCA">Saint Lucia</asp:ListItem>
                                <asp:ListItem Value="SPM">St. Pierre And Miquelon</asp:ListItem>
                                <asp:ListItem Value="VCT">Saint Vincent And The Grenadines</asp:ListItem>
                                <asp:ListItem Value="WSM">Samoa</asp:ListItem>
                                <asp:ListItem Value="SMR">San Marino</asp:ListItem>
                                <asp:ListItem Value="STP">Sao Tome And Principe</asp:ListItem>
                                <asp:ListItem Value="SAU">Saudi Arabia</asp:ListItem>
                                <asp:ListItem Value="SEN">Senegal</asp:ListItem>
                                <asp:ListItem Value="SYC">Seychelles</asp:ListItem>
                                <asp:ListItem Value="SLE">Sierra Leone</asp:ListItem>
                                <asp:ListItem Value="SGP">Singapore</asp:ListItem>
                                <asp:ListItem Value="SVK">Slovakia</asp:ListItem>
                                <asp:ListItem Value="SVN">Slovenia</asp:ListItem>
                                <asp:ListItem Value="SLB">Solomon Islands</asp:ListItem>
                                <asp:ListItem Value="SOM">Somalia</asp:ListItem>
                                <asp:ListItem Value="ZAF">South Africa</asp:ListItem>
                                <asp:ListItem Value="SGS">South Georgia And The South Sandwich Islands</asp:ListItem>
                                <asp:ListItem Value="ESP">Spain</asp:ListItem>
                                <asp:ListItem Value="LKA">Sri Lanka</asp:ListItem>
                                <asp:ListItem Value="SDN">Sudan</asp:ListItem>
                                <asp:ListItem Value="SUR">Suriname</asp:ListItem>
                                <asp:ListItem Value="SJM">Svalbard And Jan Mayen Islands</asp:ListItem>
                                <asp:ListItem Value="SWZ">Swaziland</asp:ListItem>
                                <asp:ListItem Value="SWE">Sweden</asp:ListItem>
                                <asp:ListItem Value="CHE">Switzerland</asp:ListItem>
                                <asp:ListItem Value="SYR">Syrian Arab Republic</asp:ListItem>
                                <asp:ListItem Value="TWN">Taiwan, Province Of China</asp:ListItem>
                                <asp:ListItem Value="TJK">Tajikistan</asp:ListItem>
                                <asp:ListItem Value="TZA">Tanzania, United Republic Of</asp:ListItem>
                                <asp:ListItem Value="THA">Thailand</asp:ListItem>
                                <asp:ListItem Value="TGO">Togo</asp:ListItem>
                                <asp:ListItem Value="TKL">Tokelau</asp:ListItem>
                                <asp:ListItem Value="TON">Tonga</asp:ListItem>
                                <asp:ListItem Value="TTO">Trinidad And Tobago</asp:ListItem>
                                <asp:ListItem Value="TUN">Tunisia</asp:ListItem>
                                <asp:ListItem Value="TUR">Turkey</asp:ListItem>
                                <asp:ListItem Value="TKM">Turkmenistan</asp:ListItem>
                                <asp:ListItem Value="TCA">Turks And Caicos Islands</asp:ListItem>
                                <asp:ListItem Value="TUV">Tuvalu</asp:ListItem>
                                <asp:ListItem Value="UGA">Uganda</asp:ListItem>
                                <asp:ListItem Value="UKR">Ukraine</asp:ListItem>
                                <asp:ListItem Value="ARE">United Arab Emirates</asp:ListItem>
                                <asp:ListItem Value="GBR">United Kingdom</asp:ListItem>
                                <asp:ListItem Value="USA">United States</asp:ListItem>
                                <asp:ListItem Value="UMI">United States Minor Outlying Islands</asp:ListItem>
                                <asp:ListItem Value="URY">Uruguay</asp:ListItem>
                                <asp:ListItem Value="UZB">Uzbekistan</asp:ListItem>
                                <asp:ListItem Value="VUT">Vanuatu</asp:ListItem>
                                <asp:ListItem Value="VAT">Vatican City State (Holy See)</asp:ListItem>
                                <asp:ListItem Value="VEN">Venezuela</asp:ListItem>
                                <asp:ListItem Value="VNM">Viet Nam</asp:ListItem>
                                <asp:ListItem Value="VGB">Virgin Islands (British)</asp:ListItem>
                                <asp:ListItem Value="VIR">Virgin Islands (U.S.)</asp:ListItem>
                                <asp:ListItem Value="WLF">Wallis And Futuna Islands</asp:ListItem>
                                <asp:ListItem Value="ESH">Western Sahara</asp:ListItem>
                                <asp:ListItem Value="YEM">Yemen</asp:ListItem>
                                <asp:ListItem Value="YUG">Yugoslavia</asp:ListItem>
                                <asp:ListItem Value="ZAR">Zaire</asp:ListItem>
                                <asp:ListItem Value="ZMB">Zambia</asp:ListItem>
                                <asp:ListItem Value="ZWE">Zimbabwe</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                    <tr class="title">
                        <td colspan="2" style="text-align: left"><p><strong>&nbsp;Optional Airline Passenger Data Fields</strong></p></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            You cannot send both (APD and ITD) together for a single transaction. One or the other, but not both.
                            <br />An error will be thrown it you try and send both.
                        </td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Departure Date: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_DeptDate" runat="server"> </asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Passenger Title: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_PassengerTitle" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Passenger Firstname: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_PassengerFirstname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Passenger Middlename: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_PassengerMiddlename" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Passenger Lastname: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_PassengerLastname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Cardmember Title: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_CardmemberTitle" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Cardmember Firstname: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_CardmemberFirstname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Cardmember Middlename: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_CardmemberMiddlename" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Cardmember Lastname: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_CardmemberLastname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Origin: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_Origin" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Destination: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_Dest" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Routing Cities: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_Route" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Airline Carriers: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_Carriers" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Fare Basis: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_FareBasis" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Number of Passengers: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_NumPassengers" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>E-Ticket Indicator: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_eTicket" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Reservation Code: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_ResCode" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>IATA (Travel Agent Code): </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_APD_TravelAgentCode" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                    <tr class="title">
                        <td colspan="2" style="text-align: left"><p><strong>&nbsp;Optional Internet Source Data Fields</strong></p></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            You cannot send both (APD and ITD) together for a single transaction. One or the other, but not both.
                            <br />An error will be thrown it you try and send both.
                        </td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Customer Email: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_CustomerEmail" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Customer Hostname: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ITD_CustomerHostname" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Http Browser Type: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ITD_CustomerBrowser" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Shipping Method Code: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ITD_ShipMethodCode" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Ship To Country Code: </em></strong></td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="vpc_ITD_ShipToCountryCode" runat="server">
                                <asp:ListItem Value="">Please Select</asp:ListItem>
                                <asp:ListItem Value="AFG">Afghanistan</asp:ListItem>
                                <asp:ListItem Value="ALB">Albania</asp:ListItem>
                                <asp:ListItem Value="DZA">Algeria</asp:ListItem>
                                <asp:ListItem Value="ASM">American Samoa</asp:ListItem>
                                <asp:ListItem Value="AND">Andorra</asp:ListItem>
                                <asp:ListItem Value="AGO">Angola</asp:ListItem>
                                <asp:ListItem Value="AIA">Anguilla</asp:ListItem>
                                <asp:ListItem Value="ATA">Antarctica</asp:ListItem>
                                <asp:ListItem Value="ATG">Antigua And Barbuda</asp:ListItem>
                                <asp:ListItem Value="ARG">Argentina</asp:ListItem>
                                <asp:ListItem Value="ARM">Armenia</asp:ListItem>
                                <asp:ListItem Value="ABW">Aruba</asp:ListItem>
                                <asp:ListItem Value="AUS">Australia</asp:ListItem>
                                <asp:ListItem Value="AUT">Austria</asp:ListItem>
                                <asp:ListItem Value="AZE">Azerbaijan</asp:ListItem>
                                <asp:ListItem Value="BHS">Bahamas</asp:ListItem>
                                <asp:ListItem Value="BHR">Bahrain</asp:ListItem>
                                <asp:ListItem Value="BGD">Bangladesh</asp:ListItem>
                                <asp:ListItem Value="BRB">Barbados</asp:ListItem>
                                <asp:ListItem Value="BLR">Belarus</asp:ListItem>
                                <asp:ListItem Value="BEL">Belgium</asp:ListItem>
                                <asp:ListItem Value="BLZ">Belize</asp:ListItem>
                                <asp:ListItem Value="BEN">Benin</asp:ListItem>
                                <asp:ListItem Value="BMU">Bermuda</asp:ListItem>
                                <asp:ListItem Value="BTN">Bhutan</asp:ListItem>
                                <asp:ListItem Value="BOL">Bolivia</asp:ListItem>
                                <asp:ListItem Value="BIH">Bosnia And Herzegowina</asp:ListItem>
                                <asp:ListItem Value="BWA">Botswana</asp:ListItem>
                                <asp:ListItem Value="BVT">Bouvet Island</asp:ListItem>
                                <asp:ListItem Value="BRA">Brazil</asp:ListItem>
                                <asp:ListItem Value="IOT">British Indian Ocean Territory</asp:ListItem>
                                <asp:ListItem Value="BRN">Brunei Darussalam</asp:ListItem>
                                <asp:ListItem Value="BGR">Bulgaria</asp:ListItem>
                                <asp:ListItem Value="BFA">Burkina Faso</asp:ListItem>
                                <asp:ListItem Value="BDI">Burundi</asp:ListItem>
                                <asp:ListItem Value="KHM">Cambodia</asp:ListItem>
                                <asp:ListItem Value="CMR">Cameroon</asp:ListItem>
                                <asp:ListItem Value="CAN">Canada</asp:ListItem>
                                <asp:ListItem Value="CPV">Cape Verde</asp:ListItem>
                                <asp:ListItem Value="CYM">Cayman Islands</asp:ListItem>
                                <asp:ListItem Value="CAF">Central African Republic</asp:ListItem>
                                <asp:ListItem Value="TCD">Chad</asp:ListItem>
                                <asp:ListItem Value="CHL">Chile</asp:ListItem>
                                <asp:ListItem Value="CHN">China</asp:ListItem>
                                <asp:ListItem Value="CXR">Christmas Island</asp:ListItem>
                                <asp:ListItem Value="CCK">Cocos (Keeling) Islands</asp:ListItem>
                                <asp:ListItem Value="COL">Colombia</asp:ListItem>
                                <asp:ListItem Value="COM">Comoros</asp:ListItem>
                                <asp:ListItem Value="COG">Congo</asp:ListItem>
                                <asp:ListItem Value="COK">Cook Islands</asp:ListItem>
                                <asp:ListItem Value="CRI">Costa Rica</asp:ListItem>
                                <asp:ListItem Value="CIV">Cote D'ivoire</asp:ListItem>
                                <asp:ListItem Value="HRV">Croatia</asp:ListItem>
                                <asp:ListItem Value="CUB">Cuba</asp:ListItem>
                                <asp:ListItem Value="CYP">Cyprus</asp:ListItem>
                                <asp:ListItem Value="CZE">Czech Republic</asp:ListItem>
                                <asp:ListItem Value="DNK">Denmark</asp:ListItem>
                                <asp:ListItem Value="DJI">Djibouti</asp:ListItem>
                                <asp:ListItem Value="DMA">Dominica</asp:ListItem>
                                <asp:ListItem Value="DOM">Dominican Republic</asp:ListItem>
                                <asp:ListItem Value="TMP">East Timor</asp:ListItem>
                                <asp:ListItem Value="ECU">Ecuador</asp:ListItem>
                                <asp:ListItem Value="EGY">Egypt</asp:ListItem>
                                <asp:ListItem Value="SLV">El Salvador</asp:ListItem>
                                <asp:ListItem Value="GNQ">Equatorial Guinea</asp:ListItem>
                                <asp:ListItem Value="ERI">Eritrea</asp:ListItem>
                                <asp:ListItem Value="EST">Estonia</asp:ListItem>
                                <asp:ListItem Value="ETH">Ethiopia</asp:ListItem>
                                <asp:ListItem Value="FLK">Falkland Islands (Malvinas)</asp:ListItem>
                                <asp:ListItem Value="FRO">Faroe Islands</asp:ListItem>
                                <asp:ListItem Value="FJI">Fiji</asp:ListItem>
                                <asp:ListItem Value="FIN">Finland</asp:ListItem>
                                <asp:ListItem Value="FRA">France</asp:ListItem>
                                <asp:ListItem Value="FXX">France, Metropolitan</asp:ListItem>
                                <asp:ListItem Value="GUF">French Guiana</asp:ListItem>
                                <asp:ListItem Value="PYF">French Polynesia</asp:ListItem>
                                <asp:ListItem Value="ATF">French Southern Territories</asp:ListItem>
                                <asp:ListItem Value="GAB">Gabon</asp:ListItem>
                                <asp:ListItem Value="GMB">Gambia</asp:ListItem>
                                <asp:ListItem Value="GEO">Georgia</asp:ListItem>
                                <asp:ListItem Value="DEU">Germany</asp:ListItem>
                                <asp:ListItem Value="GHA">Ghana</asp:ListItem>
                                <asp:ListItem Value="GIB">Gibraltar</asp:ListItem>
                                <asp:ListItem Value="GRC">Greece</asp:ListItem>
                                <asp:ListItem Value="GRL">Greenland</asp:ListItem>
                                <asp:ListItem Value="GRD">Grenada</asp:ListItem>
                                <asp:ListItem Value="GLP">Guadeloupe</asp:ListItem>
                                <asp:ListItem Value="GUM">Guam</asp:ListItem>
                                <asp:ListItem Value="GTM">Guatemala</asp:ListItem>
                                <asp:ListItem Value="GIN">Guinea</asp:ListItem>
                                <asp:ListItem Value="GNB">Guinea-Bissau</asp:ListItem>
                                <asp:ListItem Value="GUY">Guyana</asp:ListItem>
                                <asp:ListItem Value="HTI">Haiti</asp:ListItem>
                                <asp:ListItem Value="HMD">Heard And Mc Donald Islands</asp:ListItem>
                                <asp:ListItem Value="HND">Honduras</asp:ListItem>
                                <asp:ListItem Value="HKG">Hong Kong</asp:ListItem>
                                <asp:ListItem Value="HUN">Hungary</asp:ListItem>
                                <asp:ListItem Value="ISL">Iceland</asp:ListItem>
                                <asp:ListItem Value="IND">India</asp:ListItem>
                                <asp:ListItem Value="IDN">Indonesia</asp:ListItem>
                                <asp:ListItem Value="IRN">Iran (Islamic Republic Of)</asp:ListItem>
                                <asp:ListItem Value="IRQ">Iraq</asp:ListItem>
                                <asp:ListItem Value="IRL">Ireland</asp:ListItem>
                                <asp:ListItem Value="ISR">Israel</asp:ListItem>
                                <asp:ListItem Value="ITA">Italy</asp:ListItem>
                                <asp:ListItem Value="JAM">Jamaica</asp:ListItem>
                                <asp:ListItem Value="JPN">Japan</asp:ListItem>
                                <asp:ListItem Value="JOR">Jordan</asp:ListItem>
                                <asp:ListItem Value="KAZ">Kazakhstan</asp:ListItem>
                                <asp:ListItem Value="KEN">Kenya</asp:ListItem>
                                <asp:ListItem Value="KIR">Kiribati</asp:ListItem>
                                <asp:ListItem Value="PRK">Korea, Democratic People's Republic Of</asp:ListItem>
                                <asp:ListItem Value="KOR">Korea, Republic Of</asp:ListItem>
                                <asp:ListItem Value="KWT">Kuwait</asp:ListItem>
                                <asp:ListItem Value="KGZ">Kyrgyzstan</asp:ListItem>
                                <asp:ListItem Value="LAO">Lao People's Democratic Republic</asp:ListItem>
                                <asp:ListItem Value="LVA">Latvia</asp:ListItem>
                                <asp:ListItem Value="LBN">Lebanon</asp:ListItem>
                                <asp:ListItem Value="LSO">Lesotho</asp:ListItem>
                                <asp:ListItem Value="LBR">Liberia</asp:ListItem>
                                <asp:ListItem Value="LBY">Libyan Arab Jamahiriya</asp:ListItem>
                                <asp:ListItem Value="LIE">Liechtenstein</asp:ListItem>
                                <asp:ListItem Value="LTU">Lithuania</asp:ListItem>
                                <asp:ListItem Value="LUX">Luxembourg</asp:ListItem>
                                <asp:ListItem Value="MAC">Macau</asp:ListItem>
                                <asp:ListItem Value="MKD">Macedonia, The Former Yugoslav Republic Of</asp:ListItem>
                                <asp:ListItem Value="MDG">Madagascar</asp:ListItem>
                                <asp:ListItem Value="MWI">Malawi</asp:ListItem>
                                <asp:ListItem Value="MYS">Malaysia</asp:ListItem>
                                <asp:ListItem Value="MDV">Maldives</asp:ListItem>
                                <asp:ListItem Value="MLI">Mali</asp:ListItem>
                                <asp:ListItem Value="MLT">Malta</asp:ListItem>
                                <asp:ListItem Value="MHL">Marshall Islands</asp:ListItem>
                                <asp:ListItem Value="MTQ">Martinique</asp:ListItem>
                                <asp:ListItem Value="MRT">Mauritania</asp:ListItem>
                                <asp:ListItem Value="MUS">Mauritius</asp:ListItem>
                                <asp:ListItem Value="MYT">Mayotte</asp:ListItem>
                                <asp:ListItem Value="MEX">Mexico</asp:ListItem>
                                <asp:ListItem Value="FSM">Micronesia (Federated States Of)</asp:ListItem>
                                <asp:ListItem Value="MDA">Moldova, Republic Of</asp:ListItem>
                                <asp:ListItem Value="MCO">Monaco</asp:ListItem>
                                <asp:ListItem Value="MNG">Mongolia</asp:ListItem>
                                <asp:ListItem Value="MSR">Montserrat</asp:ListItem>
                                <asp:ListItem Value="MAR">Morocco</asp:ListItem>
                                <asp:ListItem Value="MOZ">Mozambique</asp:ListItem>
                                <asp:ListItem Value="MMR">Myanmar</asp:ListItem>
                                <asp:ListItem Value="NAM">Namibia</asp:ListItem>
                                <asp:ListItem Value="NRU">Nauru</asp:ListItem>
                                <asp:ListItem Value="NPL">Nepal</asp:ListItem>
                                <asp:ListItem Value="NLD">Netherlands</asp:ListItem>
                                <asp:ListItem Value="ANT">Netherlands Antilles</asp:ListItem>
                                <asp:ListItem Value="NCL">New Caledonia</asp:ListItem>
                                <asp:ListItem Value="NZL">New Zealand</asp:ListItem>
                                <asp:ListItem Value="NIC">Nicaragua</asp:ListItem>
                                <asp:ListItem Value="NER">Niger</asp:ListItem>
                                <asp:ListItem Value="NGA">Nigeria</asp:ListItem>
                                <asp:ListItem Value="NIU">Niue</asp:ListItem>
                                <asp:ListItem Value="NFK">Norfolk Island</asp:ListItem>
                                <asp:ListItem Value="MNP">Northern Mariana Islands</asp:ListItem>
                                <asp:ListItem Value="NOR">Norway</asp:ListItem>
                                <asp:ListItem Value="OMN">Oman</asp:ListItem>
                                <asp:ListItem Value="PAK">Pakistan</asp:ListItem>
                                <asp:ListItem Value="PLW">Palau</asp:ListItem>
                                <asp:ListItem Value="PAN">Panama</asp:ListItem>
                                <asp:ListItem Value="PNG">Papua New Guinea</asp:ListItem>
                                <asp:ListItem Value="PRY">Paraguay</asp:ListItem>
                                <asp:ListItem Value="PER">Peru</asp:ListItem>
                                <asp:ListItem Value="PHL">Philippines</asp:ListItem>
                                <asp:ListItem Value="PCN">Pitcairn</asp:ListItem>
                                <asp:ListItem Value="POL">Poland</asp:ListItem>
                                <asp:ListItem Value="PRT">Portugal</asp:ListItem>
                                <asp:ListItem Value="PRI">Puerto Rico</asp:ListItem>
                                <asp:ListItem Value="QAT">Qatar</asp:ListItem>
                                <asp:ListItem Value="REU">Reunion</asp:ListItem>
                                <asp:ListItem Value="ROM">Romania</asp:ListItem>
                                <asp:ListItem Value="RUS">Russian Federation</asp:ListItem>
                                <asp:ListItem Value="RWA">Rwanda</asp:ListItem>
                                <asp:ListItem Value="SHN">St. Helena</asp:ListItem>
                                <asp:ListItem Value="KNA">Saint Kitts And Nevis</asp:ListItem>
                                <asp:ListItem Value="LCA">Saint Lucia</asp:ListItem>
                                <asp:ListItem Value="SPM">St. Pierre And Miquelon</asp:ListItem>
                                <asp:ListItem Value="VCT">Saint Vincent And The Grenadines</asp:ListItem>
                                <asp:ListItem Value="WSM">Samoa</asp:ListItem>
                                <asp:ListItem Value="SMR">San Marino</asp:ListItem>
                                <asp:ListItem Value="STP">Sao Tome And Principe</asp:ListItem>
                                <asp:ListItem Value="SAU">Saudi Arabia</asp:ListItem>
                                <asp:ListItem Value="SEN">Senegal</asp:ListItem>
                                <asp:ListItem Value="SYC">Seychelles</asp:ListItem>
                                <asp:ListItem Value="SLE">Sierra Leone</asp:ListItem>
                                <asp:ListItem Value="SGP">Singapore</asp:ListItem>
                                <asp:ListItem Value="SVK">Slovakia</asp:ListItem>
                                <asp:ListItem Value="SVN">Slovenia</asp:ListItem>
                                <asp:ListItem Value="SLB">Solomon Islands</asp:ListItem>
                                <asp:ListItem Value="SOM">Somalia</asp:ListItem>
                                <asp:ListItem Value="ZAF">South Africa</asp:ListItem>
                                <asp:ListItem Value="SGS">South Georgia And The South Sandwich Islands</asp:ListItem>
                                <asp:ListItem Value="ESP">Spain</asp:ListItem>
                                <asp:ListItem Value="LKA">Sri Lanka</asp:ListItem>
                                <asp:ListItem Value="SDN">Sudan</asp:ListItem>
                                <asp:ListItem Value="SUR">Suriname</asp:ListItem>
                                <asp:ListItem Value="SJM">Svalbard And Jan Mayen Islands</asp:ListItem>
                                <asp:ListItem Value="SWZ">Swaziland</asp:ListItem>
                                <asp:ListItem Value="SWE">Sweden</asp:ListItem>
                                <asp:ListItem Value="CHE">Switzerland</asp:ListItem>
                                <asp:ListItem Value="SYR">Syrian Arab Republic</asp:ListItem>
                                <asp:ListItem Value="TWN">Taiwan, Province Of China</asp:ListItem>
                                <asp:ListItem Value="TJK">Tajikistan</asp:ListItem>
                                <asp:ListItem Value="TZA">Tanzania, United Republic Of</asp:ListItem>
                                <asp:ListItem Value="THA">Thailand</asp:ListItem>
                                <asp:ListItem Value="TGO">Togo</asp:ListItem>
                                <asp:ListItem Value="TKL">Tokelau</asp:ListItem>
                                <asp:ListItem Value="TON">Tonga</asp:ListItem>
                                <asp:ListItem Value="TTO">Trinidad And Tobago</asp:ListItem>
                                <asp:ListItem Value="TUN">Tunisia</asp:ListItem>
                                <asp:ListItem Value="TUR">Turkey</asp:ListItem>
                                <asp:ListItem Value="TKM">Turkmenistan</asp:ListItem>
                                <asp:ListItem Value="TCA">Turks And Caicos Islands</asp:ListItem>
                                <asp:ListItem Value="TUV">Tuvalu</asp:ListItem>
                                <asp:ListItem Value="UGA">Uganda</asp:ListItem>
                                <asp:ListItem Value="UKR">Ukraine</asp:ListItem>
                                <asp:ListItem Value="ARE">United Arab Emirates</asp:ListItem>
                                <asp:ListItem Value="GBR">United Kingdom</asp:ListItem>
                                <asp:ListItem Value="USA">United States</asp:ListItem>
                                <asp:ListItem Value="UMI">United States Minor Outlying Islands</asp:ListItem>
                                <asp:ListItem Value="URY">Uruguay</asp:ListItem>
                                <asp:ListItem Value="UZB">Uzbekistan</asp:ListItem>
                                <asp:ListItem Value="VUT">Vanuatu</asp:ListItem>
                                <asp:ListItem Value="VAT">Vatican City State (Holy See)</asp:ListItem>
                                <asp:ListItem Value="VEN">Venezuela</asp:ListItem>
                                <asp:ListItem Value="VNM">Viet Nam</asp:ListItem>
                                <asp:ListItem Value="VGB">Virgin Islands (British)</asp:ListItem>
                                <asp:ListItem Value="VIR">Virgin Islands (U.S.)</asp:ListItem>
                                <asp:ListItem Value="WLF">Wallis And Futuna Islands</asp:ListItem>
                                <asp:ListItem Value="ESH">Western Sahara</asp:ListItem>
                                <asp:ListItem Value="YEM">Yemen</asp:ListItem>
                                <asp:ListItem Value="YUG">Yugoslavia</asp:ListItem>
                                <asp:ListItem Value="ZAR">Zaire</asp:ListItem>
                                <asp:ListItem Value="ZMB">Zambia</asp:ListItem>
                                <asp:ListItem Value="ZWE">Zimbabwe</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Merchant Product SKU (Stock Keeping Unit): </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ITD_MerchantSKU" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Customer IP Address: </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_CustomerIpAddress" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Customer ANI (Automatic Number Identiciation): </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ITD_CustomerANI" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>Customer II Digitits (caller type code): </em></strong></td>
                        <td style="text-align: left"><asp:TextBox ID="vpc_ITD_CustomerANICallType" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="height: 21px"></td>
                        <td style="height: 21px"></td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlError" runat="server" Style="margin: 0 auto; width: 719px;">
            <table style="margin-right: auto; margin-left:auto; border-width: 0; padding: 5px; width: 719px;">
                <tbody>
                    <tr class="title" style="text-align: left">
                        <td colspan="2"><p><strong>&nbsp;Error Information</strong></p></td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 149px;"><strong><em>Error Message: </em></strong></td>
                        <td style="text-align: left; width: 650px"><asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="height: 21px" />
                        <td style="height: 21px" />
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
        &nbsp;
    </div>
    </form>
    <div style="text-align: center;">
        <asp:Panel ID="pnlVersionInformation" runat="server" Style="margin: 0 auto; width: 719px;">
            <table style="margin-right: auto; margin-left:auto; border-width: 0; padding: 5px; width: 719px;">
                <tbody>
                    <tr class="title" style="text-align: left">
                        <td colspan="2"><p><strong>&nbsp;Example Code Version Information</strong></p></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>Example Version: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_Example_Version" runat="server"/></td>
                    </tr>
                    <tr class="shade">
                        <td style="text-align: right"><strong><em>VPCRequest Version: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_VPCRequest_Version" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><strong><em>PaymentCodesHelper Version: </em></strong></td>
                        <td style="text-align: left"><asp:Label id="Label_PaymentCodesHelper_Version" runat="server"/></td>
                    </tr>
                    <tr>
                        <td style="height: 21px; width:50%" />
                        <td style="height: 21px; width:50%" />
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
        &nbsp;
    </div>
</body>
</html>