<%


 ' VPCConnection.asp
 '
 ' Version 1.0
 '
 ' ----------------- Disclaimer ------------------------------------------------
 '
 ' Copyright � 2007 Dialect Payment Technologies - a Transaction Network
 ' Services company.  All rights reserved.
 '
 ' This program is provided by Dialect Payment Technologies on the basis that
 ' you will treat it as confidential.
 '
 ' No part of this program may be reproduced or copied in any form by any means
 ' without the written permission of Dialect Payment Technologies.  Unless
 ' otherwise expressly agreed in writing, the information contained in this
 ' program is subject to change without notice and Dialect Payment Technologies
 ' assumes no responsibility for any alteration to, or any error or other
 ' deficiency, in this program.
 '
 ' 1. All intellectual property rights in the program and in all extracts and 
 '    things derived from any part of the program are owned by Dialect and will 
 '    be assigned to Dialect on their creation. You will protect all the 
 '    intellectual property rights relating to the program in a manner that is 
 '    equal to the protection you provide your own intellectual property.  You 
 '    will notify Dialect immediately, and in writing where you become aware of 
 '    a breach of Dialect's intellectual property rights in relation to the
 '    program.
 ' 2. The names "Dialect", "QSI Payments" and all similar words are trademarks
 '    of Dialect Payment Technologies and you must not use that name or any 
 '    similar name.
 ' 3. Dialect may at its sole discretion terminate the rights granted in this 
 '    program with immediate effect by notifying you in writing and you will 
 '    thereupon return (or destroy and certify that destruction to Dialect) all 
 '    copies and extracts of the program in its possession or control.
 ' 4. Dialect does not warrant the accuracy or completeness of the program or  
 '    its content or its usefulness to you or your merchant customers.  To the  
 '    extent permitted by law, all conditions and warranties implied by law  
 '    (whether as to fitness for any particular purpose or otherwise) are  
 '    excluded. Where the exclusion is not effective, Dialect limits its  
 '    liability to $100 or the resupply of the program (at Dialect's option).
 ' 5. Data used in examples and sample data files are intended to be fictional 
 '    and any resemblance to real persons or companies is entirely coincidental.
 ' 6. Dialect does not indemnify you or any third party in relation to the
 '   content or any use of the content as contemplated in these terms and 
 '    conditions. 
 ' 7. Mention of any product not owned by Dialect does not constitute an 
 '    endorsement of that product.
 ' 8. This program is governed by the laws of New South Wales, Australia and is 
 '    intended to be legally binding. 
 ' ---------------------------------------------------------------------------'/


 ' Please refer to the following guides for more information:
 '     1. Payment Client Integration Guide
 '        this details how to integrate with Payment Client 3.1.
 '     2. Payment Client Reference Guide
 '        this guide details all the input and return parameters that are used
 '        by the Payment Client and Payment Server for a Payment Client
 '        integration.
 '     3. Payment Client Install Guide
 '        this guide details the installation of Payment Client 3.1 and related
 '        issues.
 '
 ' @author Dialect Payment Technologies
 '


Class VPCConnection

	'  -----------------------------------------------------------------------------
	'
	' This Property returns the MD5 Secure Hash of the Param MyArray
	Public Property Get doSecureHash
		Dim md5HashData
		Dim index    
		 ' sort the array only if we are creating the MD5 hash
		 MyArray = sortArray(MyArray)	
		 ' start the MD5 input
		 md5HashData = SECURE_SECRET		 
		 ' loop though the array and add each parameter value to the MD5 input
		 index = 0
		 count = 0
		 For index = 0 to UBound(MyArray)
			  If (Len(MyArray(index,1)) > 0) Then
					md5HashData = md5HashData & MyArray(index,1)
					count = count + 1
			  End If
		 Next
		 ' increment the count to the next array location
		 count = count + 1		 
		 doSecureHash = MD5(md5HashData)
	End Property


	'  -----------------------------------------------------------------------------
	' This function takes an array and sorts it
	'
	' @param MyArray is the array to be sorted
	Public Function SortArray(MyArray)
	
	    Dim keepChecking
	    Dim loopCounter
	    Dim firstKey
	    Dim secondKey
	    Dim firstValue
	    Dim secondValue
	    
	    keepChecking = TRUE
	    loopCounter = 0
	    
	    Do Until keepChecking = FALSE
	        keepChecking = FALSE
	        For loopCounter = 0 To (UBound(MyArray)-1)
	            If MyArray(loopCounter,0) > MyArray((loopCounter+1),0) Then
	                ' transpose the key
	                firstKey = MyArray(loopCounter,0)
	                secondKey = MyArray((loopCounter+1),0)
	                MyArray(loopCounter,0) = secondKey
	                MyArray((loopCounter+1),0) = firstKey
	                ' transpose the key's value
	                firstValue = MyArray(loopCounter,1)
	                secondValue = MyArray((loopCounter+1),1)
	                MyArray(loopCounter,1) = secondValue
	                MyArray((loopCounter+1),1) = firstValue
	                keepChecking = TRUE
	            End If
	        Next
	    Loop
	    SortArray = MyArray
	End Function
	

	
	'  -----------------------------------------------------------------------------	
	' This function uses the URL Encoded value retrieved from the Digital
	' Receipt and returns a decoded string
	'
	' @param input containing the URLEncoded input value
	'
	' @return a string of the decoded input
	'
	Public Function URLDecode(encodedTxt)
	
	    Dim output
	    Dim percentSplit
	
	    If encodedTxt = "" Then
	        URLDecode = ""
	        Exit Function
	    End If
	
	    ' First convert the + to a space
	    output = Replace(encodedTxt, "+", " ")
	
	    ' Then convert the %hh to normal code
	    percentSplit = Split(output, "%")
	
	    If IsArray(percentSplit) Then
	        output = percentSplit(0)
	        Dim i
	        Dim part
	        Dim strHex
	        Dim Letter
	        For i = Lbound(percentSplit) To UBound(percentSplit) - 1
	            part = percentSplit(i + 1)
	            strHex = "&H" & Left(part, 2)
	            Letter = Chr(strHex)
	            output = output & Letter & Right(part, Len(part) -2)
	        Next
	    End If
	
	    URLDecode = output
	
	End Function

	
	'  -----------------------------------------------------------------------------	
	' This function uses the Response String retrieved from the Digital
	' Receipt and returns a collection of the the key value pairs
	'
	' @param queryString containing the Response String
	'
	' @return Dictionary of key value pairs
	'
	Function splitResponse(data)
	
	    Dim params
	    Dim pairs
	    Dim pair
	    Dim equalsIndex
	    Dim name
	    Dim value
	
	    Set params = CreateObject("Scripting.Dictionary")
	    
	    ' Check if there was a response
	    If Len(data) > 0 Then
	        ' Check if there are any paramenters in the response
	        If InStr(data, "=") > 0 Then
	            ' Get the parameters out of the response
	            pairs = Split(data, "&")
	            For Each pair In pairs
	                ' If there is a key/value pair in this item then store it
	                equalsIndex = InStr(pair, "=")
	                If equalsIndex > 1 And Len(pair) > equalsIndex Then
	                    name = Left(pair, equalsIndex - 1)
	                    value = Right(pair, Len(pair) - equalsIndex)
	                    params.add name, URLDecode(value)
	                End If
	            Next
	        Else
	            ' There were no parameters so create an error
	            params.Add "vpc_Message", "The data contained in the response was invalid or corrupt, the data is: <pre>" & data & "</pre>"
	        End If
	    Else
	        ' There was no data so create an error
	        params.Add "vpc_Message", "There was no data contained in the response"
	    End If
	    Set splitResponse = params
	End Function
	
	
	'  -----------------------------------------------------------------------------	     
	' This function takes a String and add a value if empty
	'
	' @param inputData is the String to be tested
	' @return String If input is empty returns string - "No Value Returned", Else returns inputData
	Function null2unknown(inputData) 
	    
	    If inputData = "" Then
	        null2unknown = "No Value Returned"
	    Else
	        null2unknown = inputData
	    End If
	End Function
	
	' -----------------------------------------------------------------------------	     
	' This function takes the vpc URL, postData, and proxy information
	' to perform a 2Party transaction and AMA transactions using the WinHttp object
	Function doPost(vpc_Url, postData, ProxyHost, ProxyPort, ProxyUsername, ProxyPassword)
		Dim txt
		Dim objHTTP
		Set objHTTP = Server.CreateObject("WinHttp.WinHttpRequest.5.1")		
		
		Dim message
		message = ""
		If Err Then
				Err.Raise 11,"VPCConnection.doPost", "Error creating WinHttp Object: " & Err.number & " - " & Err.Description
		Else			
				objHTTP.Open "POST", vpc_Url, False
				objHTTP.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"		
		
				Dim ProxyExclusions
				Dim ProxyServer
				
				ProxyServer = ProxyHost & ":" & ProxyPort
				ProxyExclusions=""
				
				' HttpRequest SetCredentials flags to suit your proxy.
				' Choose from the variables below to set the appropriate flag.
				Dim HTTPREQUEST_PROXYSETTING_DEFAULT
				Dim HTTPREQUEST_PROXYSETTING_PRECONFIG
				Dim HTTPREQUEST_PROXYSETTING_DIRECT
				Dim HTTPREQUEST_PROXYSETTING_PROXY
				HTTPREQUEST_PROXYSETTING_DEFAULT   = 0 ' Uses the setting configured in registry
				HTTPREQUEST_PROXYSETTING_PRECONFIG = 0 ' Uses the setting configured in registry
				HTTPREQUEST_PROXYSETTING_DIRECT    = 1 ' Establishes a direct connection without using a proxy
				HTTPREQUEST_PROXYSETTING_PROXY     = 2 ' Uses the proxy server configured in this code
				
				' Use the variables below to define the credentials required for Proxy
				' Authentication. If poroxy Authentication is not required then comment out
				Dim HTTPREQUEST_SETCREDENTIALS_FOR_SERVER
				Dim HTTPREQUEST_SETCREDENTIALS_FOR_PROXY
				' the objHTTP.SetCredentials call below.
				HTTPREQUEST_SETCREDENTIALS_FOR_SERVER = 0
				HTTPREQUEST_SETCREDENTIALS_FOR_PROXY = 1
				
				' Setup the Proxy Server credentials (if authentication is not required comment out this line)
				'objHTTP.SetCredentials ProxyUsername ProxyPassword HTTPREQUEST_SETCREDENTIALS_FOR_PROXY
				
				' Setup the proxy server
				objHTTP.SetProxy  HTTPREQUEST_PROXYSETTING_PROXY,  ProxyServer, ProxyExclusions

				
				If Err Then
						Err.Raise 11,"VPCConnection.doPost", "Error getting connection to Server: " & Err.number & " - " & Err.Description
				Else
						
						'Do Post
						objHTTP.send postData
						
						Dim httpStatusCode
						httpStatusCode=objHTTP.Status ' 200 is a successful result
						 
						Dim httpStatusText
						httpStatusText=objHTTP.StatusText
						
						If Not httpStatusCode = 200 And Len(message) = 0 Then
						    Err.Raise 11,"VPCConnection.doPost", "Error communicating with Server: " & httpStatusCode & " - " & httpStatusText
						ElseIf Err And Len(message) = 0 Then
						    Err.Raise 11,"VPCConnection.doPost", "Error using WinHttp Object: " & Err.number & " - " & Err.Description
						End If
						
						'Get response text
						doPost=objHTTP.ResponseText
						'return txt
				End If		
		End If
		
		Set objHTTP = Nothing
	End Function
	
	
	

End Class

%>
