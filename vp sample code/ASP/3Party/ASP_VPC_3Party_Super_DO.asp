<%@ LANGUAGE=vbscript %>

<%


 ' ASP_3party_Super_DO.asp
 '
 ' Version 1.0
 '
 ' ----------------- Disclaimer ------------------------------------------------
 '
 ' Copyright � 2007 Dialect Payment Technologies - a Transaction Network
 ' Services company.  All rights reserved.
 '
 ' This program is provided by Dialect Payment Technologies on the basis that
 ' you will treat it as confidential.
 '
 ' No part of this program may be reproduced or copied in any form by any means
 ' without the written permission of Dialect Payment Technologies.  Unless
 ' otherwise expressly agreed in writing, the information contained in this
 ' program is subject to change without notice and Dialect Payment Technologies
 ' assumes no responsibility for any alteration to, or any error or other
 ' deficiency, in this program.
 '
 ' 1. All intellectual property rights in the program and in all extracts and 
 '    things derived from any part of the program are owned by Dialect and will 
 '    be assigned to Dialect on their creation. You will protect all the 
 '    intellectual property rights relating to the program in a manner that is 
 '    equal to the protection you provide your own intellectual property.  You 
 '    will notify Dialect immediately, and in writing where you become aware of 
 '    a breach of Dialect's intellectual property rights in relation to the
 '    program.
 ' 2. The names "Dialect", "QSI Payments" and all similar words are trademarks
 '    of Dialect Payment Technologies and you must not use that name or any 
 '    similar name.
 ' 3. Dialect may at its sole discretion terminate the rights granted in this 
 '    program with immediate effect by notifying you in writing and you will 
 '    thereupon return (or destroy and certify that destruction to Dialect) all 
 '    copies and extracts of the program in its possession or control.
 ' 4. Dialect does not warrant the accuracy or completeness of the program or  
 '    its content or its usefulness to you or your merchant customers.  To the  
 '    extent permitted by law, all conditions and warranties implied by law  
 '    (whether as to fitness for any particular purpose or otherwise) are  
 '    excluded. Where the exclusion is not effective, Dialect limits its  
 '    liability to $100 or the resupply of the program (at Dialect's option).
 ' 5. Data used in examples and sample data files are intended to be fictional 
 '    and any resemblance to real persons or companies is entirely coincidental.
 ' 6. Dialect does not indemnify you or any third party in relation to the
 '   content or any use of the content as contemplated in these terms and 
 '    conditions. 
 ' 7. Mention of any product not owned by Dialect does not constitute an 
 '    endorsement of that product.
 ' 8. This program is governed by the laws of New South Wales, Australia and is 
 '    intended to be legally binding. 
 ' ---------------------------------------------------------------------------'/


 ' Please refer to the following guides for more information:
 '     1. Payment Client Integration Guide
 '        this details how to integrate with Payment Client 3.1.
 '     2. Payment Client Reference Guide
 '        this guide details all the input and return parameters that are used
 '        by the Payment Client and Payment Server for a Payment Client
 '        integration.
 '     3. Payment Client Install Guide
 '        this guide details the installation of Payment Client 3.1 and related
 '        issues.
 '
 ' @author Dialect Payment Technologies
 '


' Force explicit declaration of all variables
Option Explicit

' Turn off default error checking, as any errors are explicitly handled
'On Error Resume Next

' Include the classes that will be used
%>
<!--#include file="md5.asp"-->
<!--#include file="VPCConnection.asp"-->
<!--#include file="VPCCodesHelper.asp"-->
<%

'  @author Dialect Payment Pty Ltd Group 

' '''''''''''''''''''''''''''''''''''''''''''
' START OF MAIN PROGRAM
' '''''''''''''''''''''''''''''''''''''''''''

' The Page redirects the cardholder to the Virtual Payment Client (VPC)

' Define Constants
' ----------------
' This is secret for encoding the MD5 hash
' This secret will vary from merchant to merchant
' To not create a secure hash, let SECURE_SECRET be an empty string - ""
' Const SECURE_SECRET = "Your-Secure-Secret"
Const SECURE_SECRET = ""

' Stop the page being cached on the web server
Response.Expires = 0

' Create objects
Dim objMyVPCConn
Set objMyVPCConn = New VPCConnection

'Define variables
Dim message
Dim count
Dim item
Dim seperator
Dim redirectURL

' Create a 2 dimensional Array that we will use if we need a Secure Hash
If Len(SECURE_SECRET) > 0 Then
    Dim MyArray
    ReDim MyArray(Request.Form.Count,1)
End If

' Create the URL that will send the data to the Virtual Payment Client
redirectURL = Request("virtualPaymentClientURL")

' Add each of the appropriate form variables to the data.
seperator = "?"
count = 1
For Each item In Request.Form

    ' Do not include the Virtual Payment Client URL, the Submit button 
    ' from the form post, or any empty form fields, as we do not want to send 
    ' these fields to the Virtual Payment Client. 
    ' Also construct the VPC URL QueryString while looping through the Form data.
    If Request(item) <> "" And item <> "SubButL" And item <> "virtualPaymentClientURL" Then

        ' Add the item to the array if we need a Secure Hash
        If Len(SECURE_SECRET) > 0 Then
            MyArray (count,0) = CStr(item)
            MyArray (count,1) = CStr(Request(item))
        End If
       
        redirectURL = redirectURL & seperator & Server.URLEncode(CStr(item)) & "=" & Server.URLEncode(CStr(Request(item)))
        
        ' Add the data to the VPC URL QueryString
        'redirectURL = redirectURL & seperator & Server.URLEncode(CStr(item)) & "=" & Server.URLEncode(CStr(Request(item)))
        seperator = "&"

        ' Increment the count to the next array location
        count = count + 1

    End If
Next


If Err Then
    message = "Error creating request data: " & Err.Source & " - " & Err.number & " - " & Err.Description
    Response.Redirect Request("vpc_ReturnURL") & "?vpc_Message=" & message
    Response.End
End If

' If there is no Secure Secret then there is no need to create the Secure Hash
If Len(SECURE_SECRET) > 0 Then

    ' Create MD5 Message-Digest Algorithm hash and add it to the data to be sent
    redirectURL = redirectURL & seperator & "vpc_SecureHash=" & objMyVPCConn.doSecureHash

    If Err Then
        message = "Error creating Secure Hash: " & Err.Source & " - " & Err.number & " - " & Err.Description
        Response.Redirect Request("vpc_ReturnURL") & "?vpc_Message=" & message
        Response.End
    End If

End If

' FINISH TRANSACTION - Send the cardholder to the VPC
' ===================================================
' For the purposes of demonstration, we perform a standard URL redirect. 
Response.Redirect redirectURL
Response.End

' '''''''''''''''''''
' END OF MAIN PROGRAM
' '''''''''''''''''''

'  -----------------------------------------------------------------------------
%>