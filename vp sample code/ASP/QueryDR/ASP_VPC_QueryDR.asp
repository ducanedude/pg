<%@ LANGUAGE=vbscript %>

<%
 ' ASP_VPC_QueryDR.asp
 '
 ' Version 1.0
 '
 ' ----------------- Disclaimer ------------------------------------------------
 '
 ' Copyright � 2007 Dialect Payment Technologies - a Transaction Network
 ' Services company.  All rights reserved.
 '
 ' This program is provided by Dialect Payment Technologies on the basis that
 ' you will treat it as confidential.
 '
 ' No part of this program may be reproduced or copied in any form by any means
 ' without the written permission of Dialect Payment Technologies.  Unless
 ' otherwise expressly agreed in writing, the information contained in this
 ' program is subject to change without notice and Dialect Payment Technologies
 ' assumes no responsibility for any alteration to, or any error or other
 ' deficiency, in this program.
 '
 ' 1. All intellectual property rights in the program and in all extracts and 
 '    things derived from any part of the program are owned by Dialect and will 
 '    be assigned to Dialect on their creation. You will protect all the 
 '    intellectual property rights relating to the program in a manner that is 
 '    equal to the protection you provide your own intellectual property.  You 
 '    will notify Dialect immediately, and in writing where you become aware of 
 '    a breach of Dialect's intellectual property rights in relation to the
 '    program.
 ' 2. The names "Dialect", "QSI Payments" and all similar words are trademarks
 '    of Dialect Payment Technologies and you must not use that name or any 
 '    similar name.
 ' 3. Dialect may at its sole discretion terminate the rights granted in this 
 '    program with immediate effect by notifying you in writing and you will 
 '    thereupon return (or destroy and certify that destruction to Dialect) all 
 '    copies and extracts of the program in its possession or control.
 ' 4. Dialect does not warrant the accuracy or completeness of the program or  
 '    its content or its usefulness to you or your merchant customers.  To the  
 '    extent permitted by law, all conditions and warranties implied by law  
 '    (whether as to fitness for any particular purpose or otherwise) are  
 '    excluded. Where the exclusion is not effective, Dialect limits its  
 '    liability to $100 or the resupply of the program (at Dialect's option).
 ' 5. Data used in examples and sample data files are intended to be fictional 
 '    and any resemblance to real persons or companies is entirely coincidental.
 ' 6. Dialect does not indemnify you or any third party in relation to the
 '   content or any use of the content as contemplated in these terms and 
 '    conditions. 
 ' 7. Mention of any product not owned by Dialect does not constitute an 
 '    endorsement of that product.
 ' 8. This program is governed by the laws of New South Wales, Australia and is 
 '    intended to be legally binding. 
 ' ---------------------------------------------------------------------------'/


 ' Please refer to the following guides for more information:
 '     1. Payment Client Integration Guide
 '        this details how to integrate with Payment Client 3.1.
 '     2. Payment Client Reference Guide
 '        this guide details all the input and return parameters that are used
 '        by the Payment Client and Payment Server for a Payment Client
 '        integration.
 '     3. Payment Client Install Guide
 '        this guide details the installation of Payment Client 3.1 and related
 '        issues.
 '
 ' @author Dialect Payment Technologies
 '

 
 
' Force explicit declaration of all variables
Option Explicit

' Turn off default error checking, as any errors are explicitly handled
On Error Resume Next

' Include the Connection Class and the Codes helper class
%>
<!--#include file="VPCConnection.asp"-->
<!--#include file="VPCCodesHelper.asp"-->
<%
'  ----------------------------------------------------------------------------

'  This program assumes that a URL has been sent to this example with the
'  required fields. The example then processes the command and displays the
'  receipt or error to a HTML page in the users web browser.

'  @author Dialect Payment Technologies Pty Ltd  

'  ----------------------------------------------------------------------------

' '''''''''''''''''''''''''''''''''''''''''''
' START OF MAIN PROGRAM
' '''''''''''''''''''''''''''''''''''''''''''
' The Page does a redirect to the Virtual Payment Client

' Stop the page being cached on the web server
Response.Expires = 0

' Create objects
Dim objMyVPCHelper
Dim objMyVPCConn
Set objMyVPCHelper = New VPCCodesHelper
Set objMyVPCConn = New VPCConnection


' Add each of the appropriate form variables to the data.
Dim postData
Dim count
Dim item
Dim receiptType
Dim message

' Add each of the appropriate form variables to the data.
count = 1
postData = ""

' This is the receipt type to use in display
receiptType  = Request("ReceiptType")

For Each item In Request.Form
    ' Do not include the Virtual Payment Client URL, the Submit button 
    ' from the form post, or any empty form fields, as we do not want to send 
    ' these fields to the Virtual Payment Client. 
    ' Also construct the VPC URL QueryString while looping through the Form data.
    If Request(item) <> "" _
            And item <> "SubButL" _
            And item <> "ReceiptType" _
            And item <> "virtualPaymentClientURL" _
            And item <> "title" Then

        ' Add the data to the VPC URL QueryString
        postData = postData & Server.URLEncode(CStr(item)) & "=" & Server.URLEncode(CStr(Request(item))) & "&"

        ' Increment the count to the next array location
        count = count + 1

    End If
Next

' Remove the trailing ampersand on the data
postData = Mid(postData,1,Len(postData)-1)

If Err Then
    message = "Error creating POST data: " & Err.number & " - " & Err.Description
    Err.Clear
End If

' Perform Query DR
Dim vpc_Url, proxyPort, proxyHost, ProxyUsername, proxyPassword
vpc_Url=Request("virtualPaymentClientURL")
proxyHost = ""
proxyPort = ""
ProxyUsername = ""
proxyPassword = ""

Dim txt

txt = objMyVPCConn.doPost(vpc_Url, postData, ProxyHost, ProxyPort, ProxyUsername, ProxyPassword)

' Create the dictionary to hold the response data
Dim respParams
Set respParams = objMyVPCConn.splitResponse(txt)

If Err And Len(message) = 0 Then
    message = "Error reading response: " & Err.number & " - " & Err.Description
    Err.clear
End If

' *******************
' END OF MAIN PROGRAM
' *******************


' FINISH TRANSACTION - Output the VPC Response Data
' =====================================================
' For the purposes of demonstration, we simply display the Result fields on a
' web page.

' Miscellaneous Data
Dim title
title     = Request("title")

' This field is not returned in receipt for an error condition
Dim merchTxnRef
merchTxnRef     = Request("vpc_MerchTxnRef")

' Extract the available receipt fields from the VPC Response
' If not present then set the value to "No Value Returned" using the 
' null2unknown Function

Dim drExists, multipleDRs
' QueryDR Data
drExists        = objMyVPCConn.null2unknown(respParams("vpc_DRExists"))
multipleDRs     = objMyVPCConn.null2unknown(respParams("vpc_FoundMultipleDRs"))

Dim amount, batchNo, command, version, cardType, orderInfo, receiptNo, _
    merchantID, authorizeID, transactionNo, acqResponseCode, txnResponseCode
' Standard Receipt Data
amount          = objMyVPCConn.null2unknown(respParams("vpc_Amount"))
batchNo         = objMyVPCConn.null2unknown(respParams("vpc_BatchNo"))
command         = objMyVPCConn.null2unknown(respParams("vpc_Command"))
version         = objMyVPCConn.null2unknown(respParams("vpc_Version"))
cardType        = objMyVPCConn.null2unknown(respParams("vpc_Card"))
orderInfo       = objMyVPCConn.null2unknown(respParams("vpc_OrderInfo"))
receiptNo       = objMyVPCConn.null2unknown(respParams("vpc_ReceiptNo"))
merchantID      = objMyVPCConn.null2unknown(respParams("vpc_Merchant"))
authorizeID     = objMyVPCConn.null2unknown(respParams("vpc_AuthorizeId"))
transactionNo   = objMyVPCConn.null2unknown(respParams("vpc_TransactionNo"))
acqResponseCode = objMyVPCConn.null2unknown(respParams("vpc_AcqResponseCode"))
txnResponseCode = objMyVPCConn.null2unknown(respParams("vpc_TxnResponseCode"))
' Don't overwrite an existing error message
If Len(message) = 0 Then 
    message = objMyVPCConn.null2unknown(respParams("vpc_Message"))
End If

Dim cscResultCode, cscRequestCode, acqCSCRespCode
' CSC Receipt Data
cscResultCode  = objMyVPCConn.null2unknown(respParams("vpc_CSCResultCode"))
cscRequestCode = objMyVPCConn.null2unknown(respParams("vpc_CSCRequestCode"))
acqCSCRespCode = objMyVPCConn.null2unknown(respParams("vpc_AcqCSCRespCode"))

Dim avs_City, avs_Country, avs_Street01, avs_PostCode, avs_StateProv, _
    avsResultCode, avsRequestCode, acqAVSRespCode
' AVS Receipt Data
avs_City       = objMyVPCConn.null2unknown(respParams("vpc_AVS_City"))
avs_Country    = objMyVPCConn.null2unknown(respParams("vpc_AVS_Country"))
avs_Street01   = objMyVPCConn.null2unknown(respParams("vpc_AVS_Street01"))
avs_PostCode   = objMyVPCConn.null2unknown(respParams("vpc_AVS_PostCode"))
avs_StateProv  = objMyVPCConn.null2unknown(respParams("vpc_AVS_StateProv"))
avsResultCode  = objMyVPCConn.null2unknown(respParams("vpc_AVSResultCode"))
avsRequestCode = objMyVPCConn.null2unknown(respParams("vpc_AVSRequestCode"))
acqAVSRespCode = objMyVPCConn.null2unknown(respParams("vpc_AcqAVSRespCode"))

Dim shopTransNo, authorisedAmount, capturedAmount, refundedAmount, ticketNumber
' AMA Transaction Data
shopTransNo     = objMyVPCConn.null2unknown(respParams("vpc_ShopTransactionNo"))
authorisedAmount= objMyVPCConn.null2unknown(respParams("vpc_AuthorisedAmount"))
capturedAmount  = objMyVPCConn.null2unknown(respParams("vpc_CapturedAmount"))
refundedAmount  = objMyVPCConn.null2unknown(respParams("vpc_RefundedAmount"))
ticketNumber    = objMyVPCConn.null2unknown(respParams("vpc_TicketNo"))


Dim amaTransaction
' Define an AMA transaction output for Refund & Capture transactions
If LCase(receiptType) = "ama" Then
    amaTransaction = 1
Else
    amaTransaction = 0
End if

Response.write "amaTransaction=" & amaTransaction

' Show "Error" in title if there is an error condition
Dim errorTitle
errorTitle = ""
' Show this page as an error page if vpc_TxnResponseCode is not "0"
If txnResponseCode = "" Or txnResponseCode = "7" Or txnResponseCode = "No Value Returned" Then 
    errorTitle = "Error "
End If
    
' FINISH TRANSACTION - Process the VPC Response Data
' =====================================================
' For the purposes of demonstration, we simply display the Result fields on
' a web page.
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title><%=title%> - <%=errorTitle%>Response Page</title>
        <meta http-equiv="Content-Type" content="text/html, charset=iso-8859-1">
        <style type="text/css">
            <!--
            h1       { font-family:Arial,sans-serif; font-size:20pt; font-weight:600; margin-bottom:0.1em; color:#08185A;}
            h2       { font-family:Arial,sans-serif; font-size:14pt; font-weight:100; margin-top:0.1em; color:#08185A;}
            h2.co    { font-family:Arial,sans-serif; font-size:24pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
            h3       { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
            h3.co    { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#FFFFFF}
            body     { font-family:Verdana,Arial,sans-serif; font-size:10pt; background-color:#FFFFFF; color:#08185A}
            th       { font-family:Verdana,Arial,sans-serif; font-size:8pt; font-weight:bold; background-color:#CED7EF; padding-top:0.5em; padding-bottom:0.5em;  color:#08185A}
            tr       { height:25px; }
            .shade   { height:25px; background-color:#CED7EF }
            .title   { height:25px; background-color:#0074C4 }
            td       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A }
            td.red   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
            td.green { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#008800 }
            p        { font-family:Verdana,Arial,sans-serif; font-size:10pt; color:#FFFFFF }
            p.blue   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#08185A }
            p.red    { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
            p.green  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#008800 }
            div.bl   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#0074C4 }
            div.red  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
            li       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
            input    { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold }
            select   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold; }
            textarea { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:normal; scrollbar-arrow-color:#08185A; scrollbar-base-color:#CED7EF }
            -->
        </style>
    </head>
    <body>
        <!-- Start Branding Table -->
        <table width="100%" border="2" cellpadding="2" class="title">
            <tr>
                <td class="shade" width="90%"><h2 class="co">&nbsp;Virtual Payment Client Example</h2></td>
                <td class="title" align="center"><h3 class="co">Dialect<br />Payments</h3></td>
            </tr>
        </table>
        <!-- End Branding Table -->
        <center><h1><%=title%> - <%=errorTitle%>Response Page</h1></center>
        <table width="85%" align="center" cellpadding="5" border="0" ID="Table2">
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;QueryDR Transaction Fields</strong></p></td>
            </tr>
            <tr>
                <td align="right" width="55%"><strong><i>VPC API Version: </i></strong></td>
                <td width="45%"><%=version%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Command: </i></strong></td>
                <td><%=command%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Merchant Transaction Reference: </i></strong></td>
                <td><%=merchTxnRef%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Merchant ID: </i></strong></td>
                <td><%=merchantID%></td>
            </tr>
        <tr>
            <td colspan='2' align='center'><font color='#0074C4'>Fields above are the primary request values.<br/><hr/>
                Fields immediately below are additional special fields for QueryDR functionality.</font><br/></td>
        </tr>
              
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;QueryDR Only Fields</strong></p></td>
            </tr>
            <tr>                    
                <td align='right'><strong><i>Receipt Exists: </i></strong></td>
                <td><%=drExists%></td>
            </tr>
            <tr class='shade'>
                <td align='right'><strong><i>Found Multiple Receipts: </i></strong></td>
                <td><%=multipleDRs%></td>
            </tr>
<% ' No need to display any more response fields if no receipt present
If UCase(drExists) <> "N" Then
%>            
            <tr>
                <td colspan='2' align='center'><font color='#0074C4'><hr/>
                    Fields below are for a Standard Transaction.</font><br/></td>
            </tr>
            
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Standard Transaction Fields</strong></p></td>
            </tr>
      
            <tr>
                <td align="right"><strong><i>Order Information: </i></strong></td>
                <td><%=orderInfo%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Amount: </i></strong></td>
                <td><%=amount%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>VPC Transaction Response Code: </i></strong></td>
                <td><%=txnResponseCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Transaction Response Code Description: </i></strong></td>
                <td><%=objMyVPCHelper.getResponseDescription(txnResponseCode)%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Message: </i></strong></td>
                <td><%=message%></td>
            </tr>
<%
' Only display the following fields if not an error condition
	If txnResponseCode <> "" And txnResponseCode <> "7" And txnResponseCode <> "No Value Returned" And UCase(drExists) = "Y" Then 
%>
            <tr>
                <td align="right"><strong><i>Receipt Number: </i></strong></td>
                <td><%=receiptNo%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Transaction Number: </i></strong></td>
                <td><%=transactionNo%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Acquirer Response Code: </i></strong></td>
                <td><%=acqResponseCode%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Bank Authorization ID: </i></strong></td>
                <td><%=authorizeID%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Batch Number: </i></strong></td>
                <td><%=batchNo%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Card Type: </i></strong></td>
                <td><%=cardType%></td>
            </tr>
            <tr>
                <td colspan='2' align='center'><font color='#0074C4'>Fields above are for Standard Transactions<br/><hr/>
                    Fields below are additional fields for extra functionality.</font><br/></td>
            </tr>

<%   
		If amaTransaction then 
%>
            <tr>
                <td colspan = '2'><hr /></td>
            </tr>
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Financial Transaction Fields</strong></p></td>
            </tr>
            <tr>
                <td align='right'><strong><i>Shopping Transaction Number: </i></strong></td>
                <td><%=shopTransNo%></td>
            </tr>
            <tr class='shade'>
                <td align='right'><strong><i>Authorised Amount: </i></strong></td>
                <td><%=authorisedAmount%></td>
            </tr>
            <tr>                
                <td align='right'><strong><i>Captured Amount: </i></strong></td>
                <td><%=capturedAmount%></td>
            </tr>
            <tr class='shade'>
                <td align='right'><strong><i>Refunded Amount: </i></strong></td>
                <td><%=refundedAmount%></td>
            </tr>
            <tr>                  
                <td align='right'><strong><i>Ticket Number: </i></strong></td>
                <td><%=ticketNumber%></td>
            </tr>
<%   
		Else 
%>

            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Card Security Code Fields</strong></p></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>CSC Acquirer Response Code: </i></strong></td>
                <td><%=acqCSCRespCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>CSC Result Code: </i></strong></td>
                <td><%=cscResultCode%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>CSC Result Description: </i></strong></td>
                <td><%=objMyVPCHelper.displayCSCResponse(cscResultCode)%></td>
            </tr>
            <tr>
                <td colspan="2"><hr /></td>
            </tr>
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Address Verification Service Fields</strong></p></td>
            </tr>
            <tr>
                <td align="right"><strong><i>AVS Street/Postal Address: </i></strong></td>
                <td><%=avs_Street01%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>AVS City/Town/Suburb: </i></strong></td>
                <td><%=avs_City%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>AVS State/Province: </i></strong></td>
                <td><%=avs_StateProv%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>AVS Postal/Zip Code: </i></strong></td>
                <td><%=avs_PostCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>AVS Country Code: </i></strong></td>
                <td><%=avs_Country%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>AVS Request Code: </i></strong></td>
                <td><%=avsRequestCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>AVS Acquirer Response Code: </i></strong></td>
                <td><%=acqAVSRespCode%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>AVS QSI Result Code: </i></strong></td>
                <td><%=avsResultCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>AVS Result Description: </i></strong></td>
                <td><%=objMyVPCHelper.displayAVSResponse(avsResultCode)%></td>
            </tr>

            <tr>
                <td colspan="2"><hr /></td>
            </tr>
 
<%
		End If 
	End If 
End If%>
        </table>
    </body>
</html>

