<%@ LANGUAGE=vbscript %>

<%


 ' ASP_VPC_3party_Auth_Capture_DR.asp
 '
 ' Version 1.0
 '
 ' ----------------- Disclaimer ------------------------------------------------
 '
 ' Copyright � 2007 Dialect Payment Technologies - a Transaction Network
 ' Services company.  All rights reserved.
 '
 ' This program is provided by Dialect Payment Technologies on the basis that
 ' you will treat it as confidential.
 '
 ' No part of this program may be reproduced or copied in any form by any means
 ' without the written permission of Dialect Payment Technologies.  Unless
 ' otherwise expressly agreed in writing, the information contained in this
 ' program is subject to change without notice and Dialect Payment Technologies
 ' assumes no responsibility for any alteration to, or any error or other
 ' deficiency, in this program.
 '
 ' 1. All intellectual property rights in the program and in all extracts and 
 '    things derived from any part of the program are owned by Dialect and will 
 '    be assigned to Dialect on their creation. You will protect all the 
 '    intellectual property rights relating to the program in a manner that is 
 '    equal to the protection you provide your own intellectual property.  You 
 '    will notify Dialect immediately, and in writing where you become aware of 
 '    a breach of Dialect's intellectual property rights in relation to the
 '    program.
 ' 2. The names "Dialect", "QSI Payments" and all similar words are trademarks
 '    of Dialect Payment Technologies and you must not use that name or any 
 '    similar name.
 ' 3. Dialect may at its sole discretion terminate the rights granted in this 
 '    program with immediate effect by notifying you in writing and you will 
 '    thereupon return (or destroy and certify that destruction to Dialect) all 
 '    copies and extracts of the program in its possession or control.
 ' 4. Dialect does not warrant the accuracy or completeness of the program or  
 '    its content or its usefulness to you or your merchant customers.  To the  
 '    extent permitted by law, all conditions and warranties implied by law  
 '    (whether as to fitness for any particular purpose or otherwise) are  
 '    excluded. Where the exclusion is not effective, Dialect limits its  
 '    liability to $100 or the resupply of the program (at Dialect's option).
 ' 5. Data used in examples and sample data files are intended to be fictional 
 '    and any resemblance to real persons or companies is entirely coincidental.
 ' 6. Dialect does not indemnify you or any third party in relation to the
 '   content or any use of the content as contemplated in these terms and 
 '    conditions. 
 ' 7. Mention of any product not owned by Dialect does not constitute an 
 '    endorsement of that product.
 ' 8. This program is governed by the laws of New South Wales, Australia and is 
 '    intended to be legally binding. 
 ' ---------------------------------------------------------------------------'/


 ' Please refer to the following guides for more information:
 '     1. Payment Client Integration Guide
 '        this details how to integrate with Payment Client 3.1.
 '     2. Payment Client Reference Guide
 '        this guide details all the input and return parameters that are used
 '        by the Payment Client and Payment Server for a Payment Client
 '        integration.
 '     3. Payment Client Install Guide
 '        this guide details the installation of Payment Client 3.1 and related
 '        issues.
 '
 ' @author Dialect Payment Technologies
 '


' Force explicit declaration of all variables
Option Explicit

' Turn off default error checking, as any errors are explicitly handled
On Error Resume Next

' Include the classes that will be used
%>
<!--#include file="md5.asp"-->
<!--#include file="VPCConnection.asp"-->
<!--#include file="VPCCodesHelper.asp"-->
<%

'  @author Dialect Payments Pty Ltd Group 

' '''''''''''''''''''''''''''''''''''''''''''
' START OF MAIN PROGRAM
' '''''''''''''''''''''''''''''''''''''''''''

' The Page redirects the cardholder to the Virtual Payment Client (VPC)

' Define Constants
' ----------------
' This is secret for encoding the MD5 hash
' This secret will vary from merchant to merchant
' To not create a secure hash, let SECURE_SECRET be an empty string - ""
' Const SECURE_SECRET = "Your-Secure-Secret"
Const SECURE_SECRET = ""

' Stop the page being cached on the web server
Response.Expires = 0

' Create objects
Dim objMyVPCHelper
Dim objMyVPCConn
Set objMyVPCHelper = New VPCCodesHelper
Set objMyVPCConn = New VPCConnection

' Local Variables
Dim hashValidated, errorExists, errorTitle

' Miscellaneous Data that was added to the Digital Order
Dim title

' Standard Receipt Data
Dim amount, locale, batchNo, command, message, version, cardType, orderInfo, receiptNo, _
    merchantID, authorizeID, merchTxnRef, transactionNo, acqResponseCode, txnResponseCode

' Variables used for capture   
Dim captAmount, captLocale, captBatchNo, captCommand, captVersion, captOrderInfo, captReceiptNo, captAuthorizeID, captTransactionNr, _ 
		captAcqResponseCode, captTxnResponseCode, captMerchTxnRef, captTxnResponseCodeDesc, captAuthorisedAmount, captCapturedAmount, _
		captRefundedAmount, captTicketNumber

' CSC Receipt Data
Dim cscResultCode, cscRequestCode, acqCSCRespCode

' AVS Receipt Data
Dim avs_City, avs_Country, avs_Street01, avs_PostCode, avs_StateProv, _
    avsResultCode, avsRequestCode, acqAVSRespCode

' Initialise the Local Variables
hashValidated = "<font color='orange'><b>Not Calculated</b></font>"
errorTitle = ""
errorExists = 0

' If we have a SECURE_SECRET then validate the incoming data using the MD5 hash
' included in the incoming data
If Len(SECURE_SECRET) > 0 And Len(Request.QueryString("vpc_SecureHash")) > 0 Then
    ' Find out if the incoming data is in a POST or a GET
    ' Create a 2 dimensional array to hold the form variables so we can sort them
    Dim MyArray
    Dim count
    Dim item
    ReDim MyArray((Request.QueryString.Count),1)

    ' Enter each of the appropriate form variables into the array.
    count = 1
    For Each item In Request.QueryString
        ' Do not include the Virtual Payment Client URL, the Submit button 
        ' from the form post, or any control fields, as we do not want to send 
        ' these fields to the Virtual Payment Client. 
        If Request.QueryString(item) <> "" And item <> "vpc_SecureHash" Then
            ' Add the item to the array
            MyArray (count,0) = item                
            MyArray (count,1) = Request.QueryString(item)
            ' Increment the count to the next array location
            count = count + 1
        End If
    Next

    ' Validate the Secure Hash (remember MD5 hashes are not case sensitive)
    If UCase(Request.QueryString("vpc_SecureHash")) = UCase(objMyVPCConn.doSecureHash) Then
        ' Secure Hash validation succeeded,
        ' add a data field to be displayed later.
        hashValidated = "<font color='#00AA00'><b>CORRECT</b></font>"
    Else
        ' Secure Hash validation failed, add a data field to be displayed
        ' later.
        hashValidated = "<font color='#FF0066'><b>INVALID HASH</b></font>"
        errorExists = 1
    End If
End If

If Err Then
    message = "Error validating Secure Hash: " & Err.Source & " - " & Err.number & " - " & Err.Description
    Response.End
End If

' FINISH TRANSACTION - Output the VPC Response Data
' =====================================================
' For the purposes of demonstration, we simply display the Result fields on a
' web page.

' Extract the available receipt fields from the VPC Response
' If not present then set the value to "No Value Returned" using the 
' null2unknown Function

' Miscellaneous Data that was added to the Digital Order
title     = objMyVPCConn.null2unknown(Request.QueryString("title"))


' Standard Receipt Data
amount          = objMyVPCConn.null2unknown(Request.QueryString("vpc_Amount"))
locale          = objMyVPCConn.null2unknown(Request.QueryString("vpc_Locale"))
batchNo         = objMyVPCConn.null2unknown(Request.QueryString("vpc_BatchNo"))
command         = objMyVPCConn.null2unknown(Request.QueryString("vpc_Command"))
version         = objMyVPCConn.null2unknown(Request.QueryString("vpc_Version"))
cardType        = objMyVPCConn.null2unknown(Request.QueryString("vpc_Card"))
orderInfo       = objMyVPCConn.null2unknown(Request.QueryString("vpc_OrderInfo"))
receiptNo       = objMyVPCConn.null2unknown(Request.QueryString("vpc_ReceiptNo"))
merchantID      = objMyVPCConn.null2unknown(Request.QueryString("vpc_Merchant"))
authorizeID     = objMyVPCConn.null2unknown(Request.QueryString("vpc_AuthorizeId"))
merchTxnRef     = objMyVPCConn.null2unknown(Request.QueryString("vpc_MerchTxnRef"))
transactionNo   = objMyVPCConn.null2unknown(Request.QueryString("vpc_TransactionNo"))
acqResponseCode = objMyVPCConn.null2unknown(Request.QueryString("vpc_AcqResponseCode"))
txnResponseCode = objMyVPCConn.null2unknown(Request.QueryString("vpc_TxnResponseCode"))

If Len(message) = 0 then
    message     = objMyVPCConn.null2unknown(Request.QueryString("vpc_Message"))
End If

' CSC Receipt Data
cscResultCode  = objMyVPCConn.null2unknown(Request.QueryString("vpc_CSCResultCode"))
cscRequestCode = objMyVPCConn.null2unknown(Request.QueryString("vpc_CSCRequestCode"))
acqCSCRespCode = objMyVPCConn.null2unknown(Request.QueryString("vpc_AcqCSCRespCode"))

' AVS Receipt Data
avs_City       = objMyVPCConn.null2unknown(Request.QueryString("vpc_AVS_City"))
avs_Country    = objMyVPCConn.null2unknown(Request.QueryString("vpc_AVS_Country"))
avs_Street01   = objMyVPCConn.null2unknown(Request.QueryString("vpc_AVS_Street01"))
avs_PostCode   = objMyVPCConn.null2unknown(Request.QueryString("vpc_AVS_PostCode"))
avs_StateProv  = objMyVPCConn.null2unknown(Request.QueryString("vpc_AVS_StateProv"))
avsResultCode  = objMyVPCConn.null2unknown(Request.QueryString("vpc_AVSResultCode"))
avsRequestCode = objMyVPCConn.null2unknown(Request.QueryString("vpc_AVSRequestCode"))
acqAVSRespCode = objMyVPCConn.null2unknown(Request.QueryString("vpc_AcqAVSRespCode"))



' FINISH TRANSACTION - Process the VPC Response Data
' =====================================================
' For the purposes of demonstration, we simply display the Result fields on
' a web page.

' Show this page as an error page if vpc_TxnResponseCode is not "0"
If txnResponseCode = "7" Or txnResponseCode = "No Value Returned" Or errorExists = 1 Then 
    errorTitle = "Error "
End If

Dim captMessage, captAttempted

captAttempted = false

'****************************************************
' CAPTURE
'
' If QSI response code is zero perform the Capture
If txnResponseCode = "0" Then

	' Create objects for Capture transaction
	captAttempted = true
	Dim username, password, accesscode, captVpcURL, captPostData

	' Add username and password to request NOTE: This username and password will need to be created in 
	' Merchant Administration and will need the "Advanced Merchant Administration" and "Perform Capture" privelages
	' to be selected. The access code will need to be retrieved from Merchant Administration.
	username = ""
	password = ""
	accesscode = ""
	captVpcURL = ""
	
	'Create post data for capture transaction	
	captPostData = "vpc_Version=1&"
	captPostData = captPostData & "vpc_Command=capture&"
	captPostData = captPostData & "vpc_AccessCode=" & accesscode & "&"
	captPostData = captPostData & "vpc_MerchTxnRef=" & merchTxnRef & "_C&"
	captPostData = captPostData & "vpc_Merchant=" & merchantID & "&"
	captPostData = captPostData & "vpc_TransNo=" & transactionNo & "&"
	captPostData = captPostData & "vpc_Amount=" & amount & "&"
	captPostData = captPostData & "vpc_User=" & username & "&"
	captPostData = captPostData & "vpc_Password=" & password
	
	If Err Then
    message = "Error creating POST data: " & Err.number & " - " & Err.Description
    Err.Clear
	End If	
	
	' Perform Capture	
	Dim vpc_Url, proxyPort, proxyHost, ProxyUsername, proxyPassword
	vpc_Url=Request("virtualPaymentClientURL")
	proxyHost = ""
	proxyPort = ""
	ProxyUsername = ""
	proxyPassword = ""
	
	Dim txt
	txt = objMyVPCConn.doPost(captVpcURL, captPostData, ProxyHost, ProxyPort, ProxyUsername, ProxyPassword)
	
	' Create the dictionary to hold the response data
	Dim respParams
	Set respParams = objMyVPCConn.splitResponse(txt)
	
	If Err And Len(message) = 0 Then
	    message = "Error reading response: " & Err.number & " - " & Err.Description
	    Err.clear
	End If
	
	If Len(captMessage) = 0 Then
        captMessage = objMyVPCConn.null2unknown(respParams("vpc_Message"))
  End If

  ' Standard Receipt Data
  captAmount          = objMyVPCConn.null2unknown(respParams("vpc_Amount"))
  captLocale          = objMyVPCConn.null2unknown(respParams("vpc_Locale"))
  captBatchNo         = objMyVPCConn.null2unknown(respParams("vpc_BatchNo"))
  captCommand         = objMyVPCConn.null2unknown(respParams("vpc_Command"))
  captVersion         = objMyVPCConn.null2unknown(respParams("vpc_Version"))
  captOrderInfo       = objMyVPCConn.null2unknown(respParams("vpc_OrderInfo"))
  captReceiptNo       = objMyVPCConn.null2unknown(respParams("vpc_ReceiptNo"))
  captAuthorizeID     = objMyVPCConn.null2unknown(respParams("vpc_AuthorizeId"))
  captTransactionNr   = objMyVPCConn.null2unknown(respParams("vpc_TransactionNo"))
  captAcqResponseCode = objMyVPCConn.null2unknown(respParams("vpc_AcqResponseCode"))
  captTxnResponseCode = objMyVPCConn.null2unknown(respParams("vpc_TxnResponseCode"))
  captMerchTxnRef     = objMyVPCConn.null2unknown(respParams("vpc_MerchTxnRef"))

  If captTxnResponseCode = "No Value Returned" Then
      captTxnResponseCodeDesc = objMyVPCHelper.getResponseDescription(captTxnResponseCode)
  End If   

  ' Capture Data
  captAuthorisedAmount = objMyVPCConn.null2unknown(respParams("vpc_AuthorisedAmount"))
  captCapturedAmount   = objMyVPCConn.null2unknown(respParams("vpc_CapturedAmount"))
  captRefundedAmount   = objMyVPCConn.null2unknown(respParams("vpc_RefundedAmount"))
  captTicketNumber     = objMyVPCConn.null2unknown(respParams("vpc_TicketNo"))


  If captTxnResponseCode = "7" or captTxnResponseCode = "No Value Returned" Then
      captError = "Error "
  End If
Else 
    captMessage = "Capture not attempted due to Authorisation Failure - see above"
End If
	    
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>
            ASP 3-Party Auth & Capture Transaction - <%=errorTitle%>Response Page</title>
        <meta http-equiv="Content-Type" content="text/html, charset=iso-8859-1">
        <style type="text/css">
            <!--
            h1       { font-family:Arial,sans-serif; font-size:20pt; font-weight:600; margin-bottom:0.1em; color:#08185A;}
            h2       { font-family:Arial,sans-serif; font-size:14pt; font-weight:100; margin-top:0.1em; color:#08185A;}
            h2.co    { font-family:Arial,sans-serif; font-size:24pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
            h3       { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
            h3.co    { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#FFFFFF}
            body     { font-family:Verdana,Arial,sans-serif; font-size:10pt; background-color:#FFFFFF; color:#08185A}
            th       { font-family:Verdana,Arial,sans-serif; font-size:8pt; font-weight:bold; background-color:#CED7EF; padding-top:0.5em; padding-bottom:0.5em;  color:#08185A}
            tr       { height:25px; }
            .shade   { height:25px; background-color:#CED7EF }
            .title   { height:25px; background-color:#0074C4 }
            td       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A }
            td.red   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
            td.green { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#008800 }
            p        { font-family:Verdana,Arial,sans-serif; font-size:10pt; color:#FFFFFF }
            p.blue   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#08185A }
            p.red    { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
            p.green  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#008800 }
            div.bl   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#0074C4 }
            div.red  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
            li       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
            input    { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold }
            select   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold; }
            textarea { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:normal; scrollbar-arrow-color:#08185A; scrollbar-base-color:#CED7EF }
            -->
        </style>
    </head>
    <body>
        <!-- Start Branding Table -->
        <table width="100%" border="2" cellpadding="2" class="title">
            <tr>
                <td class="shade" width="90%"><h2 class="co">&nbsp;Virtual Payment Client Example</h2></td>
                <td class="title" align="center"><h3 class="co">Dialect<br />Payments</h3></td>
            </tr>
        </table>
        <!-- End Branding Table -->
        <center><h1>ASP 3-Party Auth & Capture Transaction - <%=errorTitle%>Response Page</h1></center>
        <table width="85%" align="center" cellpadding="5" border="0" ID="Table2">
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Basic Transaction Fields</strong></p></td>
            </tr>
            <tr>
                <td align="right" width="55%"><strong><i>VPC API Version: </i></strong></td>
                <td width="45%"><%=version%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Command: </i></strong></td>
                <td><%=command%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Merchant Transaction Reference: </i></strong></td>
                <td><%=merchTxnRef%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Merchant ID: </i></strong></td>
                <td><%=merchantID%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Order Information: </i></strong></td>
                <td><%=orderInfo%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Amount: </i></strong></td>
                <td><%=amount%></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <font color="#0074C4">Fields above are the primary request values.<br />
                        <hr />
                        Fields below are the response fields for a Standard Transaction.<br />
                    </font>
                </td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>VPC Transaction Response Code: </i></strong></td>
                <td><%=txnResponseCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Transaction Response Code Description: </i></strong></td>
                <td><%=objMyVPCHelper.getResponseDescription(txnResponseCode)%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Message: </i></strong></td>
                <td><%=message%></td>
            </tr>
            <% 
    ' Only display the following fields if not an error condition
    If txnResponseCode <> "7" And txnResponseCode <> "No Value Returned" Then 
%>
            <tr>
                <td align="right"><strong><i>Receipt Number: </i></strong></td>
                <td><%=receiptNo%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Transaction Number: </i></strong></td>
                <td><%=transactionNo%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Acquirer Response Code: </i></strong></td>
                <td><%=acqResponseCode%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Bank Authorization ID: </i></strong></td>
                <td><%=authorizeID%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Batch Number: </i></strong></td>
                <td><%=batchNo%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Card Type: </i></strong></td>
                <td><%=cardType%></td>
            </tr>
<%
			If captAttempted = true Then
%>
            <tr class="title">
                <td colspan="2"><p><strong>&nbsp;Capture Transaction Receipt Fields???</strong></p></td>
            </tr>
            <tr>    
                <td align="right"><strong><em>Capture Merchant Transaction Reference: </em></strong></td>
                <td><%=captMerchTxnRef%></td>
            </tr>
            <tr class ="shade">    
                <td align="right"><strong><em>Capture Financial Transaction Number: </em></strong></td>
                <td><%=captTransactionNr%></td>
            </tr>
            <tr>    
                <td align="right"><strong><em>Capture Amount: </em></strong></td>
                <td><%=captAmount%></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div class='bl'>Fields above are the primary request values.<hr>Fields below are receipt data fields.</div>
                </td>
            </tr>
            <tr>    
                <td align="right"><strong><em>Capture QSI Response Code: </em></strong></td>
                <td><%=captTxnResponseCode%></td>
            </tr>
            <tr class="shade">
                <td align='right'><strong><em>Capture QSI Response Code Description: </em></strong></td>
                <td><%=objMyVPCHelper.getResponseDescription(captTxnResponseCode)%></td>
            </tr>
            <tr>    
                <td align="right"><strong><em>Capture Acquirer Response Code: </em></strong></td>
                <td><%=captAcqResponseCode%></td>
            </tr>
            <tr class='shade'>
                <td align='right'><em><strong>Receipt No: </strong></em></td>
                <td><%=captReceiptNo%></td>
            </tr>
            <tr>                  
                <td align='right'><em><strong>Authorize Id: </strong></em></td>
                <td><%=captAuthorizeID%></td>
            </tr>
<%
			Else           
 %>
            <tr class='title'>
                <td colspan='2'><p><strong>&nbsp;Capture Transaction Receipt Fields</strong></p></td>
            </tr>
            <tr>
                <td align='right'><strong><em>Capture Error: </em></strong></td>
                <td><%=captMessage%></td>
            </tr>
<%    
			End If
%>
        <tr>
            <tr>
                <td colspan="2" align="center">
                    <font color="#0074C4">Fields above are for a Standard Transaction<br />
                        <hr />
                        Fields below are additional fields for extra functionality.<br />
                    </font>
                </td>
            </tr>
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Card Security Code Fields</strong></p></td>
            </tr>

            <tr class='shade'>
                <td align="right"><strong><i>CSC Acquirer Response Code: </i></strong></td>
                <td><%=acqCSCRespCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>CSC QSI Result Code: </i></strong></td>
                <td><%=cscResultCode%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>CSC Result Description: </i></strong></td>
                <td><%=objMyVPCHelper.displayCSCResponse(cscResultCode)%></td>
            </tr>
            <tr>
                <td colspan="2"><hr /></td>
            </tr>
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Address Verification Service Fields</strong></p></td>
            </tr>
            <tr>
                <td align="right"><strong><i>AVS Street/Postal Address: </i></strong></td>
                <td><%=avs_Street01%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>AVS City/Town/Suburb: </i></strong></td>
                <td><%=avs_City%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>AVS State/Province: </i></strong></td>
                <td><%=avs_StateProv%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>AVS Postal/Zip Code: </i></strong></td>
                <td><%=avs_PostCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>AVS Country Code: </i></strong></td>
                <td><%=avs_Country%></td>
            </tr>

            <tr class='shade'>
                <td align="right"><strong><i>AVS Acquirer Response Code: </i></strong></td>
                <td><%=acqAVSRespCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>AVS QSI Result Code: </i></strong></td>
                <td><%=avsResultCode%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>AVS Result Description: </i></strong></td>
                <td><%=objMyVPCHelper.displayAVSResponse(avsResultCode)%></td>
            <tr>
                <td colspan="2"><hr /></td>
            </tr>
<% 
End If 
%>
            <tr>
                <td colspan="2"><hr /></td>
            </tr>
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Hash Validation</strong></p></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Hash Validated Correctly: </i></strong></td>
                <td><%=hashValidated%></td>
            </tr>
        </table>
    </body>
</html>
<%    

' '''''''''''''''''''
' END OF MAIN PROGRAM
' '''''''''''''''''''


%>
