<%


 ' VPCCodesHelper.asp
 '
 ' Version 1.0
 '
 ' ----------------- Disclaimer ------------------------------------------------
 '
 ' Copyright � 2007 Dialect Payment Technologies - a Transaction Network
 ' Services company.  All rights reserved.
 '
 ' This program is provided by Dialect Payment Technologies on the basis that
 ' you will treat it as confidential.
 '
 ' No part of this program may be reproduced or copied in any form by any means
 ' without the written permission of Dialect Payment Technologies.  Unless
 ' otherwise expressly agreed in writing, the information contained in this
 ' program is subject to change without notice and Dialect Payment Technologies
 ' assumes no responsibility for any alteration to, or any error or other
 ' deficiency, in this program.
 '
 ' 1. All intellectual property rights in the program and in all extracts and 
 '    things derived from any part of the program are owned by Dialect and will 
 '    be assigned to Dialect on their creation. You will protect all the 
 '    intellectual property rights relating to the program in a manner that is 
 '    equal to the protection you provide your own intellectual property.  You 
 '    will notify Dialect immediately, and in writing where you become aware of 
 '    a breach of Dialect's intellectual property rights in relation to the
 '    program.
 ' 2. The names "Dialect", "QSI Payments" and all similar words are trademarks
 '    of Dialect Payment Technologies and you must not use that name or any 
 '    similar name.
 ' 3. Dialect may at its sole discretion terminate the rights granted in this 
 '    program with immediate effect by notifying you in writing and you will 
 '    thereupon return (or destroy and certify that destruction to Dialect) all 
 '    copies and extracts of the program in its possession or control.
 ' 4. Dialect does not warrant the accuracy or completeness of the program or  
 '    its content or its usefulness to you or your merchant customers.  To the  
 '    extent permitted by law, all conditions and warranties implied by law  
 '    (whether as to fitness for any particular purpose or otherwise) are  
 '    excluded. Where the exclusion is not effective, Dialect limits its  
 '    liability to $100 or the resupply of the program (at Dialect's option).
 ' 5. Data used in examples and sample data files are intended to be fictional 
 '    and any resemblance to real persons or companies is entirely coincidental.
 ' 6. Dialect does not indemnify you or any third party in relation to the
 '   content or any use of the content as contemplated in these terms and 
 '    conditions. 
 ' 7. Mention of any product not owned by Dialect does not constitute an 
 '    endorsement of that product.
 ' 8. This program is governed by the laws of New South Wales, Australia and is 
 '    intended to be legally binding. 
 ' ---------------------------------------------------------------------------'/


 ' Please refer to the following guides for more information:
 '     1. Payment Client Integration Guide
 '        this details how to integrate with Payment Client 3.1.
 '     2. Payment Client Reference Guide
 '        this guide details all the input and return parameters that are used
 '        by the Payment Client and Payment Server for a Payment Client
 '        integration.
 '     3. Payment Client Install Guide
 '        this guide details the installation of Payment Client 3.1 and related
 '        issues.
 '
 ' @author Dialect Payment Technologies
 '


Class VPCCodesHelper

	
	Public Function getResponseDescription(txnResponseCode)
	
			' This function uses the QSIResponseCode retrieved from the Digital Receipt
			' and returns an appropriate description
			'
			' @param txnResponseCode String containing the QSIResponseCode
			'
			' @return description String containing the appropriate description
			
	    Select Case txnResponseCode
	        Case "0"  getResponseDescription = "Transaction Successful"
	        Case "1"  getResponseDescription = "Transaction Declined"
	        Case "2"  getResponseDescription = "Bank Declined Transaction"
	        Case "3"  getResponseDescription = "No Reply from Bank"
	        Case "4"  getResponseDescription = "Expired Card"
	        Case "5"  getResponseDescription = "Insufficient Funds"
	        Case "6"  getResponseDescription = "Error Communicating with Bank"
	        Case "7"  getResponseDescription = "Payment Server detected an error"
	        Case "8"  getResponseDescription = "Transaction Type Not Supported"
	        Case "9"  getResponseDescription = "Bank declined transaction (Do not contact Bank)"
	        Case "A"  getResponseDescription = "Transaction Aborted"
	        Case "B"  getResponseDescription = "Transaction Declined (Contact Bank)"
	        Case "C"  getResponseDescription = "Transaction Cancelled"
	        Case "D"  getResponseDescription = "Deferred transaction has been received and is awaiting processing"
	        Case "F"  getResponseDescription = "3-D Secure Authentication failed"
	        Case "I"  getResponseDescription = "Card Security Code verification failed"
	        Case "L"  getResponseDescription = "Shopping Transaction Locked (Please try the transaction again later)"
	        Case "N"  getResponseDescription = "Cardholder is not enrolled in Authentication scheme"
	        Case "P"  getResponseDescription = "Transaction has been received by the Payment Adaptor and is being processed"
	        Case "R"  getResponseDescription = "Transaction was not processed - Reached limit of retry attempts allowed"
	        Case "S"  getResponseDescription = "Duplicate OrderInfo"
	        Case "T"  getResponseDescription = "Address Verification Failed"
	        Case "U"  getResponseDescription = "Card Security Code Failed"
	        Case "V"  getResponseDescription = "Address Verification and Card Security Code Failed"
	        Case "?"  getResponseDescription = "Transaction status is unknown"
	        Case Else getResponseDescription = "Unable to be determined"
	    End Select
	End Function


	' This function uses the AVSResultCode retrieved from the Digital Receipt
	' and returns an appropriate description.
	'
	' @param avsResultCode String containing the AVSResultCode
	' @return description String containing the appropriate description
	'
	Public function displayAVSResponse(avsResultCode)
		if avsResultCode <> "" then
		    select Case avsResultCode
		        Case "Unsupported"  displayAVSResponse = "AVS not supported or there was no AVS data provided"
		        Case "X"  displayAVSResponse = "Exact match - address and 9 digit ZIP/postal code"
		        Case "Y"  displayAVSResponse = "Exact match - address and 5 digit ZIP/postal code"
		        Case "S"  displayAVSResponse = "Service not supported or address not verified (international transaction)"
		        Case "G"  displayAVSResponse = "Issuer does not participate in AVS (international transaction)"
		        Case "A"  displayAVSResponse = "Address match only"
		        Case "W"  displayAVSResponse = "9 digit ZIP/postal code matched, Address not Matched"
		        Case "Z"  displayAVSResponse = "5 digit ZIP/postal code matched, Address not Matched"
		        Case "R"  displayAVSResponse = "Issuer system is unavailable"
		        Case "U"  displayAVSResponse = "Address unavailable or not verified"
		        Case "E"  displayAVSResponse = "Address and ZIP/postal code not provided"
		        Case "N"  displayAVSResponse = "Address and ZIP/postal code not matched"
		        Case "0"  displayAVSResponse = "AVS not requested"
		        Case Else displayAVSResponse = "Unable to be determined"
		    End Select
		else
		    displayAVSResponse = "null response"
		end if
	end function
	
	' ______________________________________________________________________________

	' This function uses the QSI CSC Result Code retrieved from the Digital
	' Receipt and returns an appropriate description for this code.
	'
	' @param vCSCResultCode String containing the QSI CSC Result Code
	' @return description String containing the appropriate description
	'
	Public function displayCSCResponse(vCSCResultCode)
	    if vCSCResultCode <> "" then
	        select Case vCSCResultCode
	        Case "Unsupported"  displayCSCResponse = "CSC not supported or there was no CSC data provided"
	        Case "M"  displayCSCResponse = "Exact code match"
	        Case "S"  displayCSCResponse = "Merchant has indicated that CSC is not present on the card (MOTO situation)"
	        Case "P"  displayCSCResponse = "Code not processed"
	        Case "U"  displayCSCResponse = "Card issuer is not registered and/or certified"
	        Case "N"  displayCSCResponse = "Code invalid or not matched"
	        Case Else displayCSCResponse = "Unable to be determined"
	        End Select
	    else
	        displayCSCResponse = "null response"
	    end if
	end function

End Class

%>
