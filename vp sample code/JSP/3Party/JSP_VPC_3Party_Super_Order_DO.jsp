<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%-- 
  ----------------- Disclaimer ------------------------------------------------
 
  Copyright © 2007 Dialect Payment Technologies - a Transaction Network
  Services company.  All rights reserved.
 
  This program is provided by Dialect Payment Technologies on the basis that
  you will treat it as confidential.
 
  No part of this program may be reproduced or copied in any form by any means
  without the written permission of Dialect Payment Technologies.  Unless
  otherwise expressly agreed in writing, the information contained in this
  program is subject to change without notice and Dialect Payment Technologies
  assumes no responsibility for any alteration to, or any error or other
  deficiency, in this program.
 
  1. All intellectual property rights in the program and in all extracts and 
     things derived from any part of the program are owned by Dialect and will 
     be assigned to Dialect on their creation. You will protect all the 
     intellectual property rights relating to the program in a manner that is 
     equal to the protection you provide your own intellectual property.  You 
     will notify Dialect immediately, and in writing where you become aware of 
     a breach of Dialect's intellectual property rights in relation to the
     program.
  2. The names "Dialect", "QSI Payments" and all similar words are trademarks
     of Dialect Payment Technologies and you must not use that name or any 
     similar name.
  3. Dialect may at its sole discretion terminate the rights granted in this 
     program with immediate effect by notifying you in writing and you will 
     thereupon return (or destroy and certify that destruction to Dialect) all 
     copies and extracts of the program in its possession or control.
  4. Dialect does not warrant the accuracy or completeness of the program or  
     its content or its usefulness to you or your merchant customers.  To the  
     extent permitted by law, all conditions and warranties implied by law  
     (whether as to fitness for any particular purpose or otherwise) are  
     excluded. Where the exclusion is not effective, Dialect limits its  
     liability to $100 or the resupply of the program (at Dialect's option).
  5. Data used in examples and sample data files are intended to be fictional 
     and any resemblance to real persons or companies is entirely coincidental.
  6. Dialect does not indemnify you or any third party in relation to the
    content or any use of the content as contemplated in these terms and 
     conditions. 
  7. Mention of any product not owned by Dialect does not constitute an 
     endorsement of that product.
  8. This program is governed by the laws of New South Wales, Australia and is 
     intended to be legally binding. 
  -----------------------------------------------------------------------------
 
  @author Dialect Payment Technologies
 
 ------------------------------------------------------------------------------
 PLEASE NOTE: 1. To utilise this example, you will need to have compiled the 
                 VPCPaymentConnection.java, VPC3PartyConnection.java and 
                 VPCPaymentCodesHelper.java and include the compiled classes in 
                 your classpath for this example to work.
              
              2. For this example to work correctly, you will need to set the 
                 Secure Secret value.
 ------------------------------------------------------------------------------             
--%>
<%@ page import="java.util.List,
                 java.util.ArrayList,
                 java.util.Collections,
                 java.util.Iterator,
                 java.util.Enumeration,
                 java.security.MessageDigest,
                 java.util.Map,
                 java.net.URLEncoder,
                 java.util.HashMap,
                 com.Dialect.*"%>
                 
<%      // *******************************************
        // START OF MAIN PROGRAM
        // *******************************************

        // The Page does a redirect to the Virtual Payment Client
        VPC3PartyConnection vpcconn = new VPC3PartyConnection();
        // ****************
        // This is secret for encoding the MD5 hash
        // This secret will vary from merchant to merchant
        String secureSecret = "";
        
        vpcconn.setSecureSecret(secureSecret);
        
        // retrieve all the parameters into a hash map
	Map fields = new HashMap();
	for (Enumeration en = request.getParameterNames(); en.hasMoreElements();) {
		String fieldName = (String) en.nextElement();
		String fieldValue = request.getParameter(fieldName);
		if ((fieldValue != null) && (fieldValue.length() > 0)) {
			fields.put(fieldName, fieldValue);
		}
	}
        
	// no need to send the vpc url, EnableAVSdata and submit button to the vpc
	String vpcURL = (String) fields.remove("virtualPaymentClientURL");
	fields.remove("SubButL");
	
        // Retrieve the order page URL from the incoming order page and add it to 
        // the hash map. This is only here to give the user the easy ability to go 
	// back to the Order page. This would not be required in a production system
        // NB. Other merchant application fields can be added in the same manner
	String againLink = request.getHeader("Referer");
	fields.put("AgainLink", againLink);
        

	// Create MD5 secure hash and insert it into the hash map if it was created
	// created. Remember if secureSecret = "" it will not be created
	if (secureSecret != null && secureSecret.length() > 0) {
		String secureHash = vpcconn.hashAllFields(fields);
		fields.put("vpc_SecureHash", secureHash);
	}
	
	// Create a redirection URL
	StringBuffer buf = new StringBuffer();
	buf.append(vpcURL).append('?');
	vpcconn.appendQueryFields(buf, fields);

	// Redirect to Virtual PaymentClient
        response.sendRedirect(buf.toString());
%>