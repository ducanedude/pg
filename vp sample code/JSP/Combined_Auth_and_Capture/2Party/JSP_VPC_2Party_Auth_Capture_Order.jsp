<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%-- 
  ----------------- Disclaimer ------------------------------------------------
 
  Copyright © 2007 Dialect Payment Technologies - a Transaction Network
  Services company.  All rights reserved.
 
  This program is provided by Dialect Payment Technologies on the basis that
  you will treat it as confidential.
 
  No part of this program may be reproduced or copied in any form by any means
  without the written permission of Dialect Payment Technologies.  Unless
  otherwise expressly agreed in writing, the information contained in this
  program is subject to change without notice and Dialect Payment Technologies
  assumes no responsibility for any alteration to, or any error or other
  deficiency, in this program.
 
  1. All intellectual property rights in the program and in all extracts and 
     things derived from any part of the program are owned by Dialect and will 
     be assigned to Dialect on their creation. You will protect all the 
     intellectual property rights relating to the program in a manner that is 
     equal to the protection you provide your own intellectual property.  You 
     will notify Dialect immediately, and in writing where you become aware of 
     a breach of Dialect's intellectual property rights in relation to the
     program.
  2. The names "Dialect", "QSI Payments" and all similar words are trademarks
     of Dialect Payment Technologies and you must not use that name or any 
     similar name.
  3. Dialect may at its sole discretion terminate the rights granted in this 
     program with immediate effect by notifying you in writing and you will 
     thereupon return (or destroy and certify that destruction to Dialect) all 
     copies and extracts of the program in its possession or control.
  4. Dialect does not warrant the accuracy or completeness of the program or  
     its content or its usefulness to you or your merchant customers.  To the  
     extent permitted by law, all conditions and warranties implied by law  
     (whether as to fitness for any particular purpose or otherwise) are  
     excluded. Where the exclusion is not effective, Dialect limits its  
     liability to $100 or the resupply of the program (at Dialect's option).
  5. Data used in examples and sample data files are intended to be fictional 
     and any resemblance to real persons or companies is entirely coincidental.
  6. Dialect does not indemnify you or any third party in relation to the
    content or any use of the content as contemplated in these terms and 
     conditions. 
  7. Mention of any product not owned by Dialect does not constitute an 
     endorsement of that product.
  8. This program is governed by the laws of New South Wales, Australia and is 
     intended to be legally binding. 
  -----------------------------------------------------------------------------

  @author Dialect Payment Technologies
 
 ------------------------------------------------------------------------------
 PLEASE NOTE: 1. To utilise this example, you will need to have compiled the 
                 VPCPaymentConnection.java and VPCPaymentCodesHelper.java and
                 include the compiled classes in your classpath for this example
                 to work.
                 
              2. For this example to work correctly, you will need to set the 
                 Access Code, Username and Password.
 ------------------------------------------------------------------------------             
--%>
<%@ page import="com.sun.net.ssl.*,
java.io.*,
java.net.*,
java.util.*,
java.security.MessageDigest,
java.security.cert.X509Certificate,
javax.net.ssl.SSLSocket,
javax.net.ssl.SSLSocketFactory,
com.sun.net.ssl.SSLContext,
com.sun.net.ssl.X509TrustManager,
com.Dialect.*"%>

<%  // *******************************************
    // START OF MAIN PROGRAM
    // *******************************************

    // Define Variables
    // If using a proxy server you must set the following variables
    // If NOT using a proxy server then set the 'useProxy' to false
    boolean useProxy = false;
    String proxyHost = "";
    int proxyPort = 0;

    // Standard Receipt Data
    String captAmount           = "";
    String captLocale           = "";
    String captBatchNo          = "";
    String captCommand          = "";
    String captVersion          = "";
    String captOrderInfo        = "";
    String captReceiptNo        = "";
    String captAuthorizeID      = "";
    String captTransactionNr    = "";
    String captAcqResponseCode  = "";
    String captTxnResponseCode  = "";
    String captMerchTxnRef      = "";

    String captTxnResponseCodeDesc = "";

    // Capture Data
    String captAuthorisedAmount = "";
    String captCapturedAmount   = "";
    String captRefundedAmount   = "";
    String captTicketNumber     = "";

    String captError = "";

    // create new element that uses the VPC Payment Connection class
    VPCPaymentConnection vpcconn = new VPCPaymentConnection();

    // retrieve all the parameters into a hash map
    Map requestFields = new HashMap();
    for (Enumeration Enum = request.getParameterNames(); Enum.hasMoreElements();) {
        String fieldName = (String) Enum.nextElement();
        String fieldValue = request.getParameter(fieldName);
        if ((fieldValue != null) && (fieldValue.length() > 0)) {
            requestFields.put(fieldName, fieldValue);
        }
    }

    // no need to send the vpcURL, Title and Submit Button to the vpc
    String vpcURL = (String) requestFields.remove("virtualPaymentClientURL");
    String title  = (String) requestFields.remove("Title");
    requestFields.remove("SubButL");

    // Retrieve the order page URL from the incoming order page. This is only  
    // here to give the user the easy ability to go back to the Order page. 
    // This would not be required in a production system.
    String againLink = request.getHeader("Referer");

    // create the post data string to send
    String postData = vpcconn.createPostDataFromMap(requestFields);

    String resQS = "";
    String message = "";

    try {
        // create a URL connection to the Virtual Payment Client
        resQS = vpcconn.doPost(vpcURL, postData, useProxy, proxyHost, proxyPort);

    } catch (Exception ex) {
        // The response is an error message so generate an Error Page
        message = ex.toString();
    } //try-catch

    // create a hash map for the response data
    Map responseFields = vpcconn.createMapFromResponse(resQS);

    // Extract the available receipt fields from the VPC Response
    // If not present then let the value be equal to 'No Value returned'
    // Not all data fields will return values for all transactions.

    // don't overwrite message if any error messages detected
    if (message.length() == 0) {
        message            = vpcconn.null2unknown("vpc_Message", responseFields);
    }

    // Standard Receipt Data
    String amount          = vpcconn.null2unknown("vpc_Amount", responseFields);
    String locale          = vpcconn.null2unknown("vpc_Locale", responseFields);
    String batchNo         = vpcconn.null2unknown("vpc_BatchNo", responseFields);
    String command         = vpcconn.null2unknown("vpc_Command", responseFields);
    String version         = vpcconn.null2unknown("vpc_Version", responseFields);
    String cardType        = vpcconn.null2unknown("vpc_Card", responseFields);
    String orderInfo       = vpcconn.null2unknown("vpc_OrderInfo", responseFields);
    String receiptNo       = vpcconn.null2unknown("vpc_ReceiptNo", responseFields);
    String merchantID      = vpcconn.null2unknown("vpc_Merchant", responseFields);
    String merchTxnRef     = vpcconn.null2unknown("vpc_MerchTxnRef", responseFields);
    String authorizeID     = vpcconn.null2unknown("vpc_AuthorizeId", responseFields);
    String transactionNo   = vpcconn.null2unknown("vpc_TransactionNo", responseFields);
    String acqResponseCode = vpcconn.null2unknown("vpc_AcqResponseCode", responseFields);
    String txnResponseCode = vpcconn.null2unknown("vpc_TxnResponseCode", responseFields);

    // CSC Receipt Data
    String cscResultCode  = vpcconn.null2unknown("vpc_CSCResultCode", responseFields);
    String cscACQRespCode = vpcconn.null2unknown("vpc_AcqCSCRespCode", responseFields);

    // AVS Receipt Data
    String avsResultCode  = vpcconn.null2unknown("vpc_AVSResultCode", responseFields);
    String avsACQRespCode = vpcconn.null2unknown("vpc_AcqAVSRespCode", responseFields);

    // Get the descriptions behind the QSI, CSC and AVS Response Codes
    // Only get the descriptions if the string returned is not equal to "No Value Returned".

    String txnResponseCodeDesc = "";
    String cscResultCodeDesc = "";
    String avsResultCodeDesc = "";

    // establish the VPCPaymentCodesHelper to retreive the response descriptions
    VPCPaymentCodesHelper vpchelper = new VPCPaymentCodesHelper();

    if (txnResponseCode != "No Value Returned") {
        txnResponseCodeDesc = vpchelper.getResponseDescription(txnResponseCode);
    }

    if (cscResultCode != "No Value Returned") {
        cscResultCodeDesc = vpchelper.displayCSCResponse(cscResultCode);
    }

    if (avsResultCode != "No Value Returned") {
        avsResultCodeDesc = vpchelper.displayAVSResponse(avsResultCode);
    }    


    String error = "";
    // Show this page as an error page if error condition
    if (txnResponseCode.equals("7") || txnResponseCode.equals("No Value Returned")) {
        error = "Error ";
    }

    // If the authorisation was successful, now proceed with the capture.
    // Username and password should remain hidden from users in the request and therefore
    // is not being passed in from the html page
    boolean captAttempted = false;
    String captMessage = "";

    if (txnResponseCode.equals("0")) {
        captAttempted = true;    
        String username = "";
        String password = "";
        String accessCode = "";
        String captVpcURL = vpcURL;

        // Place all the fields required for the capture request into a hash map
        Map captRequestFields = new HashMap();

        captRequestFields.put("vpc_Version", "1");
        captRequestFields.put("vpc_Command", "capture");
        captRequestFields.put("vpc_AccessCode", accessCode);
        captRequestFields.put("vpc_MerchTxnRef", merchTxnRef + "-C");
        captRequestFields.put("vpc_Merchant", merchantID);
        captRequestFields.put("vpc_TransNo", transactionNo);
        captRequestFields.put("vpc_Amount", amount);
        captRequestFields.put("vpc_User", username);
        captRequestFields.put("vpc_Password", password);

        // create the post data string to send for capture request
        String captPostData = vpcconn.createPostDataFromMap(captRequestFields);

        String captResQS = "";

        try {
            // create a URL connection to the Virtual Payment Client
            captResQS = vpcconn.doPost(captVpcURL, captPostData, useProxy, proxyHost, proxyPort);

        } catch (Exception ex) {
            // The response is an error message so generate an Error Page
            captMessage = ex.toString();
        } //try-catch

        // create a hash map for the response data
        Map captResponseFields = vpcconn.createMapFromResponse(captResQS);

        // Extract the available receipt fields from the VPC Response
        // If not present then let the value be equal to 'No Value returned'
        // Not all data fields will return values for all transactions.

        // don't overwrite message if any error messages detected
        if (captMessage.length() == 0) {
            captMessage            = vpcconn.null2unknown("vpc_Message", captResponseFields);
        }

        // Standard Receipt Data
        captAmount          = vpcconn.null2unknown("vpc_Amount", captResponseFields);
        captLocale          = vpcconn.null2unknown("vpc_Locale", captResponseFields);
        captBatchNo         = vpcconn.null2unknown("vpc_BatchNo", captResponseFields);
        captCommand         = vpcconn.null2unknown("vpc_Command", captResponseFields);
        captVersion         = vpcconn.null2unknown("vpc_Version", captResponseFields);
        captOrderInfo       = vpcconn.null2unknown("vpc_OrderInfo", captResponseFields);
        captReceiptNo       = vpcconn.null2unknown("vpc_ReceiptNo", captResponseFields);
        captAuthorizeID     = vpcconn.null2unknown("vpc_AuthorizeId", captResponseFields);
        captTransactionNr   = vpcconn.null2unknown("vpc_TransactionNo", captResponseFields);
        captAcqResponseCode = vpcconn.null2unknown("vpc_AcqResponseCode", captResponseFields);
        captTxnResponseCode = vpcconn.null2unknown("vpc_TxnResponseCode", captResponseFields);
        captMerchTxnRef     = vpcconn.null2unknown("vpc_MerchTxnRef", captResponseFields);

        if (!captTxnResponseCode.equals("No Value Returned")) {
            captTxnResponseCodeDesc = vpchelper.getResponseDescription(captTxnResponseCode);
        }    

        // Capture Data
        captAuthorisedAmount = vpcconn.null2unknown("vpc_AuthorisedAmount", captResponseFields);
        captCapturedAmount   = vpcconn.null2unknown("vpc_CapturedAmount", captResponseFields);
        captRefundedAmount   = vpcconn.null2unknown("vpc_RefundedAmount", captResponseFields);
        captTicketNumber     = vpcconn.null2unknown("vpc_TicketNo", captResponseFields);


        if (captTxnResponseCode.equals("7") || captTxnResponseCode.equals("No Value Returned")) {
            captError = "Error ";
        }
    } else {
        captMessage = "Capture not attempted due to Authorisation Failure - see above";
    }

    // FINISH TRANSACTION - Process the VPC Response Data
    // =====================================================
    // For the purposes of demonstration, we simply display the Response fields
    // on a web page.
%>                 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title><%=title%> - <%=error%>Response Page</title>
        <meta http-equiv="Content-Type" content="text/html, charset=iso-8859-1">
        <style type="text/css">
            <!--
            h1       { font-family:Arial,sans-serif; font-size:20pt; font-weight:600; margin-bottom:0.1em; color:#08185A;}
            h2       { font-family:Arial,sans-serif; font-size:14pt; font-weight:100; margin-top:0.1em; color:#08185A;}
            h2.co    { font-family:Arial,sans-serif; font-size:24pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
            h3       { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
            h3.co    { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#FFFFFF}
            body     { font-family:Verdana,Arial,sans-serif; font-size:10pt; background-color:#FFFFFF; color:#08185A}
            th       { font-family:Verdana,Arial,sans-serif; font-size:8pt; font-weight:bold; background-color:#CED7EF; padding-top:0.5em; padding-bottom:0.5em;  color:#08185A}
            tr       { height:25px; }
            .shade   { height:25px; background-color:#CED7EF }
            .title   { height:25px; background-color:#0074C4 }
            td       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A }
            td.red   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
            td.green { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#008800 }
            p        { font-family:Verdana,Arial,sans-serif; font-size:10pt; color:#FFFFFF }
            p.blue   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#08185A }
            p.red    { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
            p.green  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#008800 }
            div.bl   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#0074C4 }
            div.red  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
            li       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
            input    { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold }
            select   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold; }
            textarea { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:normal; scrollbar-arrow-color:#08185A; scrollbar-base-color:#CED7EF }
            -->
        </style>
    </head>
    <body>
        <!-- Start Branding Table -->
        <table width="100%" border="2" cellpadding="2" class="title">
            <tr>
                <td class="shade" width="90%"><h2 class="co">&nbsp;Amex Virtual Payment Client Example</h2></td>
                <td class="title" align="center"><h3 class="co">Dialect<br />Payments</h3></td>
            </tr>
        </table>
        <!-- End Branding Table -->
        <center><h1><%=title%> - <%=error%>Response Page</h1></center>
        <table width="85%" align="center" cellpadding="5" border="0">
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Basic 2-Party Transaction Fields</strong></p></td>
            </tr>
            <tr>
                <td align="right" width="50%"><strong><i>VPC API Version: </i></strong></td>
                <td width="50%"><%=version%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Command: </i></strong></td>
                <td><%=command%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Authorisation Merchant Transaction Reference: </i></strong></td>
                <td><%=merchTxnRef%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Merchant ID: </i></strong></td>
                <td><%=merchantID%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Order Information: </i></strong></td>
                <td><%=orderInfo%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Authorisation Amount: </i></strong></td>
                <td><%=amount%></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <font color="#0074C4">Fields above are the primary request values.<br />
                        <hr />
                    Fields below are the response fields for a Standard Transaction.<br /></font>
                </td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>VPC Authorisation Transaction Response Code: </i></strong></td>
                <td><%=txnResponseCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Authorisation Transaction Response Code Description: </i></strong></td>
                <td><%=txnResponseCodeDesc%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Authorisation Message: </i></strong></td>
                <td><%=message%></td>
            </tr>
            <% 
            // Only display the following fields if not an error condition
            if (!txnResponseCode.equals("7") && !txnResponseCode.equals("No Value Returned")) { 
            %>
            <tr>
                <td align="right"><strong><i>Authorisation Receipt Number: </i></strong></td>
                <td><%=receiptNo%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Shopping Transaction Number: </i></strong></td>
                <td><%=transactionNo%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Authorisation Acquirer Response Code: </i></strong></td>
                <td><%=acqResponseCode%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i> Authorisation ID: </i></strong></td>
                <td><%=authorizeID%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Authorisation Batch Number: </i></strong></td>
                <td><%=batchNo%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Card Type: </i></strong></td>
                <td><%=cardType%></td>
            </tr>
            <% if (captAttempted) {
            %>
            <tr class="title">
                <td colspan="2"><p><strong>&nbsp;Capture Transaction Receipt Fields</strong></p></td>
            </tr>
            <tr>    
                <td align="right"><strong><em>Capture Merchant Transaction Reference: </em></strong></td>
                <td><%=captMerchTxnRef%></td>
            </tr>
            <tr class ="shade">    
                <td align="right"><strong><em>Capture Financial Transaction Number: </em></strong></td>
                <td><%=captTransactionNr%></td>
            </tr>
            <tr>    
                <td align="right"><strong><em>Capture Amount: </em></strong></td>
                <td><%=captAmount%></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div class='bl'>Fields above are the primary request values.<hr>Fields below are receipt data fields.</div>
                </td>
            </tr>
            <tr>    
                <td align="right"><strong><em>Capture QSI Response Code: </em></strong></td>
                <td><%=captTxnResponseCode%></td>
            </tr>
            <tr class="shade">
                <td align='right'><strong><em>Capture QSI Response Code Description: </em></strong></td>
                <td><%=captTxnResponseCodeDesc%></td>
            </tr>
            <tr>    
                <td align="right"><strong><em>Capture Acquirer Response Code: </em></strong></td>
                <td><%=captAcqResponseCode%></td>
            </tr>
            <tr class='shade'>
                <td align='right'><em><strong>Receipt No: </strong></em></td>
                <td><%=captReceiptNo%></td>
            </tr>
            <tr>                  
                <td align='right'><em><strong>Authorize Id: </strong></em></td>
                <td><%=captAuthorizeID%></td>
            </tr>
            <%
            } else {
            
            %>
            <tr class='title'>
                <td colspan='2'><p><strong>&nbsp;Capture Transaction Receipt Fields</strong></p></td>
            </tr>
            <tr>
                <td align='right'><strong><em>Capture Error: </em></strong></td>
                <td><%=captMessage%></td>
            </tr>
            <%    
            }
            %>
            <tr>
                <td colspan="2" align="center">
                    <font color="#0074C4">Fields above are for a Standard Transaction<br />
                        <hr />
                    Fields below are additional fields for extra functionality.</font><br />
                </td>
            </tr>
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Card Security Code Fields</strong></p></td>
            </tr>
            <tr>
            <tr class='shade'>
                <td align="right"><strong><i>Authorisation CSC Acquirer Response Code: </i></strong></td>
                <td><%=cscACQRespCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Authorisation CSC QSI Result Code: </i></strong></td>
                <td><%=cscResultCode%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Authorisation CSC Result Description: </i></strong></td>
                <td><%=cscResultCodeDesc%></td>
            </tr>
            <tr>
                <td colspan="2"><hr /></td>
            </tr>
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Address Verification Service Fields</strong></p></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Authorisation AVS Acquirer Response Code: </i></strong></td>
                <td><%=avsACQRespCode%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Authorisation AVS QSI Result Code: </i></strong></td>
                <td><%=avsResultCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Authorisation AVS Result Description: </i></strong></td>
                <td><%=avsResultCodeDesc%></td>
 
            
            <%         
            }
            } %>
            
        </table>
        <center><p><a href='<%=againLink%>'>New Transaction</a></p></center>
    </body>
</html>