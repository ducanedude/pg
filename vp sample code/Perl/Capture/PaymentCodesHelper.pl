#!/usr/bin/perl -w

package PaymentCodesHelper;

# This example assumes that a form has been sent to this example with the
# required fields. The example then processes the command and displays the
# result or error to a HTML page in the users web browser.


# Initialisation
# ==============
# Use the required Perl Libraries
# -------------------------------
use strict;
#use diagnostics;

my $VERSION = '2.0.2';

# This sub allows an external application to determine the version of this
# package
sub getPackageVersion ()
{
	return $VERSION;
}

# This method uses the QSIResponseCode retrieved from the Digital Receipt
# and returns an appropriate description
sub getResponseDescription ($)
{
	my ($responseCode) = @_;
	my %qsiResponse = 
	(
		Unknown => "null response",
		0   => "Transaction Successful",
		'?' => "Transaction status is unknown",
		1   => "Unknown Error",
		2   => "Bank Declined Transaction",
		3   => "No Reply from Bank",
		4   => "Expired Card",
		5   => "Insufficient funds",
		6   => "Error Communicating with Bank",
		7   => "Payment Server System Error",
		8   => "Transaction Type Not Supported",
		9   => "Bank declined transaction (Do not contact Bank)",
		A   => "Transaction Aborted",
		B   => "Transaction Declined - Contact the Bank",
		C   => "Transaction Cancelled",
		D   => "Deferred transaction has been received and is awaiting processing",
		F   => "3-D Secure Authentication failed",
		I   => "Card Security Code verification failed",
		L   => "Shopping Transaction Locked (Please try the transaction again later)",
		N   => "Cardholder is not enrolled in Authentication scheme",
		P   => "Transaction has been received by the Payment Adaptor and is being processed",
		R   => "Transaction was not processed - Reached limit of retry attempts allowed",
		S   => "Duplicate SessionID",
		T   => "Address Verification Failed",
		U   => "Card Security Code Failed",
		V   => "Address Verification and Card Security Code Failed"
	);

	if ( defined($responseCode) and exists $qsiResponse{$responseCode} ) {
		return $qsiResponse{$responseCode};
	}
	else
	{
		return "Unable to be determined";
	}
}

# This method uses the AVSResultCode retrieved from the Digital Receipt
# and returns an appropriate description
sub getAVSDescription ($)
{
	my ($resultCode) = @_;
	my %avsResult =
	(
		Unsupported => "AVS not supported or there was no AVS data provided",
		X => "Exact match - address and 9 digit ZIP/postal code",
		Y => "Exact match - address and 5 digit ZIP/postal code",
		S => "Service not supported or address not verified (international transaction)",
		G => "Issuer does not participate in AVS (international transaction)",
		A => "Address match only.",
		W => "9 digit ZIP/postal code matched, Address not Matched",
		Z => "5 digit ZIP/postal code matched, Address not Matched",
		R => "Issuer system is unavailable",
		U => "Address unavailable or not verified",
		E => "Address and ZIP/postal code not provided",
		N => "Address and ZIP/postal code not matched",
		0 => "AVS not requested"
	);

	if ( defined($resultCode) and exists $avsResult{$resultCode} ) {
		return $avsResult{$resultCode};
	}
	else
	{
		return "Unable to be determined";
	}
}

# This method uses the CSC Response code retrieved from the Digital
# Receipt and returns an appropriate description for the CSC Response Code
sub getCSCDescription ($)
{
	my ($resultCode) = @_;
	my %cscResult = (
		Unsupported => "CSC not supported or there was no CSC data provided",
		M           => "Exact code match",
		S           => "Merchant has indicated that CSC is not present on the card (MOTO situation)",
		P           => "Code not processed",
		U           => "Card issuer is not registered and/or certified",
		N           => "Code invalid or not matched"
	);

	if ( defined($resultCode) and exists $cscResult{$resultCode} ) {
		return $cscResult{$resultCode};
	}
	else
	{
		return "Unable to be determined";
	}
}

