#!/usr/bin/perl -w

# This example assumes that a form has been sent to this example with the
# required fields. The example then processes the command and displays the
# result or error to a HTML page in the users web browser.

# Initialisation
# ==============
# Use the required Perl Libraries
# -------------------------------
use strict;
use CGI;

require 'VPCConnection.pl';
require 'PaymentCodesHelper.pl';
#use diagnostics;

my $VERSION = '2.0.2';

# Sub Prototypes
sub doRedirect($);

my $perl_cgi = new CGI;
my %params = $perl_cgi->Vars;

# prepare to output to browser
print $perl_cgi->header(
	-expires => '0',
	pragma   => 'no-cache',
	cache    => 'no-cache'
);

my $SECURE_SECRET = "";
my $SECURE_SECRET_ISVALID = undef;

# Add the Digital Order Fields
# Add the Core Fields
if (length($params{'vpc_Version'}) > 0) { VPCConnection::addCommandField("vpc_Version", $params{'vpc_Version'}); }
if (length($params{'vpc_Command'}) > 0) { VPCConnection::addCommandField("vpc_Command", $params{'vpc_Command'}); }
if (length($params{'vpc_AccessCode'}) > 0) { VPCConnection::addCommandField("vpc_AccessCode", $params{'vpc_AccessCode'}); }
if (length($params{'vpc_MerchTxnRef'}) > 0) { VPCConnection::addCommandField("vpc_MerchTxnRef", $params{'vpc_MerchTxnRef'}); }
if (length($params{'vpc_Merchant'}) > 0) { VPCConnection::addCommandField("vpc_Merchant", $params{'vpc_Merchant'}); }
if (length($params{'vpc_OrderInfo'}) > 0) { VPCConnection::addCommandField("vpc_OrderInfo", $params{'vpc_OrderInfo'}); }
if (length($params{'vpc_Amount'}) > 0) { VPCConnection::addCommandField("vpc_Amount", $params{'vpc_Amount'}); }
if (length($params{'vpc_ReturnURL'}) > 0) { VPCConnection::addCommandField("vpc_ReturnURL", $params{'vpc_ReturnURL'}); }

# add Ticket Number Detail
if (length($params{'vpc_TicketNo'}) > 0) { VPCConnection::addCommandField("vpc_TicketNo", $params{'vpc_TicketNo'}); }

# add Transaction Source and SubType
if (length($params{'vpc_TxSource'}) > 0) { VPCConnection::addCommandField("vpc_TxSource", $params{'vpc_TxSource'}); }
if (length($params{'vpc_TxSourceSubType'}) > 0) { VPCConnection::addCommandField("vpc_TxSourceSubType", $params{'vpc_TxSourceSubType'}); }

# add Card Details
if (length($params{'vpc_CardNum'}) > 0) { VPCConnection::addCommandField("vpc_CardNum", $params{'vpc_CardNum'}); }
if (length($params{'vpc_CardExp'}) > 0) { VPCConnection::addCommandField("vpc_CardExp", VPCConnection::formatExpiryDate($params{'CardExpMonth'}, $params{'CardExpYear'})); }

# add CSC Details
if (length($params{'vpc_CardSecurityCode'}) > 0) { VPCConnection::addCommandField("vpc_CardSecurityCode", $params{'vpc_CardSecurityCode'}); }

# add AVS/AAV Details
if (length($params{'vpc_BillTo_Title'}) > 0) { VPCConnection::addCommandField("vpc_BillTo_Title", $params{'vpc_BillTo_Title'}); }
if (length($params{'vpc_BillTo_Firstname'}) > 0) { VPCConnection::addCommandField("vpc_BillTo_Firstname", $params{'vpc_BillTo_Firstname'}); }
if (length($params{'vpc_BillTo_Middlename'}) > 0) { VPCConnection::addCommandField("vpc_BillTo_Middlename", $params{'vpc_BillTo_Middlename'}); }
if (length($params{'vpc_BillTo_Lastname'}) > 0) { VPCConnection::addCommandField("vpc_BillTo_Lastname", $params{'vpc_BillTo_Lastname'}); }
if (length($params{'vpc_BillTo_Phone'}) > 0) { VPCConnection::addCommandField("vpc_BillTo_Phone", $params{'vpc_BillTo_Phone'}); }
if (length($params{'vpc_AVS_Street01'}) > 0) { VPCConnection::addCommandField("vpc_AVS_Street01", $params{'vpc_AVS_Street01'}); }
if (length($params{'vpc_AVS_City'}) > 0) { VPCConnection::addCommandField("vpc_AVS_City", $params{'vpc_AVS_City'}); }
if (length($params{'vpc_AVS_StateProv'}) > 0) { VPCConnection::addCommandField("vpc_AVS_StateProv", $params{'vpc_AVS_StateProv'}); }
if (length($params{'vpc_AVS_PostCode'}) > 0) { VPCConnection::addCommandField("vpc_AVS_PostCode", $params{'vpc_AVS_PostCode'}); }
if (length($params{'vpc_AVS_Country'}) > 0) { VPCConnection::addCommandField("vpc_AVS_Country", $params{'vpc_AVS_Country'}); }
if (length($params{'vpc_ShipTo_Fullname'}) > 0) { VPCConnection::addCommandField("vpc_ShipTo_Fullname", $params{'vpc_ShipTo_Fullname'}); }
if (length($params{'vpc_ShipTo_Title'}) > 0) { VPCConnection::addCommandField("vpc_ShipTo_Title", $params{'vpc_ShipTo_Title'}); }
if (length($params{'vpc_ShipTo_Firstname'}) > 0) { VPCConnection::addCommandField("vpc_ShipTo_Firstname", $params{'vpc_ShipTo_Firstname'}); }
if (length($params{'vpc_ShipTo_Middlename'}) > 0) { VPCConnection::addCommandField("vpc_ShipTo_Middlename", $params{'vpc_ShipTo_Middlename'}); }
if (length($params{'vpc_ShipTo_Lastname'}) > 0) { VPCConnection::addCommandField("vpc_ShipTo_Lastname", $params{'vpc_ShipTo_Lastname'}); }
if (length($params{'vpc_ShipTo_Phone'}) > 0) { VPCConnection::addCommandField("vpc_ShipTo_Phone", $params{'vpc_ShipTo_Phone'}); }
if (length($params{'vpc_ShipTo_Street01'}) > 0) { VPCConnection::addCommandField("vpc_ShipTo_Street01", $params{'vpc_ShipTo_Street01'}); }
if (length($params{'vpc_ShipTo_City'}) > 0) { VPCConnection::addCommandField("vpc_ShipTo_City", $params{'vpc_ShipTo_City'}); }
if (length($params{'vpc_ShipTo_StateProv'}) > 0) { VPCConnection::addCommandField("vpc_ShipTo_StateProv", $params{'vpc_ShipTo_StateProv'}); }
if (length($params{'vpc_ShipTo_PostCode'}) > 0) { VPCConnection::addCommandField("vpc_ShipTo_PostCode", $params{'vpc_ShipTo_PostCode'}); }
if (length($params{'vpc_ShipTo_Country'}) > 0) { VPCConnection::addCommandField("vpc_ShipTo_Country", $params{'vpc_ShipTo_Country'}); }

# add Airline Passenger Data Details
if (length($params{'vpc_APD_DeptDate'}) > 0) { VPCConnection::addCommandField("vpc_APD_DeptDate", $params{'vpc_APD_DeptDate'}); }
if (length($params{'vpc_APD_PassengerTitle'}) > 0) { VPCConnection::addCommandField("vpc_APD_PassengerTitle", $params{'vpc_APD_PassengerTitle'}); }
if (length($params{'vpc_APD_PassengerFirstname'}) > 0) { VPCConnection::addCommandField("vpc_APD_PassengerFirstname", $params{'vpc_APD_PassengerFirstname'}); }
if (length($params{'vpc_APD_PassengerMiddlename'}) > 0) { VPCConnection::addCommandField("vpc_APD_PassengerMiddlename", $params{'vpc_APD_PassengerMiddlename'}); }
if (length($params{'vpc_APD_PassengerLastname'}) > 0) { VPCConnection::addCommandField("vpc_APD_PassengerLastname", $params{'vpc_APD_PassengerLastname'}); }
if (length($params{'vpc_APD_CardmemberTitle'}) > 0) { VPCConnection::addCommandField("vpc_APD_CardmemberTitle", $params{'vpc_APD_CardmemberTitle'}); }
if (length($params{'vpc_APD_CardmemberFirstname'}) > 0) { VPCConnection::addCommandField("vpc_APD_CardmemberFirstname", $params{'vpc_APD_CardmemberFirstname'}); }
if (length($params{'vpc_APD_CardmemberMiddlename'}) > 0) { VPCConnection::addCommandField("vpc_APD_CardmemberMiddlename", $params{'vpc_APD_CardmemberMiddlename'}); }
if (length($params{'vpc_APD_CardmemberLastname'}) > 0) { VPCConnection::addCommandField("vpc_APD_CardmemberLastname", $params{'vpc_APD_CardmemberLastname'}); }
if (length($params{'vpc_APD_Origin'}) > 0) { VPCConnection::addCommandField("vpc_APD_Origin", $params{'vpc_APD_Origin'}); }
if (length($params{'vpc_APD_Dest'}) > 0) { VPCConnection::addCommandField("vpc_APD_Dest", $params{'vpc_APD_Dest'}); }
if (length($params{'vpc_APD_Route'}) > 0) { VPCConnection::addCommandField("vpc_APD_Route", $params{'vpc_APD_Route'}); }
if (length($params{'vpc_APD_Carriers'}) > 0) { VPCConnection::addCommandField("vpc_APD_Carriers", $params{'vpc_APD_Carriers'}); }
if (length($params{'vpc_APD_FareBasis'}) > 0) { VPCConnection::addCommandField("vpc_APD_FareBasis", $params{'vpc_APD_FareBasis'}); }
if (length($params{'vpc_APD_NumPassengers'}) > 0) { VPCConnection::addCommandField("vpc_APD_NumPassengers", $params{'vpc_APD_NumPassengers'}); }
if (length($params{'vpc_APD_eTicket'}) > 0) { VPCConnection::addCommandField("vpc_APD_eTicket", $params{'vpc_APD_eTicket'}); }
if (length($params{'vpc_APD_ResCode'}) > 0) { VPCConnection::addCommandField("vpc_APD_ResCode", $params{'vpc_APD_ResCode'}); }
if (length($params{'vpc_APD_TravelAgentCode'}) > 0) { VPCConnection::addCommandField("vpc_APD_TravelAgentCode", $params{'vpc_APD_TravelAgentCode'}); }

# add Internet Source Data Details
if (length($params{'vpc_CustomerEmail'}) > 0) { VPCConnection::addCommandField("vpc_CustomerEmail", $params{'vpc_CustomerEmail'}); }
if (length($params{'vpc_ITD_CustomerHostname'}) > 0) { VPCConnection::addCommandField("vpc_ITD_CustomerHostname", $params{'vpc_ITD_CustomerHostname'}); }
if (length($params{'vpc_ITD_CustomerBrowser'}) > 0) { VPCConnection::addCommandField("vpc_ITD_CustomerBrowser", $params{'vpc_ITD_CustomerBrowser'}); }
if (length($params{'vpc_ITD_ShipMethodCode'}) > 0) { VPCConnection::addCommandField("vpc_ITD_ShipMethodCode", $params{'vpc_ITD_ShipMethodCode'}); }
if (length($params{'vpc_ITD_ShipToCountryCode'}) > 0) { VPCConnection::addCommandField("vpc_ITD_ShipToCountryCode", $params{'vpc_ITD_ShipToCountryCode'}); }
if (length($params{'vpc_ITD_MerchantSKU'}) > 0) { VPCConnection::addCommandField("vpc_ITD_MerchantSKU", $params{'vpc_ITD_MerchantSKU'}); }
if (length($params{'vpc_CustomerIpAddress'}) > 0) { VPCConnection::addCommandField("vpc_CustomerIpAddress", $params{'vpc_CustomerIpAddress'}); }
if (length($params{'vpc_ITD_CustomerANI'}) > 0) { VPCConnection::addCommandField("vpc_ITD_CustomerANI", $params{'vpc_ITD_CustomerANI'}); }
if (length($params{'vpc_ITD_CustomerANICallType'}) > 0) { VPCConnection::addCommandField("vpc_ITD_CustomerANICallType", $params{'vpc_ITD_CustomerANICallType'}); }

# add Card Present (Track Data) Details
if (length($params{'vpc_CardTrack1'}) > 0) { VPCConnection::addCommandField("vpc_CardTrack1", $params{'vpc_CardTrack1'}); }
if (length($params{'vpc_CardTrack2'}) > 0) { VPCConnection::addCommandField("vpc_CardTrack2", $params{'vpc_CardTrack2'}); }

# Sign the data if a $SECURE_SECRET is configured above
if ( length($SECURE_SECRET) > 0 )
{
	VPCConnection::signCommandData($SECURE_SECRET);
}

# Get the Commnad URL
my $RedirectURL=VPCConnection::getCommandURL($params{'virtualPaymentClientURL'});

doRedirect($RedirectURL);

sub doRedirect ($)
{
	my ( $RedirectURL ) = @_;
	
    # The Digital Order is sent using a Meta refresh to send the customers
    # browser to the Payment Server with the Digital Order.
    # The tag used to perform the redirect is
    # "<META HTTP-EQUIV='Refresh' CONTENT='0;URL=$RedirectURL'>"
    # We now mark up and display the complete HTML page that we use to perform
    # this simple redirection.
    
    # If debug is set, we do not automatically rediect and the user must click
    # on the hyperlink to continue. This gives the user time to read the debug
    # output. Debug should never be set in a production environment.
    
    print "
    <!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
    <HTML>
    <HEAD><TITLE>3 Party Payment Redirect Page</TITLE>
    <META http-equiv='Content-Type' content='text/html, charset=iso-8859-1'>
    <META HTTP-EQUIV='Refresh' CONTENT=\"0;URL='$RedirectURL'\">
    <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
    <style type='text/css'>
        <!--
        h1       { font-family:Arial,sans-serif; font-size:20pt; font-weight:600; margin-bottom:0.1em; color:#08185A;}
        h2       { font-family:Arial,sans-serif; font-size:14pt; font-weight:100; margin-top:0.1em; color:#08185A;}
        h2.co    { font-family:Arial,sans-serif; font-size:24pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
        h3       { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
        h3.co    { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#FFFFFF}
        body     { font-family:Verdana,Arial,sans-serif; font-size:10pt; background-color:#FFFFFF; color:#08185A}
        th       { font-family:Verdana,Arial,sans-serif; font-size:8pt; font-weight:bold; background-color:#CED7EF; padding-top:0.5em; padding-bottom:0.5em;  color:#08185A}
        tr       { height:25px; }
        tr.shade { height:25px; background-color:#CED7EF }
        tr.title { height:25px; background-color:#0074C4 }
        td       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A }
        td.red   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
        td.green { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#008800 }
        p        { font-family:Verdana,Arial,sans-serif; font-size:10pt; color:#FFFFFF }
        p.blue   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#08185A }
        p.red    { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
        p.green  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#008800 }
        div.bl   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#0074C4 }
        div.red  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
        li       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
        input    { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold }
        select   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold; }
        textarea { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:normal; scrollbar-arrow-color:#08185A; scrollbar-base-color:#CED7EF }
        -->
    </style>
</head>
<body>
    
    <!-- start branding table -->
    <table width='100%' border='2' cellpadding='2' bgcolor='#0074C4'>
        <tr>
            <td bgcolor='#CED7EF' width='90%'><h2 class='co'>&nbsp;Amex Virtual Payment Client Example</h2></td>
            <td bgcolor='#0074C4' align='center'><h3 class='co'>Dialect<br>Payments</h3></td>
        </tr>
    </table>
    <!-- end branding table -->

    <center><h1><br>3 Party Payment Redirect Page</h1></center>

	<table width='80%' align='center' border='0' cellpadding='10'>
		<tr>
			<td align='center' colspan='2'>
    			<br />The Digital Order has been generated and your browser
    			is being redirected to process your order.<br /><br />
    			<strong>If your browser does not go to the payment site within 30 
    			seconds <a href='$RedirectURL'>click here to continue</a>.</strong>
    		</td>
		</tr>
        <tr>    
            <td width='50%'>&nbsp;</td>
            <td width='50%'>&nbsp;</td>
        </tr>
        <tr class='title'>
            <td colspan='2'><p><strong>&nbsp;Example Code Version Information</strong></p></td>
        </tr>
        <tr> 
            <td align='right'><strong><em>Example Code Version: </em></strong></td>
            <td>$VERSION</td>
        </tr class='shade'>
            <td align='right'><strong><em>VPCConnection Version: </em></strong></td>
            <td>".VPCConnection::getPackageVersion()."</td>
        </tr>
         <tr class='shade'>    
            <td align='right'><strong><em>PaymentCodesHelper Version: </em></strong></td>
            <td>".PaymentCodesHelper::getPackageVersion()."</td>
        </tr>
        <tr>    
            <td width='50%'>&nbsp;</td>
            <td width='50%'>&nbsp;</td>
        </tr>
    </table>
    </body>
	</html>
	";

	exit;

};
