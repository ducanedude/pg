#!/usr/bin/perl -w

# This example assumes that a form has been sent to this example with the
# required fields. The example then processes the command and displays the
# result or error to a HTML page in the users web browser.

# Initialisation
# ==============
# Use the required Perl Libraries
# -------------------------------
use strict;
use CGI;
require 'VPCConnection.pl';
require 'PaymentCodesHelper.pl';
#use diagnostics;

my $VERSION = '2.0.2';

# Sub Prototypes
sub displayError();
sub displayReceipt();

my $perl_cgi = new CGI;
my %params = $perl_cgi->Vars;

# prepare to output to browser
print $perl_cgi->header(
	-expires => '0',
	pragma   => 'no-cache',
	cache    => 'no-cache'
);

my $SECURE_SECRET = "";
my $SECURE_SECRET_ISVALID = undef;

# Process the response data
VPCConnection::processCommandResponseHash(%params);
 
# Check the data if a $SECURE_SECRET is configured above
if ( length($SECURE_SECRET) > 0 )
{
	$SECURE_SECRET_ISVALID = VPCConnection::checkResponseSignature($SECURE_SECRET);
}

# Get the response data
# Standard Response Fields
my $vpc_Amount = VPCConnection::getResultField("vpc_Amount","Unknown");
my $vpc_Locale = VPCConnection::getResultField("vpc_Locale","Unknown");
my $vpc_BatchNo = VPCConnection::getResultField("vpc_BatchNO","Unknown");
my $vpc_Command = VPCConnection::getResultField("vpc_Command","Unknown");
my $vpc_Version = VPCConnection::getResultField("vpc_Version","Unknown");
my $vpc_Card = VPCConnection::getResultField("vpc_Card","Unknown");
my $vpc_OrderInfo = VPCConnection::getResultField("vpc_OrderInfo","Unknown");
my $vpc_ReceiptNo = VPCConnection::getResultField("vpc_ReceiptNo","Unknown");
my $vpc_Merchant = VPCConnection::getResultField("vpc_Merchant","Unknown");
my $vpc_MerchTxnRef = VPCConnection::getResultField("vpc_MerchTxnRef","Unknown");
my $vpc_AuthorizeId = VPCConnection::getResultField("vpc_AuthorizeId","Unknown");
my $vpc_TransactionNo = VPCConnection::getResultField("vpc_TransactionNo","Unknown");
my $vpc_AcqResponseCode = VPCConnection::getResultField("vpc_AcqResponseCode","Unknown");
my $vpc_TxnResponseCode = VPCConnection::getResultField("vpc_TxnResponseCode","Unknown");
my $vpc_Message = VPCConnection::getResultField("vpc_Message","Unknown");

# CSC Response Fields
my $vpc_cscResultCode = VPCConnection::getResultField("vpc_cscResultCode","Unknown");
my $vpc_cscACQRespCode = VPCConnection::getResultField("vpc_cscACQRespCode","Unknown");

# AVS/AAV Response Fields
my $vpc_avsResultCode = VPCConnection::getResultField("vpc_avsResultCode","Unknown");
my $vpc_avsACQRespCode = VPCConnection::getResultField("vpc_acsACQRespCode","Unknown");

displayReceipt();

sub displayReceipt ()
{

	print "
<!DOCTYPE HTML PUBLIC '-'W3C'DTD HTML 4.01 Transitional'EN'>
<head><title>Virtual Payment Client Receipt</title>
    <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
    <style type='text/css'>
        <!--
        h1       { font-family:Arial,sans-serif; font-size:20pt; font-weight:600; margin-bottom:0.1em; color:#08185A;}
        h2       { font-family:Arial,sans-serif; font-size:14pt; font-weight:100; margin-top:0.1em; color:#08185A;}
        h2.co    { font-family:Arial,sans-serif; font-size:24pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
        h3       { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
        h3.co    { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#FFFFFF}
        body     { font-family:Verdana,Arial,sans-serif; font-size:10pt; background-color:#FFFFFF; color:#08185A}
        th       { font-family:Verdana,Arial,sans-serif; font-size:8pt; font-weight:bold; background-color:#CED7EF; padding-top:0.5em; padding-bottom:0.5em;  color:#08185A}
        tr       { height:25px; }
        tr.shade { height:25px; background-color:#CED7EF }
        tr.title { height:25px; background-color:#0074C4 }
        td       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A }
        td.red   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
        td.green { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#008800 }
        p        { font-family:Verdana,Arial,sans-serif; font-size:10pt; color:#FFFFFF }
        p.blue   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#08185A }
        p.red    { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
        p.green  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#008800 }
        div.bl   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#0074C4 }
        div.red  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
        li       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
        input    { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold }
        select   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold; }
        textarea { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:normal; scrollbar-arrow-color:#08185A; scrollbar-base-color:#CED7EF }
        -->
    </style>
</head>
<body>
    
    <!-- start branding table -->
    <table width='100%' border='2' cellpadding='2' bgcolor='#0074C4'>
        <tr>
            <td bgcolor='#CED7EF' width='90%'><h2 class='co'>&nbsp;Amex Virtual Payment Client Example</h2></td>
            <td bgcolor='#0074C4' align='center'><h3 class='co'>Dialect<br>Payments</h3></td>
        </tr>
    </table>
    <!-- end branding table -->
    
    <center><h1><br>Virtual Payment Client Receipt</h1></center>
    
    <table align='center' border='0' width='80%'>
        
        <tr class='title'>
            <td colspan='2'><p><strong>&nbsp;3-Party Transaction Receipt Fields</strong></p></td>
        </tr>
        <tr>
            <td align='right'><strong><em>VPC Command: </em></strong></td>
            <td>$vpc_Command</td>
        </tr>
        <tr class='shade'>
            <td align='right'><strong><em>Version: </em></strong></td>
            <td>$vpc_Version</td>
        </tr>
        <tr>
            <td align='right'><strong><em>Response Message: </em></strong></td>
            <td>$vpc_Message</td>
        </tr>
        <tr class='shade'>
            <td align='right'><strong><em>Merchant Transaction Reference: </em></strong></td>
            <td>$vpc_MerchTxnRef</td>
        </tr>
        <tr>
            <td align='right'><strong><em>Merchant ID: </em></strong></td>
            <td>$vpc_Merchant</td>
        </tr>
        <tr class='shade'>
            <td align='right'><strong><em>Order Information: </em></strong></td>
            <td>$vpc_OrderInfo</td>
        </tr>
        <tr>
            <td align='right'><strong><em>Transaction Amount: </em></strong></td>
            <td>$vpc_Amount</td>
        </tr>
        <tr>
            <td colspan='2' align='center'>
                <div class='bl'>Fields above are the primary request values.<hr>Fields below are receipt data fields.</div>
            </td>
        </tr>
        <tr class='shade'>
            <td align='right'><strong><em>QSI Response Code: </em></strong></td>
            <td>$vpc_TxnResponseCode</td>
        </tr>
        <tr>
            <td align='right'><strong><em>QSI Response Code Description: </em></strong></td>
            <td>".PaymentCodesHelper::getResponseDescription($vpc_TxnResponseCode)."</td>
        </tr>
        <tr class='shade'>
            <td align='right'><strong><em>Acquirer Response Code: </em></strong></td>
            <td>$vpc_AcqResponseCode</td>
        </tr>
        <tr>
            <td align='right'><strong><em>Shopping Transaction Number: </em></strong></td>
            <td>$vpc_TransactionNo</td>
        </tr>
        <tr class='shade'>
            <td align='right'><strong><em>Receipt Number: </em></strong></td>
            <td>$vpc_ReceiptNo</td>
        </tr>
        <tr>
            <td align='right'><strong><em>Authorization ID: </em></strong></td>
            <td>$vpc_AuthorizeId</td>
        </tr>
        <tr class='shade'>                  
            <td align='right'><em><strong>CardType: </strong></em></td>
            <td>$vpc_Card</td>
        </tr>
        <tr>
            <td colspan='2' align='center'>
                <div class='bl'>Fields above are for a Standard Transaction<br />
                    <hr />
                Fields below are additional fields for extra functionality.</div>
            </td>
        </tr>
        <tr class='title'>
            <td colspan='2'><p><strong>&nbsp;CSC Data Fields</strong></p></td>
        </tr>
        <tr>
            <td align='right'><strong><em>CSC Result Code: </em></strong></td>
            <td>$vpc_cscResultCode</td>
        </tr>
        <tr class='shade'>
            <td align='right'><strong><em>CSC Result Description: </em></strong></td>
            <td>".PaymentCodesHelper::getCSCDescription($vpc_cscResultCode)."</td>
        </tr>
        
        <tr><td colspan='2' align='center'><hr/></td></tr>
        <tr class='title'>
            <td colspan='2'><p><strong>&nbsp;AVS/AAV Data Fields</strong></p></td>
        </tr>
        <tr>
            <td align='right'><strong><em>AVS Result Code: </em></strong></td>
            <td>$vpc_avsResultCode</td>
        </tr>
        <tr class='shade'>
            <td align='right'><strong><em>AVS Result Description: </em></strong></td>
            <td>".PaymentCodesHelper::getAVSDescription($vpc_avsResultCode)."</td>
        </tr>

        <tr><td colspan='2' align='center'><hr/></td></tr>

        <tr class='title'>
        	<td colspan='2'><p><strong>&nbsp;Secure Hash Validation</strong></p></td>
        </tr>";
        if ( defined($SECURE_SECRET_ISVALID) )
        {
        	if ( $SECURE_SECRET_ISVALID == 1) {
        		print "
        <tr>
        	<td align='right'><strong><em>Validation Status: </em></strong></td>
        	<td><strong><font color='green'>VALIDATION SUCCESSFULL</font></strong></td>
        </tr>";
        	} else {
        		print "
        <tr>
        	<td align='right'><strong><em>Validation Status: </em></strong></td>
        	<td><strong><font color='red'>VALIDATION FAILED</font></strong></td>
        </tr>";
        	}
        } else {
        	print "
        <tr>
        	<td align='right'><strong><em>Validation Status: </em></strong></td>
        	<td><strong><font color='orange'>VALIDATION NOT PERFORMED</font></strong> (No 'SECURE_SECRET' present).</td>
        </tr>";
        }
        
    print "
        <tr><td colspan='2' align='center'><hr/></td></tr>

        <tr class='title'>
            <td colspan='2'><p><strong>&nbsp;Example Code Version Information</strong></p></td>
        </tr>
        <tr> 
            <td align='right'><strong><em>Example Code Version: </em></strong></td>
            <td>$VERSION</td>
        </tr>
        <tr class='shade'>    
            <td align='right'><strong><em>VPCConnection Version: </em></strong></td>
            <td>".VPCConnection::getPackageVersion()."</td>
        </tr>
        <tr>    
            <td align='right'><strong><em>PaymentCodesHelper Version: </em></strong></td>
            <td>".PaymentCodesHelper::getPackageVersion()."</td>
        </tr>
        <tr>    
            <td width='50%'>&nbsp;</td>
            <td width='50%'>&nbsp;</td>
        </tr>
        <tr><td colspan='2' align='center'><p class='blue'><a href='/Perl_VPC_3Party_Super_Order.html'>Another Transaction</a></p></td></tr>
        <tr>    
            <td width='50%'>&nbsp;</td>
            <td width='50%'>&nbsp;</td>
        </tr>
    </table>
</body>
</html>";

	exit;
};
