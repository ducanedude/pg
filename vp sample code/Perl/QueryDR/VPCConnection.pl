#!/usr/bin/perl -w

package VPCConnection;

# Initialisation
# ==============
# Use the required Perl Libraries
# -------------------------------
use strict;
use CGI;
use Digest::MD5 qw (md5_hex);

#use diagnostics;

my $VERSION = '2.0.2';

# This variable defines if the code is going to opperate in debug mode.
# Debug mode writes all socket communications to standard error so that
# errors with communications can be debugged.
my $DEBUG = 0;

my %ORDER_FIELDS;
my %RESPONSE_FIELDS;

# This sub allows an external application to determine the version of this
# package
sub getPackageVersion ()
{
	return $VERSION;
};

# This sub allows an external application to set the debug parameter in this package
sub setDebug($)
{
	my ( $debugvalue) = @_;
	$DEBUG=$debugvalue;
	return 1;
};

# This sub clears all the command data so that a new command can be performed
sub reset ()
{
	%ORDER_FIELDS=undef;
	%RESPONSE_FIELDS=undef;
	return 1;
};

# Utility function for URL Decode
my $urldecode = sub ($)
{
	my ( $value ) = @_;
	
	return CGI::unescapeHTML($value);
};

# Utility function for URL Encode
my $urlencode = sub ($)
{
	my ( $value ) = @_;
	
	return CGI::escapeHTML($value);
};

# This is a utility sub that formats a date correctly to be added to the
# Digital Order, i.e. YYMM
sub formatExpiryDate($$)
{
	my ( $month, $year ) = @_;
	
	if (length($month) < 2)
	{
		$month = sprintf("%02d",$month);
	}
	
	if (length($month) > 2)
	{
		$month = substr($month,-2);
	}

	if (length($year) < 2)
	{
		$year = sprintf("%02d",$year);
	}
	
	if (length($year) > 2)
	{
		$year = substr($year,-2);
	}
	
	return $year . $month;
};

# This sub performs retireves the value of the specified field from the Command's response
sub getResultField($$)
{
	my ( $fieldName, $defaultValue ) = @_;

	if ( exists($RESPONSE_FIELDS{$fieldName}) )
	{
		return $RESPONSE_FIELDS{$fieldName};
	} else {
		if ( defined($defaultValue) )
		{
			return $defaultValue;
		} else {
			return undef;
		}
	}
};

# This sub retrieves the QSI Response Code (if it is available)
sub getQSICode()
{
	my $qsiCode = getResultField('vpc_TxnResponseCode',"");
	if ( defined($qsiCode) )
	{
		return $qsiCode;
	} else {
		return undef;
	}
};

# This sub performs adds a field to the data that will be used to execute the command
sub addCommandField($$)
{
	my ( $fieldName, $fieldValue ) = @_;
	
	if ( !defined($fieldValue) )
	{
		$fieldValue="";
	}
	
	$ORDER_FIELDS{&$urlencode($fieldName)} = &$urlencode($fieldValue);
	return 1;
};

# This sub executes the command using a syncronous method (2 Party via HTTP POST)
sub getPostData()
{
	my $postData="";
	my $key="";
	my $value="";
	my $separator="";
	foreach $key (keys %ORDER_FIELDS)
	{
		$value=$ORDER_FIELDS{$key};
		
		# HTTP encode key and value
		$key=&$urlencode($key);
		$value=&$urlencode($value);
		
		# Add the HTTP encoded data to the POST data
		$postData .= $separator.$key."=".$value;
		$separator="&";
	}
	return $postData;
};

# This sub constructs a URL with the command parameters encoded in the QueryString (HTTP GET)
sub getCommandURL($)
{
	my ( $vpcURL ) = @_;
	
	my $key="";
	my $value="";
	my $separator="?";
	foreach $key ( keys %ORDER_FIELDS )
	{
		$value=$ORDER_FIELDS{$key};
		
		# HTTP encode key and value
		$key=&$urlencode($key);
		$value=&$urlencode($value);
		
		# Add the HTTP encoded data to the POST data
		$vpcURL .= $separator.$key."=".$value;
		$separator="&";
	}
	return $vpcURL;
};

# This sub creates the secure hash that is used to sign the command fields.
# This signature is used to protect the data from modfication in transit to the
# Payment Gateway.
sub signCommandData($) 
{
	my ( $secureSecret ) = @_;
	
	if ( !defined($secureSecret) ) 
	{
		$secureSecret = "";
	}

	if ( exists($ORDER_FIELDS{'vpc_SecureHash'}) )
	{
		delete($ORDER_FIELDS{'vpc_SecureHash'});
	}

	my $singatureDataString = $secureSecret;
	foreach my $key (sort keys %ORDER_FIELDS)
	{
		$singatureDataString .= $ORDER_FIELDS{$key};
	}

	$ORDER_FIELDS{'vpc_SecureHash'} = md5_hex($singatureDataString);
	return 1;
};

# This sub checks the secure hash that is has been returned in the command respose
# to ensure the data has not been modified in transit from the Payment Gateway.
sub checkResponseSignature($) 
{
	my ( $secureSecret ) = @_;
	
	my %ResponseFieldsLocal = %RESPONSE_FIELDS;
	
	if ( !defined($secureSecret) ) 
	{
		$secureSecret = "";
	}

	if ( exists($ResponseFieldsLocal{'vpc_SecureHash'}) )
	{
		delete($ResponseFieldsLocal{'vpc_SecureHash'});
	}

	my $singatureDataString = $secureSecret;
	foreach my $key (sort keys %ResponseFieldsLocal)
	{
		$singatureDataString .= $ResponseFieldsLocal{$key};
	}

	my $signature = md5_hex($singatureDataString);
	
	if ( uc $signature eq uc $RESPONSE_FIELDS{'vpc_SecureHash'} )
	{
		return 1;
	} else {
		return 0;
	}
};

# This sub processess the QueryString reposnse from an Asyncronous Command (3 Party)
sub processCommandResponseHash(%)
{
	my ( %CommandResponse ) = @_;
	
	foreach my $key (sort keys %CommandResponse)
	{
		$RESPONSE_FIELDS{&$urldecode($key)} = &$urldecode($CommandResponse{$key});
	}

	return 1;
};

# This sub processes the content text from a HTTP POST syncronous Command (2 Party)
sub processCommandResponseText($)
{
	my ( $CommandResponse ) = @_;
	
	if ( !defined($CommandResponse) )
	{
		return 0;
	}
	
	my @tuplesArray = split(/&/,$CommandResponse);
	
	foreach my $tuple (@tuplesArray)
	{
		(my $fieldName, my $fieldValue) = split(/=/, $tuple, 2);
		$RESPONSE_FIELDS{&$urldecode($fieldName)} = &$urldecode($fieldValue);
	}

	return 1;
};


