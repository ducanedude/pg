package com.gateway.payment.transaction.api;

import java.util.Map;

public class PaymentProviderFactory {
	
	public static PaymentProvider getBean(Map<String,String> fields) {
		
		PaymentProvider pp = null;
		
		if(fields != null && fields.get("PROVIDER") != null && fields.get("PROVIDER").length() > 0) {
			String provider = fields.get("PROVIDER");
			
			if("AMEX".equalsIgnoreCase(provider)) {
				pp = new ProviderAmex();
			}
		}
		
		return pp;
	}
}