package com.gateway.payment.transaction.api;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import com.sun.net.ssl.SSLContext;
import com.sun.net.ssl.X509TrustManager;

public abstract class PaymentProvider {
	String paymentHost;
	int paymentPort;
    boolean useProxy = false;
    String proxyHost;
    int proxyPort = 0;
    String paymentAccessCode;
    
    /**
     * Declare a static X509 trust manager.
     */
    public static X509TrustManager s_x509TrustManager = null;
    /**
     * Declare a static ssl Socket Factory.
     */
    public static SSLSocketFactory s_sslSocketFactory = null;
    
    static {
            s_x509TrustManager = new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() { return new X509Certificate[] {}; } 
            public boolean isClientTrusted(X509Certificate[] chain) { return true; } 
            public boolean isServerTrusted(X509Certificate[] chain) { return true; } 
        };

        java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        try {
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[] { s_x509TrustManager }, null);
            s_sslSocketFactory = context.getSocketFactory();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
    
    /**
     * This method would isolate the complexity of calling different provider as each provider will have a separate implementation.
     * As an SLA it would add necessary fields required to make the call to payment service and strip off unwanted response related to payment service provider.
     * @param requestData fields required for posting a payment
     * @return attributes returned from the payment service
     */
    public abstract Map<String,String> postTransaction(Map<String,String> requestData) throws IOException;
    
    /**
     * Get the transaction response normalized to gateway response codes, isolates the complexity from different codes from each payment service
     * @param responseData attributes returned from the payment service
     * @return transaction response code
     */
    public abstract String getResponseCode(Map<String,String> responseData);
    
    /**
     * Helper function to generate and send the POST data for a 2 party VPC txn.
     * @param vpc_Host is the host where the 2 party transaction request is being sent to.
     * @param data is the data being sent in the POST request.
     * @param useProxy is the flag indicating whether a proxy is being used.
     * @param proxyHost is the proxy host being used for the transaction.
     * @param proxyPort is the port of the proxy host being used for the transaction.
     * @return the response data for the 2 party transaction request.
     * @throws java.io.IOException when unable to perform the POST request for the 2 party transaction.
     */
    protected String doPost(String vpc_Host, int vpc_Port, Map<String,String> requestData, boolean useProxy, String proxyHost, int proxyPort) throws IOException {

        InputStream is;
        OutputStream os;
        String fileName = "";
        boolean useSSL = false;
        
        String data = createPostDataFromMap(requestData);
        
        // determine if SSL encryption is being used
        if (vpc_Host.substring(0,8).equalsIgnoreCase("HTTPS://")) {
            useSSL= true;
            // remove 'HTTPS://' from host URL
            vpc_Host = vpc_Host.substring(8);
            // get the filename from the last section of vpc_URL
            fileName = vpc_Host.substring(vpc_Host.lastIndexOf("/"));
            // get the IP address of the VPC machine
            vpc_Host = vpc_Host.substring(0,vpc_Host.lastIndexOf("/"));
        }
        
        // use the next block of code if using a proxy server
        if (useProxy) {
            Socket s = new Socket(proxyHost, proxyPort);
            os = s.getOutputStream();
            is = s.getInputStream();
            // use next block of code if using SSL encryption
            if (useSSL) {
                String msg = "CONNECT " + vpc_Host + ":" + vpc_Port + " HTTP/1.0\r\n" + "User-Agent: HTTP Client\r\n\r\n";
                os.write(msg.getBytes());
                byte[] buf = new byte[4096];
                int len = is.read(buf);
                String res = new String(buf, 0, len);

                // check if a successful HTTP connection
                if (res.indexOf("200") < 0) {
                    throw new IOException("Proxy would now allow connection - " + res);
                }
                
                // write output to VPC
                SSLSocket ssl = (SSLSocket)s_sslSocketFactory.createSocket(s, vpc_Host, vpc_Port, true);
                ssl.startHandshake();
                os = ssl.getOutputStream();
                // get response data from VPC
                is = ssl.getInputStream();
            // use the next block of code if NOT using SSL encryption
            } else {
                fileName = vpc_Host;
            }
        // use the next block of code if NOT using a proxy server
        } else {
            // use next block of code if using SSL encryption
            if (useSSL) {
                Socket s = s_sslSocketFactory.createSocket(vpc_Host, vpc_Port);
                os = s.getOutputStream();
                is = s.getInputStream();
            // use next block of code if NOT using SSL encryption
            } else {
                Socket s = new Socket(vpc_Host, vpc_Port);
                os = s.getOutputStream();
                is = s.getInputStream();
            }
        }
        
        String req = "POST " + fileName + " HTTP/1.0\r\n"
                             + "User-Agent: HTTP Client\r\n"
                             + "Content-Type: application/x-www-form-urlencoded\r\n"
                             + "Content-Length: " + data.length() + "\r\n\r\n"
                             + data;

        // log outgoing traffic
        // audit.log(req);
        
        os.write(req.getBytes());
        String res = new String(readAll(is));

        // log incoming traffic
        // audit.log(res);
        
        // check if a successful connection
        if (res.indexOf("200") < 0) {
            throw new IOException("Connection Refused - " + res);
        }
        
        if (res.indexOf("404 Not Found") > 0) {
            throw new IOException("File Not Found Error - " + res);
        }
        
        int resIndex = res.indexOf("\r\n\r\n");
        String body = res.substring(resIndex + 4, res.length());
        return body;
    }
    
    /**
     * Helper function to read the response data.
     * @param is the response data returned.
     * @return a byte array of the input data.
     * @throws java.io.IOException when it is unable to read the response data.
     */
    public static byte[] readAll(InputStream is) throws IOException {
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        
        while (true) {
            int len = is.read(buf);
            if (len < 0) {
                break;
            }
            baos.write(buf, 0, len);
        }
        return baos.toByteArray();
    }
    
    /**
     * Helper function to create the POST data from the fields provided.
     * @param fields a map of the fields to be included in the POST request.
     * @return the POST data string to be sent for processing.
     */
    public String createPostDataFromMap(Map<String,String> fields) { 
    	StringBuffer buf = new StringBuffer();

        String ampersand = "";

        // append all fields in a data string
        for (Iterator<String> i = fields.keySet().iterator(); i.hasNext(); ) {
            
            String key = i.next();
            String value = fields.get(key);
            
            if ((value != null) && (value.length() > 0)) {
                // append the parameters
                buf.append(ampersand);
                buf.append(URLEncoder.encode(key));
                buf.append('=');
                buf.append(URLEncoder.encode(value));
            }
            ampersand = "&";
        }

        // return string 
        return buf.toString();    
    }
    
    /**
     * Helper function to create a map of the response data.
     * @param queryString the result string returned from the POST request.
     * @return the map of the response fields returned.
     */
    public Map<String,String> createMapFromResponse(String queryString) {
        Map<String,String> map = new HashMap<String,String>();
        StringTokenizer st = new StringTokenizer(queryString, "&");
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            int i = token.indexOf('=');
            if (i > 0) {
                try {
                    String key = token.substring(0, i);
                    String value = URLDecoder.decode(token.substring(i + 1, token.length()));
                    map.put(key, value);
                } catch (Exception ex) {
                    // Do Nothing and keep looping through data
                }
            }
        }
        return map;
    }
    
    
    
    /**
     * Helper function to get the response data returned for the given field.
     * @param in the field to retrieve the data for.
     * @param responseFields the map of fields in the response string to the transaction attempt.
     * @return the data for the given field or "No Value Returned".
     */
    public static String null2unknown(String in, Map<String,String> responseFields) {
      if (in == null || in.length() == 0 || (String)responseFields.get(in) == null) {
            return "No Value Returned";
        } else {
            return (String)responseFields.get(in);
        }
    }
    
    /**       
    * This method takes a data String and returns a predefined value if empty
    * If data Sting is null, returns string "No Value Returned", else returns input
    *
    * @param in String containing the data String
    * @return String containing the output String
    */
    public static String null2unknownDR(String in) {
        if (in == null || in.length() == 0) {
            return "No Value Returned";
        } else {
            return in;
        }
    }
}
