package com.gateway.payment.transaction.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ProviderAmex extends PaymentProvider{
	
	// bean init should be configuration driven
	public ProviderAmex() {
	    this.useProxy = false;
	    //this.proxyHost = "";
	    //this.proxyPort = 0;
		this.paymentHost = "https://vpos.amxvpos.com/vpcpay";
		this.paymentPort = 443;
		this.paymentAccessCode = "72E8CEB6";
	}
	
	public Map<String,String> postTransaction(Map<String,String> requestData) throws IOException {
		
		// add additional required fields
		//requestData.put("access_code", paymentAccessCode); vpc_AccessCode
		
		String resp = doPost(paymentHost, paymentPort, requestData, useProxy, proxyHost, proxyPort);
		System.out.println("resp = " + resp);
		
		Map<String,String> responseMap = createMapFromResponse(resp);
		//Map<String,String> responseMap = new HashMap<String,String>();
		
		//responseMap.remove("access_code");
	
		return responseMap;
	}
	
	public String getResponseCode(Map<String,String> responseData) {
		String code = responseData.get("vpc_TxnResponseCode");
		
		// the response codes from each PaymentProvider could be different so they need to be mapped to a common code
		if("S".equalsIgnoreCase(code)) return "0";
		
		return "?";
	}
}