<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%-- 
  ----------------- Disclaimer ------------------------------------------------
 
  Copyright © 2007 Dialect Payment Technologies - a Transaction Network
  Services company.  All rights reserved.
 
  This program is provided by Dialect Payment Technologies on the basis that
  you will treat it as confidential.
 
  No part of this program may be reproduced or copied in any form by any means
  without the written permission of Dialect Payment Technologies.  Unless
  otherwise expressly agreed in writing, the information contained in this
  program is subject to change without notice and Dialect Payment Technologies
  assumes no responsibility for any alteration to, or any error or other
  deficiency, in this program.
 
  1. All intellectual property rights in the program and in all extracts and 
     things derived from any part of the program are owned by Dialect and will 
     be assigned to Dialect on their creation. You will protect all the 
     intellectual property rights relating to the program in a manner that is 
     equal to the protection you provide your own intellectual property.  You 
     will notify Dialect immediately, and in writing where you become aware of 
     a breach of Dialect's intellectual property rights in relation to the
     program.
  2. The names "Dialect", "QSI Payments" and all similar words are trademarks
     of Dialect Payment Technologies and you must not use that name or any 
     similar name.
  3. Dialect may at its sole discretion terminate the rights granted in this 
     program with immediate effect by notifying you in writing and you will 
     thereupon return (or destroy and certify that destruction to Dialect) all 
     copies and extracts of the program in its possession or control.
  4. Dialect does not warrant the accuracy or completeness of the program or  
     its content or its usefulness to you or your merchant customers.  To the  
     extent permitted by law, all conditions and warranties implied by law  
     (whether as to fitness for any particular purpose or otherwise) are  
     excluded. Where the exclusion is not effective, Dialect limits its  
     liability to $100 or the resupply of the program (at Dialect's option).
  5. Data used in examples and sample data files are intended to be fictional 
     and any resemblance to real persons or companies is entirely coincidental.
  6. Dialect does not indemnify you or any third party in relation to the
    content or any use of the content as contemplated in these terms and 
     conditions. 
  7. Mention of any product not owned by Dialect does not constitute an 
     endorsement of that product.
  8. This program is governed by the laws of New South Wales, Australia and is 
     intended to be legally binding. 
  -----------------------------------------------------------------------------

  @author Dialect Payment Technologies
 
 ------------------------------------------------------------------------------
 PLEASE NOTE: To utilise this example, you will need to have compiled the 
              VPCPaymentConnection.java and VPCPaymentCodesHelper.java and
              include the compiled classes in your classpath for this example
              to work.
 ------------------------------------------------------------------------------             
--%>
<%@ page import="com.sun.net.ssl.*,
                 java.io.*,
                 java.net.*,
                 java.util.*,
                 java.security.MessageDigest,
                 java.security.cert.X509Certificate,
                 javax.net.ssl.SSLSocket,
                 javax.net.ssl.SSLSocketFactory,
                 com.sun.net.ssl.SSLContext,
                 com.sun.net.ssl.X509TrustManager,
                 com.Dialect.*"%>
                 
<%// *******************************************
    // START OF MAIN PROGRAM
    // *******************************************

    // Define Variables
    // If using a proxy server you must set the following variables
    // If NOT using a proxy server then set the 'useProxy' to false
    boolean useProxy = false;
    String proxyHost = "";
    int proxyPort = 0;
    
    // create new element that uses the VPC Payment Connection class
    VPCPaymentConnection vpcconn = new VPCPaymentConnection();

    // retrieve all the parameters into a hash map
    Map requestFields = new HashMap();
    for (Enumeration Enum = request.getParameterNames(); Enum.hasMoreElements();) {
        String fieldName = (String) Enum.nextElement();
        String fieldValue = request.getParameter(fieldName);
        if ((fieldValue != null) && (fieldValue.length() > 0)) {
            requestFields.put(fieldName, fieldValue);
        }
    }
    
    // no need to send the vpcURL, Title and Submit Button to the vpc
    String vpcURL = (String) requestFields.remove("virtualPaymentClientURL");
    String title  = (String) requestFields.remove("Title");
    requestFields.remove("SubButL");
    
    // Retrieve the order page URL from the incoming order page. This is only  
    // here to give the user the easy ability to go back to the Order page. 
    // This would not be required in a production system.
    String againLink = request.getHeader("Referer");

    // create the post data string to send
    String postData = vpcconn.createPostDataFromMap(requestFields);

    String resQS = "";
    String message = "";

    try {
        // create a URL connection to the Virtual Payment Client
        resQS = vpcconn.doPost(vpcURL, postData, useProxy, proxyHost, proxyPort);

    } catch (Exception ex) {
        // The response is an error message so generate an Error Page
        message = ex.toString();
    } //try-catch

    // create a hash map for the response data
    Map responseFields = vpcconn.createMapFromResponse(resQS);

    // Extract the available receipt fields from the VPC Response
    // If not present then let the value be equal to 'No Value returned'
    // Not all data fields will return values for all transactions.

    // don't overwrite message if any error messages detected
    if (message.length() == 0) {
        message            = vpcconn.null2unknown("vpc_Message", responseFields);
    }

    // Standard Receipt Data
    String amount          = vpcconn.null2unknown("vpc_Amount", responseFields);
    String locale          = vpcconn.null2unknown("vpc_Locale", responseFields);
    String batchNo         = vpcconn.null2unknown("vpc_BatchNo", responseFields);
    String command         = vpcconn.null2unknown("vpc_Command", responseFields);
    String version         = vpcconn.null2unknown("vpc_Version", responseFields);
    String cardType        = vpcconn.null2unknown("vpc_Card", responseFields);
    String orderInfo       = vpcconn.null2unknown("vpc_OrderInfo", responseFields);
    String receiptNo       = vpcconn.null2unknown("vpc_ReceiptNo", responseFields);
    String merchantID      = vpcconn.null2unknown("vpc_Merchant", responseFields);
    String merchTxnRef     = vpcconn.null2unknown("vpc_MerchTxnRef", responseFields);
    String authorizeID     = vpcconn.null2unknown("vpc_AuthorizeId", responseFields);
    String transactionNo   = vpcconn.null2unknown("vpc_TransactionNo", responseFields);
    String acqResponseCode = vpcconn.null2unknown("vpc_AcqResponseCode", responseFields);
    String txnResponseCode = vpcconn.null2unknown("vpc_TxnResponseCode", responseFields);

    // CSC Receipt Data
    String cscResultCode  = vpcconn.null2unknown("vpc_CSCResultCode", responseFields);
    String cscACQRespCode = vpcconn.null2unknown("vpc_AcqCSCRespCode", responseFields);
    
    // AVS Receipt Data
    String avsResultCode  = vpcconn.null2unknown("vpc_AVSResultCode", responseFields);
    String avsACQRespCode = vpcconn.null2unknown("vpc_AcqAVSRespCode", responseFields);
    
    // Get the descriptions behind the QSI, CSC and AVS Response Codes
    // Only get the descriptions if the string returned is not equal to "No Value Returned".
    
    String txnResponseCodeDesc = "";
    String cscResultCodeDesc = "";
    String avsResultCodeDesc = "";
    
    // establish the VPCPaymentCodesHelper to retreive the response descriptions
    VPCPaymentCodesHelper vpchelper = new VPCPaymentCodesHelper();

    if (txnResponseCode != "No Value Returned") {
        txnResponseCodeDesc = vpchelper.getResponseDescription(txnResponseCode);
    }
    
    if (cscResultCode != "No Value Returned") {
        cscResultCodeDesc = vpchelper.displayCSCResponse(cscResultCode);
    }
    
    if (avsResultCode != "No Value Returned") {
        avsResultCodeDesc = vpchelper.displayAVSResponse(avsResultCode);
    }    
    
    
    String error = "";
    // Show this page as an error page if error condition
    if (txnResponseCode.equals("7") || txnResponseCode.equals("No Value Returned")) {
        error = "Error ";
    }
        
    // FINISH TRANSACTION - Process the VPC Response Data
    // =====================================================
    // For the purposes of demonstration, we simply display the Response fields
    // on a web page.
%>                 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
    <html>
    <head>
        <title><%=title%> - <%=error%>Response Page</title>
        <meta http-equiv="Content-Type" content="text/html, charset=iso-8859-1">
        <style type="text/css">
            <!--
            h1       { font-family:Arial,sans-serif; font-size:20pt; font-weight:600; margin-bottom:0.1em; color:#08185A;}
            h2       { font-family:Arial,sans-serif; font-size:14pt; font-weight:100; margin-top:0.1em; color:#08185A;}
            h2.co    { font-family:Arial,sans-serif; font-size:24pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
            h3       { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#08185A}
            h3.co    { font-family:Arial,sans-serif; font-size:16pt; font-weight:100; margin-top:0.1em; margin-bottom:0.1em; color:#FFFFFF}
            body     { font-family:Verdana,Arial,sans-serif; font-size:10pt; background-color:#FFFFFF; color:#08185A}
            th       { font-family:Verdana,Arial,sans-serif; font-size:8pt; font-weight:bold; background-color:#CED7EF; padding-top:0.5em; padding-bottom:0.5em;  color:#08185A}
            tr       { height:25px; }
            .shade   { height:25px; background-color:#CED7EF }
            .title   { height:25px; background-color:#0074C4 }
            td       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A }
            td.red   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
            td.green { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#008800 }
            p        { font-family:Verdana,Arial,sans-serif; font-size:10pt; color:#FFFFFF }
            p.blue   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#08185A }
            p.red    { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
            p.green  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#008800 }
            div.bl   { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#0074C4 }
            div.red  { font-family:Verdana,Arial,sans-serif; font-size:7pt;  color:#FF0066 }
            li       { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#FF0066 }
            input    { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold }
            select   { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:bold; }
            textarea { font-family:Verdana,Arial,sans-serif; font-size:8pt;  color:#08185A; background-color:#CED7EF; font-weight:normal; scrollbar-arrow-color:#08185A; scrollbar-base-color:#CED7EF }
            -->
        </style>
    </head>
    <body>
        <!-- Start Branding Table -->
        <table width="100%" border="2" cellpadding="2" class="title">
            <tr>
                <td class="shade" width="90%"><h2 class="co">&nbsp;Virtual Payment Client Example</h2></td>
                <td class="title" align="center"><h3 class="co">Dialect<br />Payments</h3></td>
            </tr>
        </table>
        <!-- End Branding Table -->
        <center><h1><%=title%> - <%=error%>Response Page</h1></center>
        <table width="85%" align="center" cellpadding="5" border="0">
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Basic 2-Party Transaction Fields</strong></p></td>
            </tr>
            <tr>
                <td align="right" width="50%"><strong><i>VPC API Version: </i></strong></td>
                <td width="50%"><%=version%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Command: </i></strong></td>
                <td><%=command%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Merchant Transaction Reference: </i></strong></td>
                <td><%=merchTxnRef%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Merchant ID: </i></strong></td>
                <td><%=merchantID%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Order Information: </i></strong></td>
                <td><%=orderInfo%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Amount: </i></strong></td>
                <td><%=amount%></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <font color="#0074C4">Fields above are the primary request values.<br />
                    <hr />
                    Fields below are the response fields for a Standard Transaction.<br /></font>
                </td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>VPC Transaction Response Code: </i></strong></td>
                <td><%=txnResponseCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Transaction Response Code Description: </i></strong></td>
                <td><%=txnResponseCodeDesc%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Message: </i></strong></td>
                <td><%=message%></td>
            </tr>
<% 
    // Only display the following fields if not an error condition
    if (!txnResponseCode.equals("7") && !txnResponseCode.equals("No Value Returned")) { 
%>
            <tr>
                <td align="right"><strong><i>Receipt Number: </i></strong></td>
                <td><%=receiptNo%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Transaction Number: </i></strong></td>
                <td><%=transactionNo%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Acquirer Response Code: </i></strong></td>
                <td><%=acqResponseCode%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Bank Authorization ID: </i></strong></td>
                <td><%=authorizeID%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Batch Number: </i></strong></td>
                <td><%=batchNo%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>Card Type: </i></strong></td>
                <td><%=cardType%></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <font color="#0074C4">Fields above are for a Standard Transaction<br />
                    <hr />
                    Fields below are additional fields for extra functionality.</font><br />
                </td>
            </tr>
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Card Security Code Fields</strong></p></td>
            </tr>
            <tr>
            <tr class='shade'>
                <td align="right"><strong><i>CSC Acquirer Response Code: </i></strong></td>
                <td><%=cscACQRespCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>CSC QSI Result Code: </i></strong></td>
                <td><%=cscResultCode%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>CSC Result Description: </i></strong></td>
                <td><%=cscResultCodeDesc%></td>
            </tr>
            <tr>
                <td colspan="2"><hr /></td>
            </tr>
            <tr class='title'>
                <td colspan="2" height="25"><p><strong>&nbsp;Address Verification Service Fields</strong></p></td>
            </tr>
            <tr>
                <td align="right"><strong><i>AVS Acquirer Response Code: </i></strong></td>
                <td><%=avsACQRespCode%></td>
            </tr>
            <tr class='shade'>
                <td align="right"><strong><i>AVS QSI Result Code: </i></strong></td>
                <td><%=avsResultCode%></td>
            </tr>
            <tr>
                <td align="right"><strong><i>AVS Result Description: </i></strong></td>
                <td><%=avsResultCodeDesc%></td>
            </tr>
<%         

} %>

        </table>
        <center><p><a href='<%=againLink%>'>New Transaction</a></p></center>
    </body>
</html>