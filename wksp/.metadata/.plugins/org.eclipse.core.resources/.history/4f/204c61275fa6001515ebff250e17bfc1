package com.project.code;

import java.text.DateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gateway.payment.transaction.api.PaymentProvider;
import com.gateway.payment.transaction.api.PaymentProviderFactory;
import com.gateway.payment.transaction.constant.PaymentCodesHelper;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value = "/2p_form", method = RequestMethod.GET)
	public String twoP(Locale locale, Model model) {
		logger.info("Inside 2p_form");
		
		return "JSP_VPC_2Party_Super_Order_html";
	}
	
	@RequestMapping(value = "/2p_submit")
	public String twoPSubmit(Locale locale, Model model, HttpServletRequest request) {
		logger.info("Inside 2p_submit");
		
	    // retrieve all the parameters into a hash map
	    Map<String,String> requestData = new HashMap<String,String>();
	    for (Enumeration Enum = request.getParameterNames(); Enum.hasMoreElements();) {
	        String fieldName = (String) Enum.nextElement();
	        String fieldValue = request.getParameter(fieldName);
	        if ((fieldValue != null) && (fieldValue.length() > 0)) {
	        	requestData.put(fieldName, fieldValue);
	        }
	    }
	    
	    // log incoming traffic
        // audit.log(requestData);
	    
	    // remove the fields which are specific 
	    //String merchantId = requestData.remove("vpc_Merchant");
	    
	    Map<String,String> responseFields = null;
	    String errorMessage = "";
	    String txnResponseCode = "";
	    String txnResponseCodeDesc = "";

	    try {
	    	PaymentProvider pp = PaymentProviderFactory.getBean(requestData);
	    		
    		// make the call to post transaction
	    	responseFields = pp.postTransaction(requestData);
	    	
	    	txnResponseCode = pp.getResponseCode(responseFields);
	    	txnResponseCodeDesc = PaymentCodesHelper.getResponseDescription(txnResponseCode);
	    } catch (Exception ex) {
	        // The response is an error message so generate an Error Page
	    	errorMessage = ex.toString();
	    } //try-catch
	    
	    // add any merchant specific fields
	    //responseFields.put("merchantId",merchantId);
	    responseFields.put("txnResponseCode",txnResponseCode);
	    responseFields.put("txnResponseCodeDesc",txnResponseCodeDesc);
	    responseFields.put("errorMessage",errorMessage);
	    
        // log outgoing traffic
        // audit.log(responseFields);
	    
	    model.addAttribute("responseFeilds", responseFields);
	    
	    // return JSON with response fields
		return "JSP_VPC_2Party_Super_Order";
	}
	
	@RequestMapping(value = "/3p")
	public String threeP(Locale locale, Model model) {
		logger.info("Inside 3p");
		
		return "JSP_VPC_3Party_Super_Order_html";
	}
	
	@RequestMapping(value = "/3p_submit")
	public String threePSubmitDO(Locale locale, Model model) {
		logger.info("Inside 3p_submit");
		
		return "JSP_VPC_3Party_Super_Order_DO";
	}
	
	@RequestMapping(value = "/JSP_VPC_3Party_Super_Order_DR")
	public String threePSubmitDR(Locale locale, Model model) {
		logger.info("Inside JSP_VPC_3Party_Super_Order_DR");
		
		return "JSP_VPC_3Party_Super_Order_DR";
	}
	
}
